//
//  ServiceTypeCell.swift
//  GoJekUser
//
//  Created by Ansar on 26/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class CourierServiceTypeCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceImage:UIImageView!
    @IBOutlet weak var serviceNameLbl: UILabel!
    
    var isCurrentService = false
    {
        didSet {
            serviceNameLbl.textColor = isCurrentService ? .white : .darkGray
            serviceNameLbl.backgroundColor = isCurrentService ? .courierColor : .veryLightGray
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        initialLoads()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.serviceImage.setRoundCorner()
    }
    
}


//MARK: - Methods

extension CourierServiceTypeCell {
    private func initialLoads() {
        self.serviceImage.contentMode = .scaleAspectFill
        serviceNameLbl.font = UIFont.setCustomFont(name: .medium, size: .x16)
        serviceNameLbl.layer.cornerRadius = 4
    }
    
    
}
