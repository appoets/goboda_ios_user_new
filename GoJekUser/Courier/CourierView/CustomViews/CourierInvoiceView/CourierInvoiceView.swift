//
//  CourierInvoiceView.swift
//  GoJekUser
//
//  Created by Sudar on 03/07/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit

class CourierInvoiceView: UIView {
    
    @IBOutlet weak var mainScrlVw: UIScrollView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var totalPriceTitleLbl: UILabel!
    @IBOutlet weak var totalPriceValueLbl: UILabel!
    @IBOutlet weak var cardBackgroundView: UIView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerVw: UIView!
    @IBOutlet weak var headerImgVw: UIImageView!
    
    @IBOutlet weak var deliveryDateTitleLbl: UILabel!
    @IBOutlet weak var deliveryDateLbl: UILabel!
    @IBOutlet weak var discountApliedLbl: UILabel!
    @IBOutlet weak var discountApliedValueLbl: UILabel!
    
    @IBOutlet weak var statusTblVw: UITableView!
    @IBOutlet weak var titlebgVw: UIView!
    @IBOutlet weak var vehilcleNameLbl: UILabel!
    @IBOutlet weak var basefareVw: UIView!
    @IBOutlet weak var basefaretitleLbl: UILabel!
    @IBOutlet weak var basefaretitleValueLbl: UILabel!
    
    @IBOutlet weak var distancefareVw: UIView!
    @IBOutlet weak var distancefaretitleLbl: UILabel!
    @IBOutlet weak var distancefaretitleValueLbl: UILabel!
    @IBOutlet weak var timingVw: UIView!
    @IBOutlet weak var timingtitleLbl: UILabel!
    @IBOutlet weak var timingValueLbl: UILabel!
    
    @IBOutlet weak var taxVw: UIView!
    @IBOutlet weak var taxtitleLbl: UILabel!
    @IBOutlet weak var taxValueLbl: UILabel!
    
    @IBOutlet weak var priceLineVw: UIView!
    
    @IBOutlet weak var totalVw: UIView!
       @IBOutlet weak var totaltitleLbl: UILabel!
       @IBOutlet weak var totalValueLbl: UILabel!
    
    @IBOutlet weak var roundoffVw: UIView!
          @IBOutlet weak var roundofftitleLbl: UILabel!
          @IBOutlet weak var roundoffValueLbl: UILabel!
    
    @IBOutlet weak var subTotalVw: UIView!
    @IBOutlet weak var subTotaltitleLbl: UILabel!
    @IBOutlet weak var subTotalValueLbl: UILabel!
    @IBOutlet weak var discountView: UIView!
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var imagePayment: UIImageView!

    @IBOutlet weak var labelPaymentType: UILabel!

    
    
    var requestData : RequestResponseData?
    var ratingView: RatingView?
    var onClickConfirm:((PaymentType)->Void)?
    var isPaid:Int = 0
    var current_requestID = 0
    
    var paymentType : PaymentType = .NONE {
         didSet {
             submitBtn.setTitle(paymentType == .CASH ? Constant.SDone.localized.uppercased() : Constant.confirm.localized.uppercased(), for: .normal)
         }
     }

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func awakeFromNib(){
        initialLoad()
    }
    
    private func initialLoad(){
        mainScrlVw.contentSize  = CGSize(width: bgView.frame.width, height: bgView.frame.height)
        setDesignUpdate()
        setFont()
        setTargets()
        setColors()
        setLocalization()
        registerXiB()
      //  if let deliveryValue = requestData?.data?.first{
            //self.setValues(values: deliveryValue)
            for m in 0..<(requestData?.data?.count ?? 0) {
                if current_requestID == (requestData?.data?[m].id ?? 0){
                    self.setValues(values: requestData?.data?[m] ?? RequestData())
                }
            }
      //  }

        submitBtn.addTarget(self, action: #selector(confirmPayment(sender:)), for:.touchUpInside)
        totalVw.layer.borderColor = UIColor.black.cgColor
        totalVw.layer.borderWidth = 1
    }
    
    private func registerXiB(){
        statusTblVw.delegate =  self
        statusTblVw.dataSource = self
        statusTblVw.register(nibName: CourierConstant.CourierStatusTableViewCell)
    }
    private func setDesignUpdate(){
        headerVw.setCornerRadiuswithValue(value: 5)
        submitBtn.setCornerRadiuswithValue(value: 5)
    }
    private func setColors() {
        mainScrlVw.backgroundColor = .clear
        bgView.backgroundColor = .clear
        submitBtn.backgroundColor = .courierColor
        totalPriceTitleLbl.textColor = .white
        totalPriceValueLbl.textColor = .white
        basefaretitleLbl.textColor = .darkGray
        basefaretitleValueLbl.textColor = .darkGray
        distancefaretitleLbl.textColor = .darkGray
        distancefaretitleValueLbl.textColor = .darkGray
        timingtitleLbl.textColor = .darkGray
        timingValueLbl.textColor = .darkGray
        taxtitleLbl.textColor = .darkGray
        taxValueLbl.textColor = .darkGray
        deliveryDateLbl.textColor = .darkGray
        vehilcleNameLbl.textColor =  .courierColor
        cardBackgroundView.backgroundColor = .courierColor
    }
    private func setFont(){
        titleLbl.font = .setCustomFont(name: .medium, size: .x16)
        deliveryDateTitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        deliveryDateLbl.font = .setCustomFont(name: .medium, size: .x12)
        discountApliedLbl.font = .setCustomFont(name: .medium, size: .x14)
        discountApliedValueLbl.font = .setCustomFont(name: .medium, size: .x12)
        vehilcleNameLbl.font = .setCustomFont(name: .bold, size: .x16)
        basefaretitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        basefaretitleValueLbl.font = .setCustomFont(name: .medium, size: .x14)
        distancefaretitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        distancefaretitleValueLbl.font = .setCustomFont(name: .medium, size: .x14)
        timingtitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        timingValueLbl.font = .setCustomFont(name: .medium, size: .x14)
        taxtitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        taxValueLbl.font = .setCustomFont(name: .medium, size: .x14)
        totaltitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        totalValueLbl.font = .setCustomFont(name: .medium, size: .x14)
        subTotaltitleLbl.font = .setCustomFont(name: .bold, size: .x16)
        subTotalValueLbl.font = .setCustomFont(name: .bold, size: .x16)
        submitBtn.titleLabel?.font = .setCustomFont(name: .medium, size: .x18)
        totalPriceValueLbl.font = .setCustomFont(name: .bold, size: .x30)
        totalPriceTitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        roundofftitleLbl.font = .setCustomFont(name: .medium, size: .x14)
        roundoffValueLbl.font = .setCustomFont(name: .medium, size: .x14)
        labelPaymentType.font = .setCustomFont(name: .medium, size: .x14)
    }
    private func setLocalization(){
        titleLbl.text = CourierConstant.rating.localized
        submitBtn.setTitle(CourierConstant.proceed.localized, for: .normal)
        basefaretitleLbl.text = CourierConstant.basefare.localized
        distancefaretitleLbl.text = CourierConstant.totalDistance.localized
        timingtitleLbl.text = CourierConstant.weight.localized
        taxtitleLbl.text = CourierConstant.tax.localized
        deliveryDateTitleLbl.text = CourierConstant.deliverydate.localized
        totalPriceTitleLbl.text = CourierConstant.total.localized
        subTotaltitleLbl.text = CourierConstant.total.localized
        totaltitleLbl.text = CourierConstant.subTotal.localized
        roundofftitleLbl.text = CourierConstant.roundoff.localized
        
    }
    private func setTargets(){
        backBtn.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
    }
    
    @objc private func tapBack(){
        UIApplication.topViewController()?.navigationController?.popViewController(animated: true)
    }
    
    func setValues(values:RequestData) {
//        self.totalPriceValueLbl.text = "\(values.currency ?? "") \(values.payment?.total ?? 0)"
        self.deliveryDateLbl.text = values.assigned_time
        self.vehilcleNameLbl.text = values.booking_id
        isPaid = values.delivery?.first?.paid ?? 0
        paymentType = PaymentType(rawValue: values.payment_mode ?? "") ?? .CASH
        if isPaid == 1 {
            submitBtn.setTitle(Constant.SDone.localized.uppercased(), for: .normal)
        }
        distancefareVw.isHidden = true
        roundoffValueLbl.text = "\(values.currency ?? "") \(values.payment?.round_of ?? 0)"
        self.discountApliedValueLbl.text = "\(values.currency ?? "") \(values.payment?.discount ?? 0)"
        discountView.isHidden = values.delivery?.first?.payment?.discount == 0 ? true : false
        self.discountApliedValueLbl.text = "\(values.currency ?? "") \(values.payment?.discount ?? 0)"
        
        var FinalTotal = 0.0
        for i in 0..<(values.delivery?.count ?? 0) {
            FinalTotal += values.delivery?[i].payment?.total ?? 0
        }
        
        if paymentType == .CASH{
            subTotalValueLbl.text = "\(values.currency ?? "") \(FinalTotal)"
            totalPriceValueLbl.text = "\(values.currency ?? "") \(FinalTotal)"
        }else{
            subTotalValueLbl.text = "\(values.currency ?? "") \(FinalTotal)"
            totalPriceValueLbl.text = "\(values.currency ?? "") \(FinalTotal)"
        }
        
//        if paymentType == .CASH{
//            subTotalValueLbl.text = "\(values.currency ?? "") \(values.delivery?.first?.payment?.total ?? 0)"
//            totalPriceValueLbl.text = "\(values.currency ?? "") \(values.delivery?.first?.payment?.total ?? 0)"
//        }else{
//            subTotalValueLbl.text = "\(values.currency ?? "") \(values.delivery?.first?.payment?.payable ?? 0)"
//            totalPriceValueLbl.text = "\(values.currency ?? "") \(values.delivery?.first?.payment?.payable ?? 0)"
//        }
       
        self.basefaretitleValueLbl.text = "\(values.currency ?? "") \(values.payment?.fixed ?? 0)"
        self.distancefaretitleValueLbl.text = "\(values.currency ?? "") \(values.payment?.distance ?? 0)"
        self.timingValueLbl.text = "\(values.currency ?? "") \(values.payment?.weight ?? 0)"
        self.taxValueLbl.text = "\(values.currency ?? "") \(values.payment?.tax ?? 0)"
        
       // self.totalValueLbl.text = "\(values.currency ?? "") \(values.delivery?.first?.payment?.total ?? 0)"
        heightTableView.constant = CGFloat(((values.delivery?.count ?? 0) + 1) * 55)
        statusTblVw.reloadData()
        
        if values.payment_mode == PaymentType.CASH.rawValue {
            imagePayment.image = #imageLiteral(resourceName: "money_icon")
            labelPaymentType.text = PaymentType.CASH.rawValue
            
        }else if values.payment_mode == PaymentType.CARD.rawValue{
            imagePayment.image = #imageLiteral(resourceName: "ic_credit_card")
            labelPaymentType.text = PaymentType.CARD.rawValue
        }else {
            imagePayment.image = #imageLiteral(resourceName: "ic_credit_card")
            labelPaymentType.text = PaymentType.DPO.rawValue
        }
//        if values.use_wallet == 0 && values.payment_mode == PaymentType.CASH.rawValue {
//            labelPaymentType.text = values.payment_mode ?? ""
//        }else if values.use_wallet == 1 && values.payment_mode == PaymentType.CASH.rawValue{
//            if values.payment?.waiting_amount ?? 0 > values.payment?.payable ?? 0{
//                labelPaymentType.text = "Wallet"
//            }else{
//                labelPaymentType.text = values.payment_mode ?? "" + "Wallet"
//            }
//
//        }else if values.use_wallet == 0 && values.payment_mode == PaymentType.CARD.rawValue{
//            labelPaymentType.text = values.payment_mode ?? ""
//        }else if values.use_wallet == 1 && values.payment_mode == PaymentType.CARD.rawValue {
//            if values.payment?.wallet ?? 0 > values.payment?.payable ?? 0{
//                labelPaymentType.text = "Wallet"
//            }else{
//                labelPaymentType.text = values.payment_mode ?? "" + "Wallet"
//            }
//
//        }else if values.use_wallet == 1 && values.payment_mode == PaymentType.DPO.rawValue {
//            if values.payment?.wallet ?? 0 > values.payment?.payable ?? 0{
//                labelPaymentType.text = "Wallet"
//            }else{
//                labelPaymentType.text = values.payment_mode ?? "" + "Wallet"
//            }
//
//        }else{
//            labelPaymentType.text = values.payment_mode ?? ""
//        }
        
        if values.use_wallet == 1 && values.payment?.wallet == values.payment?.total {
            labelPaymentType.text = "Wallet"
        }else{
            labelPaymentType.text = values.payment_mode ?? ""  + "\( values.use_wallet ?? 0 == 1 ? " , Wallet" : "")"
        }
        
    }
    
    @IBAction func confirmPayment(sender:UIButton){
        if paymentType == .CASH {
            if isPaid == 1 {
                onClickConfirm?(paymentType)
            }else{
                ToastManager.show(title: Constant.pleaseConfirmPayment.localized, state: .error)
            }
        }else{
            onClickConfirm?(paymentType)
        }
    }
}
extension CourierInvoiceView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.requestData?.data?.first?.delivery?.count ?? 0) + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CourierConstant.CourierStatusTableViewCell, for: indexPath)  as! CourierStatusTableViewCell
        
        if indexPath.row == 0 {
            
            cell.locationTitleLbl.text = CourierConstant.pickuplocation
            cell.locationLbl.text = self.requestData?.data?.first?.s_address
            cell.topLineVw.isHidden = true
            cell.statusImgVw.image =  UIImage.init(named: CourierConstant.redTapeImg)
        }else{
            
            cell.locationTitleLbl.text = CourierConstant.dropofflocation
            cell.locationLbl.text =  self.requestData?.data?.first?.delivery?[indexPath.row - 1].dAddress
            cell.topLineVw.isHidden = true
            cell.statusImgVw.image =  UIImage.init(named: CourierConstant.ic_greenTap)
            
        }
        return cell
    }
}
// MARK: - UITableViewDelegate

extension CourierInvoiceView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    
    }
}
