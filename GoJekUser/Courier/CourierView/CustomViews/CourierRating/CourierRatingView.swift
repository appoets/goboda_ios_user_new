//
//  CourierRatingView.swift
//  GoJekProvider
//
//  Created by Chan Basha on 05/06/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class CourierRatingView: UIView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var labelHowWasRating: UILabel!
    
     var onClickSubmit:((_ rating: Int, _ comments: String)->Void)?
    
    override func awakeFromNib(){
       initialLoads()
    }
    
    private func initialLoads(){
        setFont()
        setDesign()
        commentsTextView.delegate = self
        buttonSubmit.addTarget(self, action: #selector(tapSubmit), for: .touchUpInside)
        buttonSubmit.backgroundColor = .courierColor
    }
    
    private func setFont(){
        commentsTextView.text = Constant.leaveComment.localized
        labelRating.text = CourierConstant.rating.localized
        labelHowWasRating.text = CourierConstant.deliveryHow.localized
        buttonSubmit.setTitle(Constant.SSubmit, for: .normal)
        labelRating.font = .setCustomFont(name: .bold, size: .x18)
        commentsTextView.font = .setCustomFont(name: .light, size: .x14)
        buttonSubmit.titleLabel?.font = .setCustomFont(name: .light, size: .x14)
    }
  
    private func setDesign(){
        commentsTextView.setCornerRadiuswithValue(value: 4)
        buttonSubmit.setCornerRadiuswithValue(value: 8)
        BGView.setCornerRadiuswithValue(value: 10)
    }
       
    @objc func tapSubmit(){
       onClickSubmit!(Int(ratingView.rating),commentsTextView.text)
    }
    
}
extension CourierRatingView: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == Constant.leaveComment.localized {
            textView.text = ""
            textView.textColor = .black
        }
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if textView.text == "" {
            commentsTextView.text = Constant.leaveComment.localized
            commentsTextView.textColor = .lightGray
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        endEditing(true)
    }
}
