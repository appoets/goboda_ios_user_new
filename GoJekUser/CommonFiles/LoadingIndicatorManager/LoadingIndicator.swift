//
//  LoadingIndicator.swift
//  GoJekUser
//
//  Created by Rajes on 13/04/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Lottie
import SwiftGifOrigin

class LoadingIndicator:NSObject{

    static var close: (() -> Void)? = {}
    static var imageView = UIImageView()

    static var backgroundView:UIView = {
        let sView = UIView()
        sView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        return sView
    }()
    
    static var contentView:AnimationView = {
        let sView = AnimationView(name: "Loader_Color")
        sView.loopMode = .loop
        sView.translatesAutoresizingMaskIntoConstraints = false
        sView.backgroundColor = UIColor.clear
        return sView
    }()
    
    
    static func frameSetup(){
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        contentView.widthAnchor.constraint(equalTo: window!.widthAnchor, multiplier: 0.2).isActive = true
        contentView.heightAnchor.constraint(greaterThanOrEqualTo: window!.widthAnchor, multiplier: 0.2).isActive = true
        contentView.centerYAnchor.constraint(equalTo: window!.centerYAnchor).isActive = true
        contentView.centerXAnchor.constraint(equalTo: window!.centerXAnchor).isActive = true
    }
    
    static func show() {
        DispatchQueue.main.async {
            let window = UIApplication.shared.windows.first { $0.isKeyWindow }
//            contentView.play()
            backgroundView.frame = window!.bounds
//            backgroundView.addSubview(contentView)
//            window!.addSubview(backgroundView)
//            frameSetup()
            
            let jeremyGif = UIImage.gif(name: "LoaderCopy")

//            imageView = UIImageView()
            imageView.frame = CGRect(x: ((window?.frame.width ?? 300)/2) - 30, y: ((window?.frame.height ?? 300)/2) - 30, width: 80, height: 80)
//            imageView.image = jeremyGif
            imageView.loadGif(name: "LoaderCopy")
//            backgroundView.addSubview(imageView)
            window!.addSubview(imageView)
//            UIApplication.shared.windows.first?.window?.addSubview(backgroundView)

//            let gif = imageView.fromGif(frame: CGRect(x:100, y: 100, width: 100, height: 100), resourceName: "LoaderCopy")
//            backgroundView.addSubview(gif ?? UIImageView())
            
//            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "LoaderCopy", withExtension: "gif")!)
//            let advTimeGif = UIImage.gifImageWithData(imageData!)
//            let imageView2 = UIImageView(image: advTimeGif)
//            imageView2.frame = CGRect(x: 20.0, y: 220.0, width:
//            self.backgroundView.frame.size.width - 40, height: 150.0)
//            backgroundView.addSubview(imageView2)

//            window!.addSubview(backgroundView)
        }
        
    }
    
    static func hide() {
        DispatchQueue.main.async {
//            contentView.stop()
//            contentView.removeFromSuperview()
//            backgroundView.removeFromSuperview()
            imageView.removeFromSuperview()

        }
    }

}



extension UIImageView {
     func fromGif(frame: CGRect, resourceName: String) -> UIImageView? {
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else {
            print("Gif does not exist at that path")
            return nil
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
            let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        let gifImageView = UIImageView(frame: frame)
        gifImageView.animationImages = images
        return gifImageView
    }
}
