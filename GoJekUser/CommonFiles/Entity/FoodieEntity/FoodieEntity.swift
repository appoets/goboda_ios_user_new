//
//  FoodieEntity.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class StoreListEntity: Mappable {
    
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [ShopsListData]?
    var error : [String]?
    var resCusine : [CusinesShop]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
        resCusine <- map["responseData"]

    }
}


struct CusinesShop : Mappable {
    var id : Int?
    var store_type_id : Int?
    var store_id : Int?
    var cuisines_id : Int?
    var name : String?
    var images : String?
    
    var cuisine : CusinesShopList?

    init(){}
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        store_type_id <- map["store_type_id"]
        store_id <- map["store_id"]
        cuisines_id <- map["cuisines_id"]
        cuisine <- map["cuisine"]
        name <- map["name"]
        images <- map["images"]
        
        
    }

}

struct CusinesShopList : Mappable {
    var id : Int?
    var store_type_id : Int?
    var name : String?
    var images : String?
    var status : Int?

    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        store_type_id <- map["store_type_id"]
        name <- map["name"]
        images <- map["images"]
        status <- map["status"]
        
    }

}




struct ShopsListData : Mappable {
    var id : Int?
    var store_type_id : Int?
    var company_id : Int?
    var store_name : String?
    var store_location : String?
    var latitude : Double?
    var longitude : Double?
    var picture : String?
    var offer_min_amount : Double?
    var estimated_delivery_time : String?
    var free_delivery : Int?
    var is_veg : String?
    var rating : Double?
    var offer_percent : Int?
    var categories : [Categories]?
    var shopstatus : String?
    var storetype : Storetype?
    var storepromo : [Coupon]?
    var store_cusinie : [CusinesShop]?
    var item_name : String?
    var store_id : Int?
    init(){
        
    }
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        store_type_id <- map["store_type_id"]
        company_id <- map["company_id"]
        store_name <- map["store_name"]
        store_location <- map["store_location"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        picture <- map["picture"]
        offer_min_amount <- map["offer_min_amount"]
        estimated_delivery_time <- map["estimated_delivery_time"]
        free_delivery <- map["free_delivery"]
        is_veg <- map["is_veg"]
        rating <- map["rating"]
        offer_percent <- map["offer_percent"]
        categories <- map["categories"]
        shopstatus <- map["shopstatus"]
        storetype <- map["storetype"]
        storepromo <- map["storepromo"]
        store_cusinie <- map["store_cusinie"]
        item_name <- map["item_name"]
        store_id <- map["store_id"]
    }
}

struct Categories : Mappable {
    
    var id : Int?
    var store_id : Int?
    var company_id : Int?
    var store_category_name : String?
    var store_category_description : String?
    var picture : String?
    var store_category_status : Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        store_id <- map["store_id"]
        company_id <- map["company_id"]
        store_category_name <- map["store_category_name"]
        store_category_description <- map["store_category_description"]
        picture <- map["picture"]
        store_category_status <- map["store_category_status"]
    }
}



class CouponListEntity: Mappable {
    
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : ResData?
    var error : [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
        

    }
}



struct ResData:Mappable {
    var coupons : [Coupon]?
    var cuisinelist : [CusinesShop]?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        
        coupons <- map["coupons"]
        cuisinelist <- map["cuisinelist"]
    }
}


struct Coupon : Mappable {
    
    var id : Int?
    var promo_code : String?
    var service : String?
    var store_type_id : Int?
    var store_id : Int?
    var picture : String?
    var percentage : Int?
    var max_amount : Int?
    var promo_description : String?
    var expiration : String?
    var status : String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        store_id <- map["store_id"]
        promo_code <- map["promo_code"]
        service <- map["service"]
        store_type_id <- map["store_type_id"]
        picture <- map["picture"]
        percentage <- map["percentage"]
        max_amount <- map["max_amount"]
        promo_description <- map["promo_description"]
        expiration <- map["expiration"]
        status <- map["status"]
    }
}




struct ShopDetailList : Mappable {
    
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : ShopDetailListData?
    var error : [String]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
    
}


struct ShopDetailListData : Mappable {
    var store_list : [ShopsListData]?
    var eatsmakehappy : [ShopsListData]?
    var toppicks : [ShopsListData]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        store_list <- map["store_list"]
        eatsmakehappy <- map["eatsmakehappy"]
        toppicks <- map["toppicks"]
    }
}
