//
//  NotificationController.swift
//  GoJekUser
//
//  Created by CSS01 on 22/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class NotificationController: UIViewController {
    
    @IBOutlet weak var notificationTableView:UITableView!
    
    
    var notificationData:[NotificationData] = []
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    var offSet: Int = 0
    var isUpdate = false
    var totalRecord = 0
    
    var isAppPresentTapOnPush:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if guestLogin() {
            self.initialLoads()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if guestLogin() {
            self.title = NotificationConstant.TNotification.localized
            //Webservice call
            getNotification()
        }
       // self.navigationController?.navigationBar.isHidden = false
        // Chat Notification
      //   NotificationCenter.default.addObserver(self, selector: #selector(isTransportChatRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_transport.rawValue), object: nil)
       
        showTabBar()
    }
}

//MARK: Methods

extension NotificationController {
    
    private func initialLoads() {
        self.setNavigationBar()
        self.notificationTableView.register(UINib(nibName: NotificationConstant.NotificationTableViewCell, bundle: nil), forCellReuseIdentifier: NotificationConstant.NotificationTableViewCell)
        self.view.backgroundColor = .white
        
    }
    
    private func setNavigationBar() {
        setNavigationTitle()
        self.title = NotificationConstant.TNotification.localized
    }
    
    private func getNotification() {
        self.notificationPresenter?.getNotification(param: [NotificationConstant.limit : 10, NotificationConstant.offset : offSet], isHideIndicator: isUpdate ? false : true)
    }
    
    
}

extension NotificationController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let height = cellHeights[indexPath] else { return 170.0 }
        
        return height
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationConstant.NotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
        cell.setValues(values: self.notificationData[indexPath.row])
        cell.showButton.addTarget(self, action: #selector(self.tapShowMoreLess(btn:)), for: .touchUpInside)
        cell.showButton.tag = indexPath.row
        cell.layoutSubviews()
        return cell
    }
    
    @objc func tapShowMoreLess(btn:UIButton) {
        let btnTag = btn.tag
        print(btnTag)
        let indexPath = IndexPath.init(row: btn.tag, section: 0)
        let cell = self.notificationTableView.cellForRow(at: indexPath) as! NotificationTableViewCell
        if cell.isShowMoreLess {
            cell.isShowMoreLess = false
            cell.notificationDetailLabel.numberOfLines = 3
            cell.showButton.setTitle(HomeConstant.showMore.localize(), for: .normal)
            
        }else{
            cell.isShowMoreLess = true
            cell.notificationDetailLabel.numberOfLines = 0
            cell.showButton.setTitle(HomeConstant.showLess.localize(), for: .normal)
            
        }
        notificationTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
        
        let lastCell = (self.notificationData.count) - 2
        if self.notificationData.count >= 10 {
            if indexPath.row == lastCell {
                if totalRecord != 0 {
                    self.isUpdate = true
                    offSet = offSet + (self.notificationData.count)
                    getNotification()
                }
            }
        }
    }
    
}

extension NotificationController:  NotificationPresenterToNotificationViewProtocol {
    
    func getNotification(notificationEntity: NotificationEntity) {
        if self.isUpdate  {
            if (notificationEntity.responseData?.notification?.count ?? 0) > 0
            {
                for i in 0..<(notificationEntity.responseData?.notification?.count ?? 0)
                {
                    let dict = notificationEntity.responseData?.notification?[i]
                    self.notificationData.append(dict!)
                }
            }
        }else{
            self.notificationData = notificationEntity.responseData?.notification ?? []
        }
        if self.notificationData.count == 0 {
            self.notificationTableView.setBackgroundImageAndTitle(imageName: NotificationConstant.noNotification, title: NotificationConstant.notificationEmpty.localized,tintColor: .black)
        }else{
            self.notificationTableView.backgroundView = nil
        }
        totalRecord  = notificationEntity.responseData?.total_records ?? 0
        
        
        self.notificationTableView.reloadData()
    }
    
}
/*
//MARK: CHAT NOTIFICATION
extension NotificationController {
@objc func isTransportChatRedirection() {
           if isAppPresentTapOnPush == false {
               isAppPresentTapOnPush = true
               print("idPush")
               let taxiHomeViewController = TaxiRouter.createTaxiModuleChat(rideTypeId: 0)
               navigationController?.pushViewController(taxiHomeViewController, animated: true)
           }
    }
    
 
}
*/
