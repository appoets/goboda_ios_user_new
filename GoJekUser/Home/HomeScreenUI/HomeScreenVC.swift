//
//  HomeScreenVC.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import GoogleMaps
import GooglePlaces

struct ServiceData {
    var name : BestServiceType
    var image : String
}

enum BestServiceType : String{
    case Taxi
    case Food
    case Movers
    case Grocery
    case Search
    
}


enum DisplayName : String{
    case DELIVERY
    case SERVICE
    case TRANSPORT
}

class HomeScreenVC: UIViewController {
    
    @IBOutlet weak var NavigationView : UIView!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var profileBtn : UIButton!
    @IBOutlet weak var locationView : UIView!
    @IBOutlet weak var selectedCityLbl : UILabel!
    @IBOutlet weak var homeTable : UITableView!
    @IBOutlet weak var liveTrackingView: UIView!
    @IBOutlet weak var liveCollectionView: UICollectionView!
    @IBOutlet weak var liveTrackingViewHeight: NSLayoutConstraint!
    
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var bannerData : BannerEntity?
    var shopArrList:[ShopsListData] = []
    var menuService: [ServicesDetails]?
    var serviceHeight = 0.0
    var mapViewHelper = GoogleMapsHelper()
  

    var bestService : [ServiceData] = [ServiceData(name: .Taxi, image: "Taxi"),ServiceData(name: .Food, image: "Foodie"),ServiceData(name: .Movers, image: "Movers"),ServiceData(name: .Grocery, image: "Grocery")]
    
    var storedOffsets : [Int] = [9,9,9,9]
    
    var promoCodeListArr:[PromocodeData] = []
    var promoCodeListArrGrocery:[PromocodeData] = []
    var promoCodeListArrFood:[PromocodeData] = []
    var promoCodeListArrXuber:[PromocodeData] = []
    var promoCodeListArrCourier:[PromocodeData] = []
    var promoCodeListArrTaxi:[PromocodeData] = []
    var bannerListArr : [BannerResponseData] = []
    var promoCodeDetailsListArr : PromocodeDetailsEntity?
    var isAppPresentTapOnPush:Bool = false
    var isFromOrderChat: Bool = false
    var isFromServiceChat: Bool = false
    var homeRes : HoneResponseData?
    var promoCode:[PromocodeData] = []
    var foodieCartList: CartListResponse?
    var storeTypeId : Int = 1
    var restaurantId : Int = 1
    var locationManager = CLLocationManager()
    var requestTimer : Timer?
    var allRequestCheck : [AllRequestResponse]?
    
    private var selectedLanguage: Language = .english {
        didSet {
            LocalizeManager.share.setLocalization(language: selectedLanguage)
        }
    }
    
    var XMapView: GMSMapView?
    
    var currentAddress = false {
        didSet {
            if currentAddress == true{
            self.mapViewHelper.getPlaceAddress(from: XCurrentLocation.shared.coordinate, on: { (locationDetail) in  // On Tapping
                print("Location>>",locationDetail)
                DispatchQueue.main.async {
                    self.selectedCityLbl.text = locationDetail.address
                }
            })
        }
    }
        
}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.requestTimer?.invalidate()
        self.requestTimer = nil
    }
         
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.liveTrackingViewHeight.constant = 0
        self.requestTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true, block: { (_) in
            if self.requestTimer != nil{
                self.homePresenter?.getAllRequest()
            }
        })
        self.navigationController?.isNavigationBarHidden = true
        showTabBar()
        self.getCart()
        self.getStoreList()
        
//        if XCurrentLocation.shared.currentAddress != nil {
//              self.selectedCityLbl.text = XCurrentLocation.shared.currentAddress
//        }else{
//            if let currentLoc = UserDefaults.standard.value(forKey: "currentLoc") {
//                   self.selectedCityLbl.text = currentLoc  as? String
//            }
//        }
 }
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialLoad()
        self.initalApi()
        self.initLocationManager()
        FLocationManager.shared.stop()
        // guestAccountLoadFirstCtiy()
//        DispatchQueue.main.async {
//            FLocationManager.shared.start { (info) in
//               print(info.longitude ?? 0.0)
//               print(info.latitude ?? 0.0)
//               print(info.address ?? 0.0)
//               XCurrentLocation.shared.currentAddress = info.address
//               self.selectedCityLbl.text = info.address ?? ""
//               UserDefaults.standard.setValue("\(info.address ?? "")", forKey: "currentLoc")
//               UserDefaults.standard.setValue("\(info.city ?? "")", forKey: "currentAdress")
////               self.navigationController?.popViewController(animated: true)
//       }
//    }
     
        self.addNotificationObservers()
        
    }
    
    func addNotificationObservers() {
           // push redirection
           NotificationCenter.default.addObserver(self, selector: #selector(isTransportRedirection), name: Notification.Name(rawValue: pushNotificationType.transport.rawValue), object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(isServicePushRedirection), name: Notification.Name(rawValue: pushNotificationType.service.rawValue), object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(isFoodiePushRedirection), name: Notification.Name(pushNotificationType.order.rawValue), object: nil)

           NotificationCenter.default.addObserver(self, selector: #selector(isTransportChatRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_transport.rawValue), object: nil)
             NotificationCenter.default.addObserver(self, selector: #selector(isFoodieChatPushRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_order.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(isServiceChatPushRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_servce.rawValue), object: nil)
        
    }
    
    
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    private func userCityDetails(cityID: String) {
        if isGuestAccount {
            guestAccountCity = cityID
            loadHomeDetil(cityId: cityID)
        } else {
            guestAccountCity = .empty
            let param: Parameters = [LoginConstant.city_id: cityID]
            homePresenter?.userCity(param: param)
        }
    }
    
    
    private func showCurrentCity () {
        self.selectedCityLbl.text = AppManager.shared.getUserDetails()?.country?.country_name ?? ""
        self.userCityDetails(cityID: "\(AppManager.shared.getUserDetails()?.country?.id ?? 0)")
    }
    
    func initialLoad(){
        self.setupAction()
        self.setupFont()
        self.registerCell()
        self.guestAccountLoadFirstCtiy()
        self.cartBtn.setImage( self.foodieCartList?.carts?.count ?? 0 > 0 ? #imageLiteral(resourceName: "Cart_Value").withTintColor(.appPrimaryColor) : #imageLiteral(resourceName: "ic_cartbag").withTintColor(.appPrimaryColor), for: .normal)
    }
    func setupAction(){
        self.cartBtn.addTap {
            let foodiecartVC = FoodieRouter.createCart(storeTypeId: self.storeTypeId, restaurantId: self.restaurantId)
            self.navigationController?.pushViewController(foodiecartVC, animated: true)
        
        }
        
        self.profileBtn.addTap {
            let foodiecartVC = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: AccountConstant.MyProfileController) as! MyProfileController
            self.navigationController?.pushViewController(foodiecartVC, animated: true)
            
        }
        self.locationView.addTap {
           let changeCityVC = HomeRouter.homeStoryboard.instantiateViewController(withIdentifier: HomeConstant.SaveLocationController) as! SaveLocationController
           changeCityVC.onLocationSelection = { location in
                self.mapViewHelper.getPlaceAddress(from: XCurrentLocation.shared.coordinate, on: { (locationDetail) in  // On Tapping
                    print("Location>>",locationDetail)
                })
                        
             }
         self.navigationController?.pushViewController(changeCityVC, animated: true)
        }
    }

    func setupFont(){
        self.selectedCityLbl.font = .setCustomFont(name: .medium, size: .x18)
        self.selectedCityLbl.textColor = .darkGray
    }
    
    func registerCell(){
        self.homeTable.register(nibName: "BestServiceCell")
        self.homeTable.register(nibName: "BestCategoryCell")
        self.homeTable.register(nibName: "StroreListCell")
        self.liveCollectionView.register(nibName: "LiveTrackCollectionViewCell")
        self.liveCollectionView.delegate = self
        self.liveCollectionView.dataSource = self
        self.homeTable.delegate = self
        self.homeTable.dataSource = self
        self.homeTable.register(UINib(nibName: HomeConstant.ServiceListCollectionTableViewCell, bundle: nil), forCellReuseIdentifier: HomeConstant.ServiceListCollectionTableViewCell)
    }
    
    func getCart(){
        self.homePresenter?.getCartList()
    }
    
    
}

extension HomeScreenVC : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestServiceCell", for: indexPath) as! BestServiceCell
            cell.bestService = bestService
            cell.navigateView = {[weak self] navigate in
                self?.reDirectScreens(serviceType: navigate.rawValue)
            }
           return cell
        }else if indexPath.row == 1{
            let serviceCell = tableView.dequeueReusableCell(withIdentifier: HomeConstant.ServiceListCollectionTableViewCell, for: indexPath) as! ServiceListCollectionTableViewCell
            serviceCell.delegate = self
            serviceCell.setServiceDataSource(services: menuService ?? [])
            return serviceCell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestCategoryCell", for: indexPath) as! BestCategoryCell
            cell.titleLbl.text = BestServiceType.Taxi.rawValue.uppercased()
            cell.promoCode = self.promoCodeListArrTaxi
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StroreListCell", for: indexPath) as! StroreListCell
            cell.titleLbl.text = "Restaurants List"
            cell.store = self.shopArrList
            cell.selectRest = {  [weak self] rest in
                let foodieItem = rest
                let vc = FoodieRouter.createFoodieItemModule(id:foodieItem.id ?? 0)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestCategoryCell", for: indexPath) as! BestCategoryCell
            cell.titleLbl.text = BestServiceType.Food.rawValue.uppercased()
            cell.promoCode = self.promoCodeListArrFood
            cell.isFromFood = true
            cell.navigateView = {[weak self] navigate in
                self?.reDirectScreens(serviceType: navigate.rawValue)
            }
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestCategoryCell", for: indexPath) as! BestCategoryCell
            cell.titleLbl.text = BestServiceType.Movers.rawValue.uppercased()
            cell.promoCode = self.promoCodeListArrCourier
            cell.navigateView = {[weak self] navigate in
                self?.reDirectScreens(serviceType: navigate.rawValue)
            }
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestCategoryCell", for: indexPath) as! BestCategoryCell
            cell.titleLbl.text = BestServiceType.Grocery.rawValue.uppercased()
            cell.isFromFood = false
            cell.promoCode = self.promoCodeListArrGrocery
            cell.navigateView = {[weak self] navigate in
                self?.reDirectScreens(serviceType: navigate.rawValue)
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestServiceCell", for: indexPath) as! BestServiceCell
           
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            let serviceCount : Int = self.bestService.count / 2
            let height = (((UIScreen.main.bounds.height / 10)*1.5) * CGFloat(serviceCount))
            return   height + CGFloat(((serviceCount * 10)+20))
        }else if indexPath.row == 1{
            return CGFloat(self.serviceHeight)
        }else if indexPath.row == 2{
            return self.promoCodeListArrTaxi.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2) + 60
        }else if indexPath.row == 3{
            return self.shopArrList.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2) + 60
        }else if indexPath.row == 4{
            return self.promoCodeListArrFood.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2) + 60
        }else if indexPath.row == 5{
            return self.promoCodeListArrCourier.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2) + 60
        }else if indexPath.row == 6{
            return self.promoCodeListArrGrocery.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2) + 60
        }
        return UITableView.automaticDimension
    }
    
}

extension HomeScreenVC{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: (((UIScreen.main.bounds.height / 10)*2.5)+80)))
        let headerCell = HomeSearchOfferView.getView()
        headerCell.promoCode = self.bannerData?.responseData ?? []
        headerCell.navigateView = {[weak self] navigate in
            self?.reDirectScreens(serviceType: navigate.rawValue)
        }
        headerCell.frame = headerView.bounds
        headerView.addSubview(headerCell)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (((UIScreen.main.bounds.height / 10)*2.5)+80)
    }
}

extension HomeScreenVC : HomeAddMoreDelegate{
    func tapShowMore(height: Double) {
        UIView.animate(withDuration: 1.0, animations: {
            self.serviceHeight = height
            DispatchQueue.main.async {
                self.homeTable.reloadData()
                self.homeTable.layoutIfNeeded()
                self.homeTable.contentOffset = CGPoint.zero
            }
        })
    }
    
    func callCheckRequest(isFlowRequest: String, service: ServicesDetails?) {
        if Flow.foodie == isFlowRequest {
            foodieRedirect(service: service)
        }else if Flow.service == isFlowRequest {
            xuberRedirect()
        }else if Flow.courier == isFlowRequest {
            showCourierRequest()
        }
    }
    private func foodieRedirect(service:ServicesDetails?) {
//        if isGuestAccount {
            if service?.title == "Food" {
                let vc = FoodieRouter.createFoodieHomeModule(id: service?.menu_type_id ?? 0)
                    UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = FoodieRouter.createFoodieModule(Id: service?.menu_type_id ?? 0)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            }
          
//        }else {
//            homePresenter?.getCheckRequest()
//        }
    }
    private func xuberRedirect() {
        if isGuestAccount {
            xuberSelectionViewRedirect()
        }else {
            xuberSelectionViewRedirect()
           // homePresenter?.getXuberRequest()
        }
    }
    private func xuberSelectionViewRedirect() {
        let vc = XuberRouter.createXuberServiceModule()
        let serviceDetail = AppManager.shared.getSelectedServices()
        SendRequestInput.shared.mainServiceId = serviceDetail?.menu_type_id ?? 0
        SendRequestInput.shared.mainSelectedService = serviceDetail?.title ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
}



extension HomeScreenVC : HomePresenterToHomeViewProtocol{
    func showHomeDetails(details: HomeEntity) {
        let menuservice = details.responseData?.services ?? [ServicesDetails]()
        homeRes = details.responseData
        self.menuService = menuservice
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 1, section: 0)
            self.homeTable.reloadRows(at: [indexPath], with: .top)
        }
    }
    
    func getAllRequest(AllRequestResponse: AllRequest) {
        if AllRequestResponse.responseData?.count ?? 0 > 0{
            self.liveTrackingView.isHidden = false
            self.liveTrackingViewHeight.constant = 80
        self.allRequestCheck = AllRequestResponse.responseData
        self.liveCollectionView.reloadInMainThread()
        }else{
            self.liveTrackingView.isHidden = true
        }
    }
    
    
    //get Menu
    private func loadHomeDetil(cityId: String) {
        let param: Parameters = [LoginConstant.city_id: cityId]
        homePresenter?.getHomeDetails(param: param)
    }
    
    
    // getPromoCode
    func getPromoCode(){
        homePresenter?.getPromoCodeDetailsList()
    }
    
    
    func getPromoList(){
        self.homePresenter?.getPromoCodeListNew()
    }
    
    func getStoreList(){
        FLocationManager.shared.stop()
        FLocationManager.shared.start { (info) in
             print(info.longitude ?? 0.0)
             print(info.latitude ?? 0.0)
       
            let param: Parameters = ["latitude":info.latitude ?? 0.0,
                                            "longitude":info.longitude ?? 0.0]
            self.homePresenter?.getListOfStores(Id: 1, param: param)
        }
    }
    func getBannerList(){
        homePresenter?.getbannerResponse()
    }
    
    func initalApi(){
        homePresenter?.fetchUserProfileDetails()
        self.getPromoCode()
        self.getPromoList()
        self.getBannerList()
        
    }
    
    func getPromoCodeNewResponse(getPromoCodeResponse: PromocodeDetailsEntity) {
        self.promoCode = getPromoCodeResponse.responseData ?? []
        guard let promoCodeValue = getPromoCodeResponse.responseData else { return  }
        for (_,value) in promoCodeValue.enumerated(){
            if value.name == "Food" || value.name == "Restaurant"{
                for promoCode in value.promocode ?? []{
                    self.promoCodeListArrFood.append(promoCode)
                }
            }else if value.name == "Grocery"{
                for promoCode in value.promocode ?? []{
                    self.promoCodeListArrGrocery.append(promoCode)
                }
            }
        }
        
        DispatchQueue.main.async {
            self.homeTable.reloadData()
        }
    }
    func getbannerResponse(details: BannerEntity) {
        print("details>.",details)
        bannerData = details
        
    }
    
    func getCartListResponse(cartListEntity: FoodieCartListEntity) {
        self.foodieCartList = cartListEntity.responseData
        self.cartBtn.isEnabled = self.foodieCartList?.carts?.count ?? 0 > 0 ? true : false
        self.cartBtn.setImage( self.foodieCartList?.carts?.count ?? 0 > 0 ? #imageLiteral(resourceName: "Cart_Value").withTintColor(.appPrimaryColor) : #imageLiteral(resourceName: "ic_cartbag").withTintColor(.appPrimaryColor), for: .normal)
        self.storeTypeId = self.foodieCartList?.carts?.first?.store?.store_type_id ?? 1
        self.restaurantId = self.foodieCartList?.carts?.first?.storeId ?? 1
    }

        
    func getPromoCodeListResponse(getPromoCodeResponse: PromocodeDetailsEntity) {
                print(getPromoCodeResponse)
                promoCodeListArrTaxi.removeAll()
                promoCodeListArrCourier.removeAll()
        self.promoCodeDetailsListArr = getPromoCodeResponse
        guard let promoCodeValue = getPromoCodeResponse.responseData else { return  }
        for (index,value) in promoCodeValue.enumerated(){
            switch value.serviceDict?.display_name {
            case DisplayName.DELIVERY.rawValue:
                promoCodeListArrCourier.append(promoCodeValue[index])
            case DisplayName.SERVICE.rawValue:
                promoCodeListArrXuber.append(promoCodeValue[index])
            case DisplayName.TRANSPORT.rawValue:
                promoCodeListArrTaxi.append(promoCodeValue[index])
            default:
                break
            }
        }
        self.homeTable.reloadData()
    }
    
    func checkCourierRequest(requestEntity: Request) {
        if requestEntity.responseData?.data?.count == 0 {
            let vc = CourierRouter.createCourierModule()
            vc.hidesBottomBarWhenPushed = true
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let courierHomeVC = CourierRouter.createCourierHomeModule(isNewOrder: true)
            courierHomeVC.hidesBottomBarWhenPushed = true
            UIApplication.topViewController()?.navigationController?.pushViewController(courierHomeVC, animated: true)
        }
    }
    
    func xuberCheckRequest(xuberReesponse: XuberRequestEntity) {
        if (xuberReesponse.request?.data?.count ?? 0) > 0 {
            let vc = XuberRouter.createXuberModule(isChat: isFromServiceChat)
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
           // navigationController?.pushViewController(vc, animated: true)
        }else {
            xuberSelectionViewRedirect()
        }
    }
    
    func showUserProfileDtails(details: UserProfileResponse) {
        var userDetails:UserProfileEntity = UserProfileEntity()
        userDetails = details.responseData ?? UserProfileEntity()
        
        DispatchQueue.main.async {
//            self.selectedCityLbl.text = (userDetails.city?.city_name ?? "")
            if !CommonFunction.checkisRTL() {
                UserDefaults.standard.set(userDetails.language, forKey: AccountConstant.language)
                self.localizable()
            }
        }
        self.loadHomeDetil(cityId: "\(userDetails.id ?? 0)")
        AppManager.shared.setUserDetails(details: userDetails)
        homePresenter?.getSavedAddress()
    }
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) {
        print(getStoreResponse)

        shopArrList = getStoreResponse.responseData ?? []
        
        homeTable.reloadData()


    }
    
//    func getCheckRequestResponse(checkRequestEntity: FoodieCheckRequestEntity) {
//        let foodiecartVC = FoodieRouter.createFoodieOrderStatusModule(isHome: true, orderId: checkRequestEntity.responseData?.data?.first?.id ?? 0,isChat: isFromOrderChat)
//          UIApplication.topViewController()?.navigationController?.pushViewController(foodiecartVC, animated: true)
//    }
    

}

extension HomeScreenVC{
    func reDirectScreens(serviceType : String){
        switch serviceType {
        case BestServiceType.Search.rawValue:
            if guestLogin() {
              let homesearch =  HomeRouter.homeStoryboard.instantiateViewController(withIdentifier: HomeConstant.Foodie1SearchViewController) as! Foodie1SearchViewController
                homesearch.isFoodieCatgory = true
               navigationController?.pushViewController(homesearch, animated: true)
            }
               
            break
        case BestServiceType.Food.rawValue:
            if guestLogin() {
                let vc = FoodieRouter.createFoodieHomeModule(id: self.homeRes?.food ?? 0)
                for value in self.menuService ?? []{
                    if value.title == "Food"{
                        AppManager.shared.setSelectedServices(service: value)
                    }
                }
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            }
             break
        case BestServiceType.Movers.rawValue:
            self.showCourierRequest()
            break
        case BestServiceType.Grocery.rawValue:
            if guestLogin() {
                let vc = FoodieRouter.createFoodieModule(Id: self.homeRes?.grocery ?? 0)
                for value in self.menuService ?? []{
                    if value.title == "Grocery"{
                        AppManager.shared.setSelectedServices(service: value)
                    }
                }
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            }
            break
        case BestServiceType.Taxi.rawValue:
            if guestLogin() {
                let taxiHomeViewController = TaxiRouter.createTaxiModule(rideTypeId: self.homeRes?.taxi ?? 0,isfromHome: true)
                for value in self.menuService ?? []{
                    if value.title == "Taxi"{
                        AppManager.shared.setSelectedServices(service: value)
                    }
                }
                navigationController?.pushViewController(taxiHomeViewController, animated: true)
            }
            break
        default:
            break
        }
        
    }
    
    private func showCourierRequest(){
         if isGuestAccount {
            let vc = CourierRouter.createCourierModule()
            for value in self.menuService ?? []{
                if value.title == "Courier"{
                    AppManager.shared.setSelectedServices(service: value)
                }
            }
              UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
         }else{
            let vc = CourierRouter.createCourierModule()
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//            let courierHomeVC = CourierRouter.createCourierHomeModule(isNewOrder: true)
//            courierHomeVC.hidesBottomBarWhenPushed = true
//            UIApplication.topViewController()?.navigationController?.pushViewController(courierHomeVC, animated: true)
////            homePresenter?.checkCourierRequest()
        }
    }
}


extension HomeScreenVC{
    
        private func guestAccountLoadFirstCtiy() {
            if isGuestAccount == false{
                let baseEntity = AppConfigurationManager.shared.baseConfigModel.responseData
                let countryArray = AppManager.shared.getCountries()
                let countryCode = AppManager.shared.getUserDetails()?.country?.id ?? baseEntity?.appsetting?.country
                let cityArray = countryArray?.filter({$0.id == countryCode}).first
               // selectedCityLbl.text = cityArray?.city?.first?.city_name
                guard let cityId = cityArray?.city?.first?.id else {
                    return
                }
                guestAccountCity = "\(cityId)"
                loadHomeDetil(cityId: "\(cityId)")
            }
            
//            FLocationManager.shared.stop()
//            FLocationManager.shared.start { (info) in
//                if XCurrentLocation.shared.currentAddress != nil {
//                     self.selectedCityLbl.text = XCurrentLocation.shared.currentAddress
//                  }else{
//                     self.selectedCityLbl.text = info.city
//                   }
//
//
//                XCurrentLocation.shared.latitude = info.latitude
//                XCurrentLocation.shared.longitude = info.longitude
//                XCurrentLocation.shared.currentAddress = info.address
//                //print("adres>>",info.address)
//                //let location = CLLocationCoordinate2D(latitude: XCurrentLocation.shared.latitude ?? 0.0, longitude: XCurrentLocation.shared.longitude ?? 0.0)
//
//                UserDefaults.standard.setValue("\(info.city ?? "")", forKey: "currentLoc")
//
//            }
        }
    private func localizable() {
        if let languageStr = UserDefaults.standard.value(forKey: AccountConstant.language) as? String, let language = Language(rawValue: languageStr) {
            LocalizeManager.share.setLocalization(language: language)
            selectedLanguage = language
            
        }else {
            LocalizeManager.share.setLocalization(language: .english)
        }
        if !CommonFunction.isAppAlreadyLaunchedOnce() {
            if self.selectedLanguage == .arabic {
                //  showTabBar()
                self.view.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                
            }else {
                view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
            DispatchQueue.main.async {
                self.homeTable.reloadData()
            }
        }
        if !CommonFunction.checkisRTL() {
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
        showTabBar()
    }
}


extension HomeScreenVC {
    
    // For Transport push redirection
    @objc func isTransportRedirection() {
        if isAppPresentTapOnPush == false {
            isAppPresentTapOnPush = true
            let taxiHomeViewController = TaxiRouter.createTaxiModule(rideTypeId: 0, isfromHome: true)
            navigationController?.pushViewController(taxiHomeViewController, animated: true)
        }
    }
    
    //For service push redirection
    @objc func isServicePushRedirection() {
        if isAppPresentTapOnPush == false {
            isAppPresentTapOnPush = true
             isFromServiceChat = false
            homePresenter?.getXuberRequest()
        }
    }
    
   //For foodie(order) push redirection
    @objc func isFoodiePushRedirection() {
        if isAppPresentTapOnPush == false {
            isAppPresentTapOnPush = true
            isFromOrderChat = false
            homePresenter?.getCheckRequest()
//            let foodieHomeViewController = FoodieRouter.createFoodieModule()
//            navigationController?.pushViewController(foodieHomeViewController, animated: true)
        }
    }

    @objc func isTransportChatRedirection() {
        if ChatPushClick.shared.isPushClick == false {
             ChatPushClick.shared.isPushClick = true
            print("idPush")
            let taxiHomeViewController = TaxiRouter.createTaxiModuleChat(rideTypeId: 0)
             UIApplication.topViewController()?.navigationController?.pushViewController(taxiHomeViewController, animated: true)
           // navigationController?.pushViewController(taxiHomeViewController, animated: true)
        }
    }

    @objc func isFoodieChatPushRedirection() {
        if  ChatPushClick.shared.isOrderPushClick == false {
            ChatPushClick.shared.isOrderPushClick = true
            isFromOrderChat = true
             homePresenter?.getCheckRequest()
          }
      }
    @objc func isServiceChatPushRedirection() {
        if  ChatPushClick.shared.isServicePushClick == false {
            ChatPushClick.shared.isServicePushClick = true
            isFromServiceChat = true
            homePresenter?.getXuberRequest()
        }
    }
}


extension HomeScreenVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
                locationManager.stopUpdatingLocation()
                if ((error) != nil) {
                    if (seenError == false) {
                        seenError = true
                       print(error)
                    }
                }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (locationFixAchieved == false) {
            locationFixAchieved = true
        }
        //Location update to storyboard
        XCurrentLocation.shared.latitude = locations.last?.coordinate.latitude
        XCurrentLocation.shared.longitude = locations.last?.coordinate.longitude
        currentAddress = true
//        currentAddress.latitude = locations.last?.coordinate.latitude ?? 0
//        currentAddress.longitude = locations.last?.coordinate.longitude ?? 0
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
           
           switch status {
           case .restricted:
               print("Location access was restricted.")
           case .authorizedAlways:
               self.locationManager.startUpdatingLocation()
           case .authorizedWhenInUse:
               self.locationManager.startUpdatingLocation()
           case .notDetermined:
               self.locationManager.requestAlwaysAuthorization()
           case .denied:
               print("User denied access to location.")
           @unknown default: break
            
           }
       }
    }


extension HomeScreenVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allRequestCheck?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveTrackCollectionViewCell", for: indexPath) as! LiveTrackCollectionViewCell
        cell.setImageandValues(adminService: self.allRequestCheck?[indexPath.row].admin_service ?? "", Status: self.allRequestCheck?[indexPath.row].status ?? "")
        cell.addTap {
            switch self.allRequestCheck?[indexPath.row].admin_service ?? "" {
            case "TRANSPORT":
                 let vc = TaxiRouter.createLiveTaxiModule(rideTypeId: self.allRequestCheck?[indexPath.row].request_id ?? 0, isfromLive: true, requestID: self.allRequestCheck?[indexPath.row].request_id ?? 0)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            
            case "ORDER" :
                let vc = FoodieRouter.createFoodieOrderStatusModule(isHome: true, orderId: self.allRequestCheck?[indexPath.row].request_id ?? 0, isChat: false)//createFoodieOrderStatusModule(isHome: Bool?, orderId: Int?,isChat: Bool?)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            case "DELIVERY":
                if let vc = CourierRouter.createCourierHomeModule(isNewOrder: false) as? CourierHomeController {
                    vc.isFromOrderPage = true
                    vc.isFromLiveTrack = true
                    vc.isIndexSet = false
                    vc.liveTrackID = self.allRequestCheck?[indexPath.row].request_id ?? 0
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                    default:
                        let vc = XuberRouter.createXuberServiceModuleFromOrder(index: indexPath.row, isFromOrderPage: true, isIndexSet: true) as! XuberHomeController
                    vc.liveTrackID = self.allRequestCheck?[indexPath.row].request_id ?? 0
                    self.navigationController?.pushViewController(vc, animated: true)
            }
        }

        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.liveTrackingView.frame.width - 10, height: self.liveTrackingView.frame.height - 5)
    }
    
     func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let collectionViewCell = cell as? LiveTrackCollectionViewCell else { return }
        print("asdfgt",indexPath.row)

    }
//
     func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let collectionViewCell = cell as? LiveTrackCollectionViewCell else { return }
        print("asdfgt12345678",indexPath.row)

    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if self.allRequestCheck?.count ?? 0 > 0{
//            var lastIndex = liveCollectionView.indexPath(for: liveCollectionView.visibleCells.first!)?.row ?? 0
//        for cell in liveCollectionView.visibleCells {
//            let indexPath = liveCollectionView.indexPath(for: cell)
//            if lastIndex < (indexPath?.row ?? 0){
//                lastIndex = (indexPath?.row ?? 0)
//            }
//        }
//        
//        liveCollectionView.scrollToItem(at: IndexPath(row: lastIndex, section: 0) , at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
//
//    }
// }
}


