//
//  HomeSearchOfferView.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class HomeSearchOfferView: UIView {
    
    @IBOutlet weak var searchView : UIView!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet weak var offerCollection: UICollectionView!

    var promoCode : [BannerResponseData] = []{
        didSet{
            self.offerCollection.reloadData()
        }
    }
    
    var navigateView : ((BestServiceType) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialLoad()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}

extension HomeSearchOfferView{
    func initialLoad(){
        self.setupView()
        self.setupAction()
        self.setupCollectionCell()
        
    }
    
    func setupView(){
        self.searchView.addShadow(radius: 10, color: .gray)
    }
    
    func setupAction(){
        self.searchBtn.addTap {
            self.navigateView?(.Search)
        }
    }

    static func getView() -> HomeSearchOfferView{
        let View = Bundle.main.loadNibNamed("HomeSearchOfferView", owner: self, options: [:])?.first as? HomeSearchOfferView
        return View!
    }
}

extension HomeSearchOfferView : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.promoCode.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCellNew", for: indexPath) as! BannerCellNew
        cell.promoData = self.promoCode[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch  (self.promoCode[indexPath.row].name ?? ""){
            case "Grocery":
                self.navigateView?(.Grocery)
            break
            case "Food":
                self.navigateView?(.Food)
            break
            case "Taxi":
                self.navigateView?(.Taxi)
            break
            default:
                self.navigateView?(.Movers)
            break
        }
        
    }
    
    func setupCollectionCell(){
        self.offerCollection.delegate = self
        self.offerCollection.dataSource = self
        self.offerCollection.registerCell(withId: "BannerCellNew")
    }
}

extension HomeSearchOfferView : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 45, height: (UIScreen.main.bounds.height / 10)*2.5)
    }
}
