//
//  LiveTrackCollectionViewCell.swift
//  Goboda
//
//  Created by Sethuram Vijayakumar on 27/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class LiveTrackCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var trackingIdLabel: UILabel!
    @IBOutlet weak var trackStatus: UILabel!
    @IBOutlet weak var contentBGView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setShadow()
        self.contentBGView.backgroundColor = UIColor.opaqueSeparator.withAlphaComponent(0.5)
        self.trackStatus.textColor = .white
    }

}

extension LiveTrackCollectionViewCell{
    
    private func setShadow(){
        self.contentView.setCorneredElevation(shadow: 1, corner: 1, color: .appPrimaryColor, clipstobound: false)
        self.contentView.setCornerRadius()
    }
    
     func setImageandValues(adminService:String,Status:String){
        switch adminService {
        case "TRANSPORT":
            self.trackImage.image = UIImage(named: "Live_Taxi")
            switch Status {
            case "STARTED":
                self.trackStatus.text = "Driver Has Accepted Your Request"
            case "ARRIVED":
                self.trackStatus.text = "Driver Has Arrived To Your Location"
            case "PICKEDUP" :
                self.trackStatus.text = "You are In Ride"
            case "DROPPED" :
                self.trackStatus.text = "Ride Completed Proceed to Pay"
            default:
                self.trackStatus.text = "Your Ride is \(Status)"
            }
        case "ORDER":
            self.trackImage.image = UIImage(named: "Live_Foodie")
            switch Status {
            case "ORDERED":
                self.trackStatus.text = "Shop Recieved Your Order"
            case "SEARCHING":
                self.trackStatus.text = "Shop Processing Your Order"
            case "STARTED":
                self.trackStatus.text = "Delivery Person Assigned"
            case "REACHED":
                self.trackStatus.text = "Delivery Person Reached Shop"
            case "PICKEDUP" :
                self.trackStatus.text = "Delivery Person Picked Up Your Order"
            case "ARRIVED":
                self.trackStatus.text = "Order Delivery Sucessfull"
            default:
                self.trackStatus.text = "Your Order is \(Status)"
            }
        case "DELIVERY":
            self.trackImage.image = UIImage(named: "Live_Courier")
            switch Status {
            case "STARTED":
                self.trackStatus.text = "Driver Has Accepted Your Request"
            case "ARRIVED":
                self.trackStatus.text = "Driver Has Arrived To Your Location"
            case "PICKEDUP" :
                self.trackStatus.text = "You are In Ride"
            case "COMPLETED" :
                self.trackStatus.text = "Ride Completed"
            default:
                self.trackStatus.text = "Your Ride is \(Status)"
            }
        default:
            self.trackImage.image = UIImage(named: "Live_Services")
            switch Status {
            case "ACCEPTED":
                self.trackStatus.text = "Service Provider Has Accepted Your Request"
            case "ARRIVED":
                self.trackStatus.text = "Service Provider Has Arrived To Your Location"
            case "PICKEDUP" :
                self.trackStatus.text = "Service Has Started"
            case "DROPPED" :
                self.trackStatus.text =  "Service is Completed"
            case "COMPLETED":
                self.trackStatus.text =  "Rate Your Service"
            default:
                self.trackStatus.text = "Service is \(Status)"
            }
        }
    }
    
    
    
    
}
