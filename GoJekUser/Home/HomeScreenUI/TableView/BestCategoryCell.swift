//
//  BestCategoryCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class BestCategoryCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var categoruCollection : UICollectionView!
    var isFromFood:Bool = false
    var navigateView : ((BestServiceType) -> Void)?
    
    var promoCode : [PromocodeData] = []{
        didSet{
            self.categoruCollection.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
        self.setupCollectionCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension BestCategoryCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.promoCode.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestServiceCollectionCell", for: indexPath) as! BestServiceCollectionCell
        cell.promoData = self.promoCode[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch  (self.promoCode[indexPath.row].serviceDict?.display_name ?? ""){
        case DisplayName.DELIVERY.rawValue:
                self.navigateView?(.Movers)
            break
        case DisplayName.TRANSPORT.rawValue:
                self.navigateView?(.Taxi)
            break
            default:
                if isFromFood{
                    self.navigateView?(.Food)
                }else{
                    self.navigateView?(.Grocery)
                }
             
            break
        }
    }

    func setupCollectionCell(){
        self.categoruCollection.delegate = self
        self.categoruCollection.dataSource = self
        self.categoruCollection.registerCell(withId: "BestServiceCollectionCell")
    }
}

extension BestCategoryCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width) - 50, height: (UIScreen.main.bounds.height / 10)*2)
    }
}
