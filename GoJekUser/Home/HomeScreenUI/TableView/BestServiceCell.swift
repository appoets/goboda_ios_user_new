//
//  BestServiceCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class BestServiceCell: UITableViewCell {
    
    @IBOutlet weak var bestServiceCollection : UICollectionView!
    
    
    var navigateView : ((BestServiceType) -> Void)?
    
    var bestService : [ServiceData]?{
        didSet{
            self.bestServiceCollection.reloadData()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionCell()
        self.bestServiceCollection.delegate = self
        self.bestServiceCollection.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
extension BestServiceCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bestService?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestServiceCollectionCell", for: indexPath) as! BestServiceCollectionCell
        cell.serviceImage.image = UIImage(named:  self.bestService?[indexPath.row].image ?? "Taxi")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigateView?(self.bestService?[indexPath.row].name ?? .Search)
    }

    
    func setupCollectionCell(){
        self.bestServiceCollection.delegate = self
        self.bestServiceCollection.dataSource = self
        self.bestServiceCollection.registerCell(withId: "BestServiceCollectionCell")
    }
}

extension BestServiceCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width / 2) - 25, height: (UIScreen.main.bounds.height / 10)*1.5)
    }
}
