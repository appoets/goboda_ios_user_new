//
//  StroreListCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class StroreListCell: UITableViewCell {

    
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var storeCollection : UICollectionView!
    
    var selectRest : ((ShopsListData) -> Void)?
    
    var store : [ShopsListData] = [ShopsListData](){
        didSet{
            storeCollection.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
        self.setupCollectionCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

extension StroreListCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.store.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell", for: indexPath) as! StoreCell
        cell.store = self.store[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectRest?(self.store[indexPath.row])
    }
    
    func setupCollectionCell(){
        self.storeCollection.delegate = self
        self.storeCollection.dataSource = self
        self.storeCollection.registerCell(withId: "StoreCell")
    }
}

extension StroreListCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width/3), height: (UIScreen.main.bounds.height / 10)*2)
    }
}
