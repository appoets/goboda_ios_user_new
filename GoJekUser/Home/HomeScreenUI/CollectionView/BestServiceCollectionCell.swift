//
//  BestServiceCollectionCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class BestServiceCollectionCell: UICollectionViewCell {
   
    @IBOutlet weak var serviceImage : UIImageView!
    @IBOutlet weak var serviceView : UIView!

    var promoData : PromocodeData = PromocodeData(){
        didSet{
            self.serviceImage.sd_setImage(with: URL(string: self.promoData.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.serviceImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.serviceImage.image = image
                           }
                       })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.serviceView.addShadow(radius: 8, color: .lightGray)
        self.serviceView.clipsToBounds = true
    }
}
