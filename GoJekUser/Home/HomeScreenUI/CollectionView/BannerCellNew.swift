//
//  OfferCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class BannerCellNew: UICollectionViewCell {
    
    @IBOutlet weak var offerImage : UIImageView!
    @IBOutlet weak var offerView : UIView!
    
    var promoData : BannerResponseData = BannerResponseData(){
        didSet{
            self.offerImage.sd_setImage(with: URL(string: self.promoData.banner_image ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.offerImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.offerImage.image = image
                           }
                       })
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.offerView.addShadow(radius: 20, color: .lightGray)
        self.offerView.clipsToBounds = true
    }

}
