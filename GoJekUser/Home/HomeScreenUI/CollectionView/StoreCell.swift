//
//  StoreCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 11/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class StoreCell: UICollectionViewCell {

    @IBOutlet weak var storeImag : UIImageView!
    @IBOutlet weak var storeNameLbl : UILabel!
    @IBOutlet weak var ratingView : FloatRatingView!
    
    var store : ShopsListData = ShopsListData(){
        didSet{
            self.storeNameLbl.text = store.store_name
            self.ratingView.rating = Double(store.rating ?? 0.0)
            self.storeImag.sd_setImage(with: URL(string: store.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.storeImag.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.storeImag.image = image
                           }
                       })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.storeImag.addShadow(radius: 10, color: .gray)
        self.storeNameLbl.font = .setCustomFont(name: .bold, size: .x20)
        self.storeNameLbl.textColor = .black
    }

}
