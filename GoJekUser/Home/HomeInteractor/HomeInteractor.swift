//
//  HomeInteractor.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class HomeInteractor: HomePresenterToHomeInteractorProtocol {
  
 
    
    var homePresenter: HomeInteractorToHomePresenterProtocol?
    
    
    func getListOfStores(Id: Int, param: Parameters) {
        
        WebServices.shared.requestToApi(type: StoreListEntity.self, with: "\(FoodieAPI.storeList)/\(Id)", urlMethod: .get, showLoader: true, params: param,encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getListOfStoresResponse(getStoreResponse: response)
            }
        }
    }
    
    func searchRestaurantList(id: Int,type: String,searchStr: String,param: Parameters) {
        let url =  FoodieAPI.foodieSearch + "/" + id.toString()
        let urlString = "?q=" + searchStr + "&t=" + type
        let WeburlString = url + urlString
        
        print("Webur1lString>>>",WeburlString)
        WebServices.shared.requestToApi(type: SearchEntity.self, with: WeburlString, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.searchRestaturantResponse(getSearchRestuarantResponse: response)
            }
        }
    }
    
    
    func getbannerResponse() {
        WebServices.shared.requestToApi(type: BannerEntity.self, with: FoodieAPI.BannerApi, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value  {
                self.homePresenter?.getbannerResponse(details: response)
            }
        }
    }
    
    
    //MARK:- Get User Details
    func fetchUserProfileDetails() {
        
        WebServices.shared.requestToApi(type: UserProfileResponse.self, with: AccountAPI.getProfile, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value  {
                self.homePresenter?.showUserProfileDtails(details: response)
            }
        }
    }
    //MARK:- Promoceode List

    func getPromoCodeDetailsList() {
        WebServices.shared.requestToApi(type: PromocodeDetailsEntity.self, with: HomeAPI.promocodeList, urlMethod: .get, showLoader: false, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getPromoCodeListResponse(getPromoCodeResponse: response)
            }
        }
    }
    
    
    
    //MARK:- get saved address
    func getSavedAddress() {
        
        WebServices.shared.requestToApi(type: SavedAddressEntity.self, with: AccountAPI.getAddress, urlMethod: .get, showLoader: false, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.savedAddressSuccess(addressEntity: response)
            }
        }
    }
    
    
    //MARK: Get Home Details
    func getHomeDetails(param: Parameters) {
        WebServices.shared.requestToApi(type: HomeEntity.self, with: HomeAPI.getHomeDetails, urlMethod: .get, showLoader: false, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.showHomeDetails(details: response)
            }
        }
    }
    
    func userCity(param: Parameters) {
        WebServices.shared.requestToApi(type: UserCity.self, with: AccountAPI.userCity, urlMethod: .post, showLoader: true, params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.showUserCity(selectedCityDetails: response)
            }
        }
    }
    func getCheckRequest() {
        WebServices.shared.requestToApi(type: FoodieCheckRequestEntity.self, with: HomeAPI.checkRequest, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getCheckRequestResponse(checkRequestEntity: response)
                
            }
        }
    }
    
    func checkCourierRequest(){
           WebServices.shared.requestToApi(type: Request.self, with: CourierAPI.checkRequest, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
               guard let self = self else {
                   return
               }
               if let response  = response?.value {
                   self.homePresenter?.checkCourierRequest(requestEntity: response)
               }
           }
       }
    
    func getPromoCodeList(){
        WebServices.shared.requestToApi(type: PromocodeEntity.self, with: HomeAPI.promocode, urlMethod: .get, showLoader: false, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getPromoCodeResponse(getPromoCodeResponse: response)
            }
        }
    }
    
    //MARK: - Chat
    func getUserChatHistory(param: Parameters) {
        WebServices.shared.requestToApi(type: ChatEntity.self, with: AccountAPI.userChat, urlMethod: .get, showLoader: true, params: param, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getUserChatHistoryResponse(chatEntity: response)
            }
        }
    }
    
    func getXuberRequest() {
        WebServices.shared.requestToApi(type: XuberRequestEntity.self, with: XuberAPI.checkRequest, urlMethod: .get, showLoader: true, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.xuberCheckRequest(xuberReesponse: response)
            }
        }
    }
    
    func getPromoCodeListNew() {
        WebServices.shared.requestToApi(type: PromocodeDetailsEntity.self, with: HomeAPI.promocode_List, urlMethod: .get, showLoader: false, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getPromoCodeNewResponse(getPromoCodeResponse: response)
            }
        }
    }
    
    
    func getCityData(Id: Int, param: Parameters) {
        
        WebServices.shared.requestToApi(type: HomeEntity.self, with: HomeAPI.getCity, urlMethod: .post, showLoader: false, params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getCityResponse(details: response)
            }
        }
    }
    
    func getCartList(){
        WebServices.shared.requestToApi(type: FoodieCartListEntity.self, with: FoodieAPI.cartList, urlMethod: .get, showLoader: true, params: nil, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getCartListResponse(cartListEntity: response)
            }
        }
    }
    
    func getAllRequest() {
        WebServices.shared.requestToApi(type: AllRequest.self, with: HomeAPI.allOrders, urlMethod: .get, showLoader: false, params: nil, encode: URLEncoding.default) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.homePresenter?.getAllRequest(AllRequestResponse: response)
            }
        }
    }
    
    
 
}
