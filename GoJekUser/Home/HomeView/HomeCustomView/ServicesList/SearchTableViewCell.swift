//
//  SearchTableViewCell.swift
//  Goboda
//
//  Created by vamsikrishna surathi on 13/03/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtFieldLocation: UITextField!
    @IBOutlet weak var viewBg: UIView!

    @IBOutlet weak var searchBar: UISearchBar!
    var tapOnLocation : (()->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}



extension SearchTableViewCell {
    
    
    func initialLoads() {
        
    }

    
}


extension SearchTableViewCell : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.tapOnLocation?()
        textField.endEditing(true)
    }
}
