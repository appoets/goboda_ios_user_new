//
//  BannerCell.swift
//  Goboda
//
//  Created by Mithra Mohan on 19/03/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class BannerCell: UICollectionViewCell {

    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.bannerImage.layer.cornerRadius = 10
        self.titleLabel.backgroundColor = .appPrimaryColor
        self.titleLabel.layer.cornerRadius = 4

    }

}


extension BannerCell {
    
    
    func setData(values : PromocodeData) {
        
        DispatchQueue.main.async {
            self.bannerImage.sd_setImage(with: URL(string: values.picture ?? ""), placeholderImage:UIImage(named: Constant.userPlaceholderImage)?.imageTintColor(color1: .veryLightGray),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                   // Perform operation.
                      if (error != nil) {
                          // Failed to load image
                          self.bannerImage.image = UIImage(named: Constant.imagePlaceHolder)
                      } else {
                          // Successful in loading image
                          self.bannerImage.image = image
                      }
                  })
            
        }
       
        
        self.titleLabel.text = "  \(values.promo_code ?? "")  "
    }
    
    
    func setData(values : BannerResponseData) {
        
        DispatchQueue.main.async {
            
            self.bannerImage.sd_setImage(with: URL(string: values.banner_image ?? ""), placeholderImage:UIImage(named: Constant.userPlaceholderImage)?.imageTintColor(color1: .veryLightGray),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                   // Perform operation.
                      if (error != nil) {
                          // Failed to load image
                          self.bannerImage.image = UIImage(named: Constant.imagePlaceHolder)
                      } else {
                          // Successful in loading image
                          self.bannerImage.image = image
                      }
                  })
        }
           
           
           
           self.titleLabel.text = "  \(values.name ?? "")  "
       }
    func setData(values : Coupon) {
        
        DispatchQueue.main.async {
            self.bannerImage.sd_setImage(with: URL(string: values.picture ?? ""), placeholderImage:UIImage(named: Constant.userPlaceholderImage)?.imageTintColor(color1: .veryLightGray),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                   // Perform operation.
                      if (error != nil) {
                          // Failed to load image
                          self.bannerImage.image = UIImage(named: Constant.imagePlaceHolder)
                      } else {
                          // Successful in loading image
                          self.bannerImage.image = image
                      }
                  })
            
        }
       
        
       
        
        self.titleLabel.text = "  \(values.promo_code ?? "")  "
    }
    
    func setCusineListData(data: CusinesShop){
        
        print("data>>>",data.images)
        DispatchQueue.main.async {
            self.bannerImage.sd_setImage(with: URL(string: data.images ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        self.bannerImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                    } else {
                        // Successful in loading image
                        self.bannerImage.image = image
                    }
                })
        }
        titleLabel.text = data.name
    

//        dishLabel.text = data.name
//        dishLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
//        
  
        
    }
}
