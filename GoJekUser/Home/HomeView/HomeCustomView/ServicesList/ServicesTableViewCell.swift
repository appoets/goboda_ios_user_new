//
//  ServicesTableViewCell.swift
//  Goboda
//
//  Created by vamsikrishna surathi on 13/03/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class ServicesTableViewCell: UITableViewCell {

    @IBOutlet weak var taxiBtn: UIButton!
    @IBOutlet weak var foodBtn: UIButton!
    @IBOutlet weak var moversBtn: UIButton!
    @IBOutlet weak var groceryBtn: UIButton!
    
    
    var taxiBtnAction : (() -> ())?
    var foodBtnAction : (() -> ())?
    var moversBtnAction : (() -> ())?
    var groceryBtnAction : (() -> ())?

        
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialLoads()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension ServicesTableViewCell {
    
    
    
    func initialLoads() {
        self.taxiBtn.addTarget(self, action: #selector(taxiAction), for: .touchUpInside)
        self.foodBtn.addTarget(self, action: #selector(foodAction), for: .touchUpInside)
        self.moversBtn.addTarget(self, action: #selector(moversAction), for: .touchUpInside)
        self.groceryBtn.addTarget(self, action: #selector(groceryAction), for: .touchUpInside)

        setDesign()
    }
    
    
    func setDesign() {
        taxiBtn.layer.cornerRadius = 10
        foodBtn.layer.cornerRadius = 10
        moversBtn.layer.cornerRadius = 10
        groceryBtn.layer.cornerRadius = 10
    }
    
    @IBAction func taxiAction() {
        if let action = taxiBtnAction {
            action()
        }
    }
    
    @IBAction func foodAction() {
        if let action = foodBtnAction {
            action()
        }
    }
    
    @IBAction func moversAction() {
        if let action = moversBtnAction {
            action()
        }
    }
    
    @IBAction func groceryAction() {
        if let action = groceryBtnAction {
            action()
        }
    }
}
