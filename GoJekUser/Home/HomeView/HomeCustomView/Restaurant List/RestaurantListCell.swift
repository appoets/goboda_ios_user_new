//
//  RestaurantListCell.swift
//  Goboda
//
//  Created by Mithra Mohan on 16/03/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class RestaurantListCell: UICollectionViewCell {

    
    @IBOutlet weak var restNameLabel: UILabel!
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var imgRest: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }

}




extension RestaurantListCell {
    
    func initialLoads() {
        self.setDesign()
    }
    
    func setDesign() {
        imgRest.layer.cornerRadius = 8
    }
    
    
    func setShopListData(data: ShopsListData){
 
        imgRest.sd_setImage(with: URL(string: data.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                // Perform operation.
                if (error != nil) {
                    // Failed to load image
                    self.imgRest.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                } else {
                    // Successful in loading image
                    self.imgRest.image = image
                }
            })

        restNameLabel.text = data.store_name
     
        if data.rating == nil || data.rating == 0 {
            self.viewRating.isHidden = true
        }else{
             self.viewRating.isHidden = false
        }
        

        
    
        
    }
    
    
}
