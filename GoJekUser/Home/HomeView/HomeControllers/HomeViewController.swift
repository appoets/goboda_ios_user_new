//
//  HomeViewController.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlaces


class HomeViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var locationHeaderView: UIView!
    @IBOutlet weak var staticLocationLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationIconImageView: UIImageView!
    @IBOutlet weak var dropDownImageView: UIImageView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var cartButton : UIButton!

//    var serviceListCollection: ServiceListCollection?
    var serviceHeight = 0.0
    var isAppPresentTapOnPush:Bool = false 
    var promoCodeListArr:[PromocodeData] = []
    var promoCodeListArrGrocery:[PromocodeData] = []
    var promoCodeListArrFood:[PromocodeData] = []
    var promoCodeListArrXuber:[PromocodeData] = []
    var promoCodeListArrCourier:[PromocodeData] = []
    var promoCodeListArrTaxi:[PromocodeData] = []
    var bannerListArr : [BannerResponseData] = []
    
    var promoCodeDetailsListArr : PromocodeDetailsEntity?
    var featuredService: [ServicesDetails]?
    var isFromOrderChat: Bool = false
    var isFromServiceChat: Bool = false
    var menuService: [ServicesDetails]?
    
    var currentLocationValue = String()
    var shopArrList:[ShopsListData] = []
    var bannerData : BannerEntity?
    var address: String = ""
    var mapViewHelper : GoogleMapsHelper?
    var homeRes : HoneResponseData?
    
    var foodieCartList: CartListResponse?
    var storeTypeId : Int = 1
    var restaurantId : Int = 1
    
    var xmapView: XMapView?
    
    private var selectedLanguage: Language = .english {
        didSet {
            LocalizeManager.share.setLocalization(language: selectedLanguage)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingIndicator.show()
        self.initialLoads()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabBar()
        self.homePresenter?.getCartList()
        // isAppPresentTapOnPush == false
        navigationController?.isNavigationBarHidden = true
        isAppPresentTapOnPush = false

        ChatPushClick.shared.clear()
        staticLocationLabel.text = HomeConstant.location.localized
        BackGroundRequestManager.share.stopBackGroundRequest()
        isFromServiceChat = false
        isFromOrderChat = false
        if !isGuestAccount {
            guestAccountCity = .empty
            self.loadHomeDetil(cityId: .empty)
        }
        homePresenter?.fetchUserProfileDetails()
        addNotificationObservers()
        DispatchQueue.main.async {
//            let btnTitle = self.serviceListCollection?.showMoreButton.isSelected ?? false ? HomeConstant.showLess.localized : HomeConstant.showMore.localized
//            self.serviceListCollection?.showMoreButton.setTitle(btnTitle, for: .normal)
            self.homeTableView.reloadData()
        }
        
        if let currentLoc = UserDefaults.standard.value(forKey: "currentLoc") {
            
            if XCurrentLocation.shared.currentAddress != nil {
                self.locationLabel.text = XCurrentLocation.shared.currentAddress
            }else{
                self.locationLabel.text = currentLoc  as? String
            }
            
           
        }
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self)
    }

}

// MARK: - LocalMethod
extension HomeViewController {
    
    func initialLoads() {
        loadServiceListNib()
        defaultIconSetup()
        addGestureForMoreButton()
        homePresenter?.getbannerResponse()
        guestAccountLoadFirstCtiy()
        self.profileButton.addTarget(self, action: #selector(profileAction), for: .touchUpInside)
        self.cartButton.addTarget(self, action: #selector(cartAction), for: .touchUpInside)
        
    }
    
        private func guestAccountLoadFirstCtiy() {
            if isGuestAccount {
                let baseEntity = AppConfigurationManager.shared.baseConfigModel.responseData
                let countryArray = AppManager.shared.getCountries()
                let countryCode = AppManager.shared.getUserDetails()?.country?.id ?? baseEntity?.appsetting?.country
                let cityArray = countryArray?.filter({$0.id == countryCode}).first
    //            locationLabel.text = cityArray?.city?.first?.city_name
                guard let cityId = cityArray?.city?.first?.id else {
                    return
                }
                guestAccountCity = "\(cityId)"
                loadHomeDetil(cityId: "\(cityId)")
            }
            
            FLocationManager.shared.stop()
            FLocationManager.shared.start { (info) in
                
                
                if XCurrentLocation.shared.currentAddress != nil {
                               self.locationLabel.text = XCurrentLocation.shared.currentAddress
                           }else{
                            self.locationLabel.text = info.city
                           }
               
                
               
                
                XCurrentLocation.shared.latitude = info.latitude
                XCurrentLocation.shared.longitude = info.longitude
                
                XCurrentLocation.shared.currentAddress = info.address
                print("adres>>",info.address)
                
                
                let location = CLLocationCoordinate2D(latitude: XCurrentLocation.shared.latitude ?? 0.0, longitude: XCurrentLocation.shared.longitude ?? 0.0)
                
                
               
                
                UserDefaults.standard.setValue("\(info.city ?? "")", forKey: "currentLoc")

            }
        }
    
    private func push(id: String)  {
        let vc = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: id)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction private func profileAction() {
        let foodiecartVC = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: AccountConstant.MyProfileController) as! MyProfileController
//        foodiecartVC.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.pushViewController(foodiecartVC, animated: true)
//        self.push(id: AccountConstant.MyProfileController)

    }
    
    @IBAction private func cartAction() {
        let foodiecartVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier:FoodieConstant.FoodieCartViewController) as! FoodieCartViewController
        foodiecartVC.storeTypeId = self.storeTypeId
        foodiecartVC.restaurantId = self.restaurantId
        navigationController?.pushViewController(foodiecartVC, animated: true)
    
        
    }
    //Controller Basic Custom Methods
    private func addGestureForMoreButton() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapLocation))
        self.locationLabel.isUserInteractionEnabled = true
        self.locationLabel.addGestureRecognizer(gesture)
    }
    
    private func defaultIconSetup() {
        self.view.backgroundColor = .white
//        self.tabBarController?.tabBar.tintColor = .appPrimaryColor
        
        locationIconImageView.image = UIImage(named: Constant.address)
        locationIconImageView.imageTintColor(color1: .green)
        
        dropDownImageView.image = UIImage(named: Constant.ic_downarrow)
        dropDownImageView.imageTintColor(color1: .lightGray)
        staticLocationLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        locationLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
        
        staticLocationLabel.text = HomeConstant.location.localized
        staticLocationLabel.textColor = .appPrimaryColor
    }
    
    func addNotificationObservers() {
           // push redirection
           NotificationCenter.default.addObserver(self, selector: #selector(isTransportRedirection), name: Notification.Name(rawValue: pushNotificationType.transport.rawValue), object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(isServicePushRedirection), name: Notification.Name(rawValue: pushNotificationType.service.rawValue), object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(isFoodiePushRedirection), name: Notification.Name(pushNotificationType.order.rawValue), object: nil)

           NotificationCenter.default.addObserver(self, selector: #selector(isTransportChatRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_transport.rawValue), object: nil)
             NotificationCenter.default.addObserver(self, selector: #selector(isFoodieChatPushRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_order.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(isServiceChatPushRedirection), name: Notification.Name(rawValue: pushNotificationType.chat_servce.rawValue), object: nil)
        
    }
    
    private func loadServiceListNib() {
        self.homeTableView.register(UINib(nibName: HomeConstant.SearchTableViewCell, bundle: nil), forCellReuseIdentifier: HomeConstant.SearchTableViewCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.ServicesTableViewCell, bundle: nil), forCellReuseIdentifier: HomeConstant.ServicesTableViewCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.BannerTableViewCell, bundle: nil), forCellReuseIdentifier: HomeConstant.BannerTableViewCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.ServiceListCollectionTableViewCell, bundle: nil), forCellReuseIdentifier: HomeConstant.ServiceListCollectionTableViewCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.OfferCouponCell, bundle: nil), forCellReuseIdentifier: HomeConstant.OfferCouponCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.RecommendedTableCell, bundle: nil), forCellReuseIdentifier: HomeConstant.RecommendedTableCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.RestaurantCell, bundle: nil), forCellReuseIdentifier: HomeConstant.RestaurantCell)
        self.homeTableView.register(UINib(nibName: HomeConstant.BannerCellView, bundle: nil), forCellReuseIdentifier: HomeConstant.BannerCellView)
        

        
        
//        if serviceListCollection == nil, let serviceListCollection = Bundle.main.loadNibNamed(HomeConstant.ServiceListCollection, owner: self, options: [:])?.first as? ServiceListCollection {
//            self.serviceListCollection = serviceListCollection
//            self.serviceListCollection?.delegate = self
//            self.homeTableView.tableHeaderView =  serviceListCollection
//        }
    }
    
    @objc func tapLocation() {
//        let changeCityVC = LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.CountryCodeViewController) as! CountryCodeViewController
//        changeCityVC.pickerType = .cityList
//        let baseEntity = AppConfigurationManager.shared.baseConfigModel.responseData
//        let countryArray = AppManager.shared.getCountries()
//        let countryCode = AppManager.shared.getUserDetails()?.country?.id ?? baseEntity?.appsetting?.country
//        let cityArray = countryArray?.filter({$0.id == countryCode})
//        changeCityVC.cityListEntity = cityArray?.first?.city ?? [CityData]()
//        changeCityVC.selectedCity = { [weak self] cityDetail in
//            guard let self = self else {
//                return
//            }
//            self.userCityDetails(cityID: "\(cityDetail.id!)")
//            self.locationLabel.text = cityDetail.city_name
//        }
//        navigationController?.pushViewController(changeCityVC, animated: true)
        
            let changeCityVC = HomeRouter.homeStoryboard.instantiateViewController(withIdentifier: HomeConstant.SaveLocationController) as! SaveLocationController
             changeCityVC.onLocationSelection = { location in
            self.mapViewHelper?.getPlaceAddress(from: XCurrentLocation.shared.coordinate, on: { (locationDetail) in  // On Tapping
                print("Location>>",locationDetail)
            })
        }
            navigationController?.pushViewController(changeCityVC, animated: true)
    }
    
    
     @IBAction private func navigateToManageAddressView() {
        
        if guestLogin() {
            
          let homesearch =  HomeRouter.homeStoryboard.instantiateViewController(withIdentifier: HomeConstant.Foodie1SearchViewController) as! Foodie1SearchViewController
            
            if shopArrList.first?.storetype?.category != FoodieConstant.food {
                homesearch.isFoodieCatgory = false
            }else{
                homesearch.isFoodieCatgory = true
            }
            navigationController?.pushViewController(homesearch, animated: true)
        }
            
            
        
    }
    
    
    private func userCityDetails(cityID: String) {
        
        if isGuestAccount {
            guestAccountCity = cityID
            loadHomeDetil(cityId: cityID)
        } else {
            guestAccountCity = .empty
            let param: Parameters = [LoginConstant.city_id: cityID]
            homePresenter?.userCity(param: param)
        }
    }
    
    private func loadHomeDetil(cityId: String) {
        
        let param: Parameters = [LoginConstant.city_id: cityId]
        homePresenter?.getHomeDetails(param: param)
    }
    
    private func xuberRedirect() {
        if isGuestAccount {
            xuberSelectionViewRedirect()
        }else {
            homePresenter?.getXuberRequest()
        }
    }
    
    private func showCourierRequest(){
         if isGuestAccount {
            let vc = CourierRouter.createCourierModule()
              UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
         }else{
            homePresenter?.checkCourierRequest()
        }
    }
    
    private func xuberSelectionViewRedirect() {
        let vc = XuberRouter.createXuberServiceModule()
        let serviceDetail = AppManager.shared.getSelectedServices()
        SendRequestInput.shared.mainServiceId = serviceDetail?.menu_type_id ?? 0
        SendRequestInput.shared.mainSelectedService = serviceDetail?.title ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func foodieRedirect(service:ServicesDetails?) {
//        if isGuestAccount {
            if service?.title == "Food" {
                let vc = FoodieRouter.createFoodieHomeModule(id: service?.menu_type_id ?? 0)
                    UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = FoodieRouter.createFoodieModule(Id: service?.menu_type_id ?? 0)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            }
          
//        }else {
//            homePresenter?.getCheckRequest()
//        }
    }
}

//Mark: google places dele
extension HomeViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

//    self.lat = place.coordinate.latitude
//    self.long = place.coordinate.longitude
    
    self.address = place.formattedAddress ?? ""
    FLocationManager.shared.stop()
    FLocationManager.shared.start { (info) in
         print("lat>>",info.longitude ?? 0.0)
         print("long>>",info.latitude ?? 0.0)
   
        let param: Parameters = ["city":info.city ?? ""]
        self.homePresenter?.getCityData(Id: 0, param: param)
        FLocationManager.shared.stop()
     }
    self.homeTableView.reloadData()
    dismiss(animated: true, completion: nil)
    
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}


// MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        return 9
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == homeTableView ? 0 : 25
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: HomeConstant.SearchTableViewCell, for: indexPath) as! SearchTableViewCell
//            cell.setCouponCellData(couponData: promoCodeListArr)
//            if featuredService?.count == 0 {
//                cell.recommendedLabel.isHidden = true
//            }else{
//                cell.recommendedLabel.isHidden = false
//            }
            
            cell.tapOnLocation = {
                self.navigateToManageAddressView()
            }
            return cell
        }
        
  
        else if indexPath.row == 2{
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: HomeConstant.ServicesTableViewCell, for: indexPath) as! ServicesTableViewCell
//            cell.setCouponCellData(couponData: promoCodeListArr)
//            if featuredService?.count == 0 {
//                cell.recommendedLabel.isHidden = true
//            }else{
//                cell.recommendedLabel.isHidden = false
//            }
            
           
            
            cell.taxiBtnAction = {

                
                if self.homeRes?.taxi == 0 {
                        ToastManager.show(title: "No services available in the city", state: .error)
                    } else {
                        if self.isAppPresentTapOnPush == false {
                            self.isAppPresentTapOnPush = true
                            
                            let taxiHomeViewController = TaxiRouter.createTaxiModule(rideTypeId: self.homeRes?.taxi ?? 0,isfromHome: false)
                            self.navigationController?.pushViewController(taxiHomeViewController, animated: true)
                        }
                    }
                
            }
            cell.foodBtnAction = {
//                if isGuestAccount {
                
                if self.homeRes?.food == 0 {
                        ToastManager.show(title: "No services available in the city", state: .error)
                    } else {
                        let vc = FoodieRouter.createFoodieHomeModule(id: self.homeRes?.food ?? 0)
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//                }else {
//                    self.homePresenter?.getCheckRequest()
//                }
                    }
                
            }
            
            cell.moversBtnAction = {

                if self.homeRes?.courier == 0 {
                        ToastManager.show(title: "No services available in the city", state: .error)
                    } else {
                        if isGuestAccount {
                           let vc = CourierRouter.createCourierModule()
                             UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            self.homePresenter?.checkCourierRequest()
                       }
                        
                    }
                
            }
            
            cell.groceryBtnAction = {

          
                if self.homeRes?.grocery == 0 {
                        ToastManager.show(title: "No services available in the city", state: .error)
                    } else {
//                        if isGuestAccount {
                            let vc = FoodieRouter.createFoodieModule(Id: self.homeRes?.grocery ?? 0)
                            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//                        }else {
//                            self.homePresenter?.getCheckRequest()
//                        }
                    }
                }
            
            return cell
        }
        
        else if indexPath.row == 3{
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: HomeConstant.ServiceListCollectionTableViewCell, for: indexPath) as! ServiceListCollectionTableViewCell
//            cell.setCouponCellData(couponData: promoCodeListArr)
            cell.delegate = self
            cell.setServiceDataSource(services: menuService ?? [])
            
            //            let btnTitle = self.serviceListCollection?.showMoreButton.isSelected ?? false ? HomeConstant.showLess.localized : HomeConstant.showMore.localized
            //            self.serviceListCollection?.showMoreButton.setTitle(btnTitle, for: .normal)
//            if featuredService?.count == 0 {
//                cell.recommendedLabel.isHidden = true
//            }else{
//                cell.recommendedLabel.isHidden = false
//            }
            return cell
        }
   
        else {
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: HomeConstant.RestaurantCell, for: indexPath) as! RestaurantCell
            cell.collectionViewRest.register(UINib(nibName: HomeConstant.BannerCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.BannerCell)
            cell.collectionViewRest.register(UINib(nibName: HomeConstant.BannerCellView, bundle: nil), forCellWithReuseIdentifier: HomeConstant.BannerCellView)
            
            cell.collectionViewRest.delegate = self
            cell.collectionViewRest.dataSource  = self
            
            if indexPath.row == 5 {
                cell.collectionViewRest.register(UINib(nibName: HomeConstant.RestaurantListCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.RestaurantListCell)
            } else if indexPath.row == 1{
                cell.collectionViewRest.register(UINib(nibName: HomeConstant.BannerCellView, bundle: nil), forCellWithReuseIdentifier: HomeConstant.BannerCellView)
            }
            else {
                cell.collectionViewRest.register(UINib(nibName: HomeConstant.BannerCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.BannerCell)
            }
            cell.titleLabel.text = indexPath.row == 1 ? "" : (indexPath.row == 4) ? "Taxi" : (indexPath.row == 5) ? "Restaurant List" : indexPath.row == 6 ? "Food" : indexPath.row == 7 ? "Movers" : "Grocery"
            cell.collectionViewRest.accessibilityIdentifier = indexPath.row == 1 ? "offer" : (indexPath.row == 4) ? "taxi" : (indexPath.row == 5) ? "restCell" : indexPath.row == 6 ? "food" : indexPath.row == 7 ? "movers" : "grocery"
            if indexPath.row == 1{
                cell.titleLabel.isHidden = true
            }
            
            cell.collectionViewRest.reloadData()
            
//            cell.setCouponCellData(couponData: promoCodeListArr)
//            if featuredService?.count == 0 {
//                cell.recommendedLabel.isHidden = true
//            }else{
//                cell.recommendedLabel.isHidden = false
//            }
            return cell
        }
        
//        else {
//            guard let data = featuredService else {
//                return UITableViewCell()
//            }
//            let cell = tableView.dequeueReusableCell(withIdentifier: HomeConstant.RecommendedTableCell, for: indexPath) as! RecommendedTableCell
//            cell.setRecommendedData(dataSource: data, indexPath: indexPath)
//            return cell
//
//        }
    }
}

// MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 75
        }else if indexPath.row == 1 {
            return (promoCodeListArr.count == 0 ? 0 : 250)
        }else if indexPath.row == 2 {
            return 268
        }
        else if indexPath.row == 3 {
            return CGFloat(self.serviceHeight)
        }else if indexPath.row == 4 {
            return (promoCodeListArrTaxi.count == 0 ? 0 : 200)
        }else if indexPath.row == 5 {
            return (shopArrList.count == 0 ? 0 : 250)
        }else if indexPath.row == 6 {
            return (promoCodeListArrFood.count == 0 ? 0 : 200)
        }else if indexPath.row == 7 {
            return (promoCodeListArrCourier.count == 0 ? 0 : 200)
        }else if indexPath.row == 8 {
            return (promoCodeListArrGrocery.count == 0 ? 0 : 200)
        }else {
            return 200
        }
//        return  indexPath.row == 0 ? CGFloat(200) : 130 //UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        if indexPath.row != 0 {
//            if featuredService?.count != 0 {
//                let index = indexPath.row - 1
//                let selectedService = featuredService?[index]
//                AppManager.shared.setSelectedServices(service: selectedService!)
//                
//                switch selectedService?.service?.admin_service_name ?? "" {
//                case MasterServices.Transport.rawValue:
//                    let vc = TaxiRouter.createTaxiModule(rideTypeId: selectedService?.menu_type_id ?? 0)
//                    vc.hidesBottomBarWhenPushed = true
//                    navigationController?.pushViewController(vc, animated: true)
//                case MasterServices.Order.rawValue:
//                    foodieRedirect(service: selectedService)
//                case MasterServices.Service.rawValue:
//                    xuberRedirect()
//                default:
//                    break
//                }
//            }
//        }
    }
}

extension HomeViewController : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.accessibilityIdentifier == "taxi" {
            return promoCodeListArrTaxi.count
        }
        if collectionView.accessibilityIdentifier == "food" {
            return promoCodeListArrFood.count
        }
        if collectionView.accessibilityIdentifier == "movers" {
            return promoCodeListArrCourier.count
        }
        if collectionView.accessibilityIdentifier == "grocery" {
            return promoCodeListArrGrocery.count
        }
        if collectionView.accessibilityIdentifier == "restCell" {
            return shopArrList.count
        }
        if collectionView.accessibilityIdentifier == "offer" {
            return bannerData?.responseData?.count ?? 0
        }
        if collectionView.accessibilityIdentifier == "BannerCellView" {
            return bannerData?.responseData?.count ?? 0
        }
        
        
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView.accessibilityIdentifier == "restCell" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.RestaurantListCell, for: indexPath) as! RestaurantListCell
            
            print("data>>",shopArrList[indexPath.row].store_name)
            
            cell.setShopListData(data: shopArrList[indexPath.row])
            return cell
        }
        
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.BannerCell, for: indexPath) as! BannerCell
            if collectionView.accessibilityIdentifier == "taxi" {
                cell.setData(values: promoCodeListArrTaxi[indexPath.row])
//                cell.bannerImage.image = #imageLiteral(resourceName: "TaxiPromo")
            }
//            if collectionView.accessibilityIdentifier == "food" {
//                cell.setData(values: promoCodeListArrFood[indexPath.row])
//            }
            if collectionView.accessibilityIdentifier == "movers" {
                cell.setData(values: promoCodeListArrCourier[indexPath.row])
            }
            if collectionView.accessibilityIdentifier == "grocery" {
                cell.setData(values: promoCodeListArrGrocery[indexPath.row])
            }
            if collectionView.accessibilityIdentifier == "offer" {
                cell.titleLabel.isHidden = false
                cell.setData(values: (bannerData?.responseData?[indexPath.row])!)
            }
            
            return cell
        }
     
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        
       
        
        if collectionView.accessibilityIdentifier == "taxi" {
            if isAppPresentTapOnPush == false {
                isAppPresentTapOnPush = true
                let taxiHomeViewController = TaxiRouter.createTaxiModule(rideTypeId: self.homeRes?.taxi ?? 0,isfromHome: false)
                navigationController?.pushViewController(taxiHomeViewController, animated: true)
            }
        }
        if collectionView.accessibilityIdentifier == "food" {
//            let foodVC = FoodieRouter.foodieStoryboard.instantiateViewController(identifier: "FoodController") as! FoodController
//            foodVC.shopArrList = self.shopArrList
//            foodVC.promocodes = self.promoCodeListArrFood
//            self.navigationController?.pushViewController(foodVC, animated: true)
            
//            if isGuestAccount {
            
            
            let vc = FoodieRouter.createFoodieHomeModule(id: self.homeRes?.food ?? 0)
             UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//            }else {
//                homePresenter?.getCheckRequest()
//            }
        }
        if collectionView.accessibilityIdentifier == "movers" {
            if isGuestAccount {
               let vc = CourierRouter.createCourierModule()
                 UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            }else{
               homePresenter?.checkCourierRequest()
           }
        }
        if collectionView.accessibilityIdentifier == "grocery" {
//            let foodVC = FoodieRouter.foodieStoryboard.instantiateViewController(identifier: "FoodController") as! FoodController
//            foodVC.shopArrList = self.shopArrList
//            foodVC.promocodes = self.promoCodeListArrFood
//            self.navigationController?.pushViewController(foodVC, animated: true)
            
//            if isGuestAccount {
            let vc = FoodieRouter.createFoodieModule(Id: self.homeRes?.grocery ?? 0)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//            }else {
//                homePresenter?.getCheckRequest()
//            }
        }
        if collectionView.accessibilityIdentifier == "restCell" {

            if shopArrList.count != 0 {
                let foodieItem = shopArrList[indexPath.row]
               // if foodieItem.shopstatus != "CLOSED" {
                let vc = FoodieRouter.createFoodieItemModule(id:foodieItem.id ?? 0)
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//                    let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
//                    foodieItemsVC.restaurentId = foodieItem.id ?? 0
//                    navigationController?.pushViewController(foodieItemsVC, animated: true)
              //  }
                
            }
        }
        
        if collectionView.accessibilityIdentifier == "offer" {
            
            if  bannerData?.responseData?[indexPath.row].name == "Taxi"{
               
                if isAppPresentTapOnPush == false {
                    isAppPresentTapOnPush = true
                    let taxiHomeViewController = TaxiRouter.createTaxiModule(rideTypeId: self.homeRes?.taxi ?? 0,isfromHome: false)
                    navigationController?.pushViewController(taxiHomeViewController, animated: true)
                }
                
            }else if bannerData?.responseData?[indexPath.row].name == "Food" {
                let vc = FoodieRouter.createFoodieHomeModule(id: self.homeRes?.food ?? 0)
                 UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            } else if bannerData?.responseData?[indexPath.row].name == "Grocery"{
                
                let vc = FoodieRouter.createFoodieModule(Id: self.homeRes?.grocery ?? 0)
                    UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                let vc = FoodieRouter.createFoodieHomeModule(id: self.homeRes?.food ?? 0)
                 UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)

            }
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if collectionView.accessibilityIdentifier == "restCell" {
            return CGSize(width: 120, height: 200)
        }  else if collectionView.accessibilityIdentifier == "taxi" {
            return CGSize(width: collectionView.frame.width, height: 160)
        }
        
        else if collectionView.accessibilityIdentifier == "offer" {
           return CGSize(width: collectionView.frame.width, height: 170)
       }
        else {
            return CGSize(width: collectionView.frame.width - 100, height: 160)

        }
    }
}

// push redirection methods

extension HomeViewController {
    
    // For Transport push redirection
    @objc func isTransportRedirection() {
        if isAppPresentTapOnPush == false {
            isAppPresentTapOnPush = true
            let taxiHomeViewController = TaxiRouter.createTaxiModule(rideTypeId: 0,isfromHome: false)
            navigationController?.pushViewController(taxiHomeViewController, animated: true)
        }
    }
    
    //For service push redirection
    @objc func isServicePushRedirection() {
        if isAppPresentTapOnPush == false {
            isAppPresentTapOnPush = true
             isFromServiceChat = false
            homePresenter?.getXuberRequest()
        }
    }
    
   //For foodie(order) push redirection
    @objc func isFoodiePushRedirection() {
        if isAppPresentTapOnPush == false {
            isAppPresentTapOnPush = true
            isFromOrderChat = false
            homePresenter?.getCheckRequest()
//            let foodieHomeViewController = FoodieRouter.createFoodieModule()
//            navigationController?.pushViewController(foodieHomeViewController, animated: true)
        }
    }

    @objc func isTransportChatRedirection() {
        if ChatPushClick.shared.isPushClick == false {
             ChatPushClick.shared.isPushClick = true
            print("idPush")
            let taxiHomeViewController = TaxiRouter.createTaxiModuleChat(rideTypeId: 0)
             UIApplication.topViewController()?.navigationController?.pushViewController(taxiHomeViewController, animated: true)
           // navigationController?.pushViewController(taxiHomeViewController, animated: true)
        }
    }

    @objc func isFoodieChatPushRedirection() {
        if  ChatPushClick.shared.isOrderPushClick == false {
            ChatPushClick.shared.isOrderPushClick = true
            isFromOrderChat = true
             homePresenter?.getCheckRequest()
          }
      }
    @objc func isServiceChatPushRedirection() {
        if  ChatPushClick.shared.isServicePushClick == false {
            ChatPushClick.shared.isServicePushClick = true
            isFromServiceChat = true
            homePresenter?.getXuberRequest()
        }
    }
}

//MARK:- HomeAddMoreDelegate
extension HomeViewController: HomeAddMoreDelegate {
    
    func callCheckRequest(isFlowRequest: String, service: ServicesDetails?) {
        if Flow.foodie == isFlowRequest {
            foodieRedirect(service: service)
        }else if Flow.service == isFlowRequest {
            xuberRedirect()
        }else if Flow.courier == isFlowRequest {
            showCourierRequest()
        }
    }
    
    func tapShowMore(height: Double) {
        UIView.animate(withDuration: 1.0, animations: {
//            self.homeTableView.tableHeaderView?.frame.size.height = self.serviceListCollection?.collectionView.frame.height ?? 0
            self.serviceHeight = height
            DispatchQueue.main.async {
                self.homeTableView.reloadData()
                self.homeTableView.layoutIfNeeded()
                self.homeTableView.contentOffset = CGPoint.zero
            }
        } )
    }
}

//MARK:- HomePresenterToHomeViewProtocol
extension HomeViewController: HomePresenterToHomeViewProtocol {
    
    func checkCourierRequest(requestEntity: Request) {
        if requestEntity.responseData?.data?.count == 0 {
            let vc = CourierRouter.createCourierModule()
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let courierHomeVC = CourierRouter.createCourierHomeModule(isNewOrder: true)
            UIApplication.topViewController()?.navigationController?.pushViewController(courierHomeVC, animated: true)
        }
    }
    
    func showUserProfileDtails(details: UserProfileResponse) {
        print("lat>>",details.responseData?.latitude , details.responseData?.longitude)
        var userDetails:UserProfileEntity = UserProfileEntity()
        userDetails = details.responseData ?? UserProfileEntity()
        
        DispatchQueue.main.async {
//            self.locationLabel.text = (userDetails.city?.city_name ?? "") + " ," + (userDetails.country?.country_code ?? "")
            if !CommonFunction.checkisRTL() {
                UserDefaults.standard.set(userDetails.language, forKey: AccountConstant.language)
                self.localizable()
            }
        }
        AppManager.shared.setUserDetails(details: userDetails)
        homePresenter?.getSavedAddress()
        
        if let currentLoc = UserDefaults.standard.value(forKey: "currentLoc") {
            currentLocationValue = currentLoc as? String ?? ""
            print("curent>>",currentLocationValue)
            if XCurrentLocation.shared.currentAddress != nil {
                 self.locationLabel.text = XCurrentLocation.shared.currentAddress
            }else{
               let currentAddress = UserDefaults.standard.value(forKey: "currentAdress")
               print("curnt >",currentAddress)
                   self.locationLabel.text = currentLoc  as? String
                }
            let param: Parameters = ["city":currentLoc ?? ""]
            self.homePresenter?.getCityData(Id: 0, param: param)
        }
            
    }
    
    func getCartListResponse(cartListEntity: FoodieCartListEntity) {
        self.foodieCartList = cartListEntity.responseData
        self.cartButton.isEnabled = self.foodieCartList?.carts?.count ?? 0 > 0 ? true : false
        self.cartButton.setImage( self.foodieCartList?.carts?.count ?? 0 > 0 ? #imageLiteral(resourceName: "Cart_Value").withTintColor(.black) : #imageLiteral(resourceName: "ic_cartbag").withTintColor(.black), for: .normal)
        self.storeTypeId = self.foodieCartList?.carts?.first?.store?.store_type_id ?? 1
        self.restaurantId = self.foodieCartList?.carts?.first?.storeId ?? 1
    }
    
    func getbannerResponse(details: BannerEntity) {
        print("details>.",details)
        
        bannerData = details
        
    }
    
    // get address for saved locations
    func savedAddressSuccess(addressEntity: SavedAddressEntity) {
        LoadingIndicator.hide()
        
        if let address = addressEntity.responseData {
            AppManager.shared.setSavedAddress(address: address)
        }
        
        homePresenter?.getPromoCodeDetailsList()
        print(addressEntity)
    }
    
    private func localizable() {
        if let languageStr = UserDefaults.standard.value(forKey: AccountConstant.language) as? String, let language = Language(rawValue: languageStr) {
            LocalizeManager.share.setLocalization(language: language)
            selectedLanguage = language
            
        }else {
            LocalizeManager.share.setLocalization(language: .english)
        }
        if !CommonFunction.isAppAlreadyLaunchedOnce() {
            if self.selectedLanguage == .arabic {
                //  showTabBar()
                self.view.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
                
            }else {
                view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
            DispatchQueue.main.async {
//                let btnTitle = self.serviceListCollection?.showMoreButton.isSelected ?? false ? HomeConstant.showLess.localized : HomeConstant.showMore.localized
//                self.serviceListCollection?.showMoreButton.setTitle(btnTitle, for: .normal)
                self.homeTableView.reloadData()
            }
        }
        if !CommonFunction.checkisRTL() {
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
        showTabBar()
    }
    
    func showHomeDetails(details: HomeEntity) {
//        if let serviceListView = homeTableView.tableHeaderView as? ServiceListCollection {
        let menuservice = details.responseData?.services ?? [ServicesDetails]()
        homeRes = details.responseData
        promoCodeListArr = details.responseData?.promocodes ?? []
        self.menuService = menuservice
//            serviceListView.setServiceDataSource(services:menuservice )
        featuredService = menuservice.filter({$0.is_featured ?? 0 == 1})
        DispatchQueue.main.async {
            self.homeTableView.reloadData()
        }
//        }
//        homePresenter?.fetchUserProfileDetails()
    }
    
    func showUserCity(selectedCityDetails: UserCity) {
        loadHomeDetil(cityId: .empty)
    }
    
    func getCheckRequestResponse(checkRequestEntity: FoodieCheckRequestEntity) {
    ///    if checkRequestEntity.responseData?.data?.count == 0 {
 //          let vc = FoodieRouter.createFoodieModule(Id: 0)
 //           UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
//        }else{
              let foodiecartVC = FoodieRouter.createFoodieOrderStatusModule(isHome: true, orderId: checkRequestEntity.responseData?.data?.first?.id ?? 0,isChat: isFromOrderChat)
                UIApplication.topViewController()?.navigationController?.pushViewController(foodiecartVC, animated: true)
//        }
    }
    
    func xuberCheckRequest(xuberReesponse: XuberRequestEntity) {
        if (xuberReesponse.request?.data?.count ?? 0) > 0 {
            let vc = XuberRouter.createXuberModule(isChat: isFromServiceChat)
              UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
           // navigationController?.pushViewController(vc, animated: true)
        }else {
            xuberSelectionViewRedirect()
        }
    }
    
    
    func getPromoCodeListResponse(getPromoCodeResponse: PromocodeDetailsEntity) {
        
        self.promoCodeDetailsListArr = getPromoCodeResponse
//        self.promoCodeListArr = getPromoCodeResponse.responseData ?? []
        for item in getPromoCodeResponse.responseData ?? []{
            let service = item.serviceDict
            if service?.display_name == "DELIVERY" {
                promoCodeListArrCourier.append(item)
            }
            if service?.display_name == "SERVICE" {
                promoCodeListArrXuber.append(item)
            }
            if service?.display_name == "TRANSPORT" {
                promoCodeListArrTaxi.append(item)
            }
//            if service?.admin_service_name == "ORDER" {
//                promoCodeListArrFood.append(item)
//            }
        }
        
        DispatchQueue.main.async {
            self.homeTableView.reloadData()
        }
//        FLocationManager.shared.stop()
//        FLocationManager.shared.start { (info) in
//             print(info.longitude ?? 0.0)
//             print(info.latitude ?? 0.0)
//
//            let param: Parameters = ["latitude":info.latitude ?? 0.0,
//                                                      "longitude":info.longitude ?? 0.0]
//            self.homePresenter?.getListOfStores(Id: 1, param: param)
//        }
        
        let param: Parameters = ["latitude":13.058506,
                                "longitude":80.253761]
        self.homePresenter?.getListOfStores(Id: 1, param: param)
    }
    
    
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) {
        print(getStoreResponse)

        shopArrList = getStoreResponse.responseData ?? []
        
        homeTableView.reloadData()
        
//        if shopArrList.count == 0 {
//            restaurantListTableView.setBackgroundImageAndTitle(imageName: FoodieConstant.orderEmpty, title: FoodieConstant.noRestaurant,tintColor: .black)
//        }else{
//            restaurantListTableView.backgroundView = nil
//        }
        LoadingIndicator.hide()
        self.homePresenter?.getPromoCodeListNew()

    }
    
    func getPromoCodeNewResponse(getPromoCodeResponse: PromocodeDetailsEntity) {
        print(getPromoCodeResponse)
        
        for item in getPromoCodeResponse.responseData ?? [] {
          
            if item.name == "Food" || item.name == "Restaurant" {
                for val in item.promocode ?? [] {
                    promoCodeListArrFood.append(val)
                }
            }
            if item.name == "Grocery" {
                for val in item.promocode ?? [] {
                    promoCodeListArrGrocery.append(val)
                }
            }
        }
    }
    
    
    func getCityResponse(details: HomeEntity) {
//        ToastManager.show(title: details.message ?? "", state: .success)
        if !isGuestAccount {
            guestAccountCity = .empty
            self.loadHomeDetil(cityId: .empty)
            
        }
    }
}

