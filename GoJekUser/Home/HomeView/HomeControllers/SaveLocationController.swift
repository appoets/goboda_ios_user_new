//
//  SaveLocationController.swift
//  GoJekUser
//
//  Created by  on 19/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import GooglePlaces

class SaveLocationController: UIViewController {
    
    @IBOutlet weak var locationTableView: UITableView!
    @IBOutlet weak var txtFieldLocation : UITextField!
    @IBOutlet weak var btnCurrentLocation : UIButton!
    
    
    @IBOutlet weak var backbuton: UIButton!
    var mapViewHelper : GoogleMapsHelper?
    var savedLocationArr = [Constant.Shome.localized,Constant.Swork.localized,Constant.other.localized]
    var savedLocImageArr = [Constant.locationHome,Constant.ic_work,Constant.ic_location_pin]
    var addressDatasource: [AddressResponseData] = Array()
    var addressTypeArray: [String] = Array()
    
    
    var onLocationSelection : ((String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        homePresenter?.getSavedAddress()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)

    }
    
}

//MARK: - Methods

extension SaveLocationController {
    private func initialLoads() {
        setNavigationTitle()
        backbuton.isUserInteractionEnabled = true
        setLeftBarButtonWith(color: .black)
        locationTableView.register(nibName: Constant.SavedLocationCell)
        view.backgroundColor = .veryLightGray
         backbuton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        btnCurrentLocation.addTarget(self, action: #selector(currentLocationAction), for: .touchUpInside)
        self.txtFieldLocation.placeholder = "Search For Your Address"
    }
    
    
    
    
    
    @objc func currentLocationAction(){
        
        print("currnteLocation>>" ,XCurrentLocation.shared.currentAddress)
             FLocationManager.shared.stop()
             FLocationManager.shared.start { (info) in
                print(info.longitude ?? 0.0)
                print(info.latitude ?? 0.0)
                print(info.city ?? 0.0)
                XCurrentLocation.shared.currentAddress = info.address
                UserDefaults.standard.setValue("\(info.address ?? "")", forKey: "currentLoc")
                UserDefaults.standard.setValue("\(info.city ?? "")", forKey: "currentAdress")
                self.navigationController?.popViewController(animated: true)
    
        }
        
    }
    
    @objc func tapBack() {
        
    }
    
    @objc func tapViewMore() {
        print("View more")
    }
    
    
     @IBAction private func navigateToManageAddressView() {
        let autocompleteController = GMSAutocompleteViewController()
           autocompleteController.delegate = self

           // Specify the place data types to return.
           let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
             UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
           autocompleteController.placeFields = fields

           // Specify a filter.
           let filter = GMSAutocompleteFilter()
           filter.type = .address
           autocompleteController.autocompleteFilter = filter

           // Display the autocomplete view controller.
           present(autocompleteController, animated: true, completion: nil)
        
     }
}


//Mark: google places dele
extension SaveLocationController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

    
//    self.lat = place.coordinate.latitude
//    self.long = place.coordinate.longitude
    
    self.txtFieldLocation.text = place.formattedAddress ?? ""
    FLocationManager.shared.stop()
    
//    FLocationManager.shared.start { (info) in
//         print(info.longitude ?? 0.0)
//         print(info.latitude ?? 0.0)
//
////        let param: Parameters = ["city":info.city ?? ""]
////        self.homePresenter?.getCityData(Id: 0, param: param)
//        FLocationManager.shared.stop()
//
//     }
    let info = LocationInformation()
    let mostRecentLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
    let geocoder = CLGeocoder()
    geocoder.reverseGeocodeLocation(mostRecentLocation) { (placemarks, error) in
        guard let placemarks = placemarks, let placemark = placemarks.first else {
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            return }
          
        self.dismiss(animated: true, completion: {
             if let city = placemark.locality,
             let state = placemark.administrativeArea,
             let zip = placemark.postalCode,
             let locationName = placemark.name,
             let thoroughfare = placemark.locality,
             let country = placemark.country {
             info.city     = city
             info.state    = state
             info.zip = zip
             info.address =  locationName + ", " + (thoroughfare as String)
             info.country  = country
             UserDefaults.standard.setValue("\(info.city ?? "")", forKey: "currentLoc")
             UserDefaults.standard.setValue("\(info.address ?? "")", forKey: "currentAdress")
             //print("inr>",info.address)
             XCurrentLocation.shared.currentAddress = info.address
            self.navigationController?.popViewController(animated: true)
         }
        })
        
    }
//        self.homeTableView.reloadData()
   
    
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}



//MARK: - Tableview Delegate Datasource

extension SaveLocationController: UITableViewDelegate {
    
}

extension SaveLocationController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? addressDatasource.count : addressDatasource.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: section == 0 ? 0 : 40))
        headerView.backgroundColor = .clear
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: headerView.frame.width-40, height: headerView.frame.height))
        label.text = section == 0 ? HomeConstant.recentSearch.localized :  HomeConstant.savedLocation.localized
        headerView.addSubview(label)
        return section == 0 ? headerView : headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0,width: view.frame.width, height: 40))
        footerView.layer.cornerRadius = 5.0
        footerView.backgroundColor  = .white
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 40))
        button.setTitle(HomeConstant.viewMore.localized, for: .normal)
        button.setTitleColor(.appPrimaryColor, for: .normal)
//        button.addTarget(self, action: #selector(tapViewMore), for: .touchUpInside)
//        footerView.addSubview(button)
        return section == 0 ? nil : footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 40 : 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SavedLocationCell = locationTableView.dequeueReusableCell(withIdentifier: Constant.SavedLocationCell, for: indexPath) as! SavedLocationCell
        cell.setCornerRadiuswithValue(value: 5.0)
        if indexPath.section == 0 {
            cell.locationImage.image = UIImage(named: Constant.ic_current_location)
//            cell.locationTitleLabel.text = HomeConstant.currentLocation.localized
//            cell.locationDetailsLabel.text = HomeConstant.enableLocation.localized
            cell.locationTitleLabel.text = addressDatasource[indexPath.row].address_type
            cell.locationDetailsLabel.text = addressDatasource[indexPath.row].street
        }else{
            switch  addressDatasource[indexPath.row].address_type ?? "" {
            case "Home":
                cell.locationImage.image = UIImage(named: Constant.locationHome)
                cell.locationTitleLabel.text = addressDatasource[indexPath.row].address_type
                cell.locationDetailsLabel.text = addressDatasource[indexPath.row].street
            case "Work":
                cell.locationImage.image = UIImage(named: Constant.ic_work)
                cell.locationTitleLabel.text = addressDatasource[indexPath.row].address_type
                cell.locationDetailsLabel.text = addressDatasource[indexPath.row].street
            case "Other":
                cell.locationImage.image = UIImage(named: Constant.ic_location_pin)
                cell.locationTitleLabel.text = addressDatasource[indexPath.row].address_type
            cell.locationDetailsLabel.text = addressDatasource[indexPath.row].street
            default:
                cell.locationImage.image = UIImage(named: Constant.ic_location_pin)
                cell.locationTitleLabel.text = addressDatasource[indexPath.row].address_type
            cell.locationDetailsLabel.text = addressDatasource[indexPath.row].street
            }
           
        }
        cell.locationImage.imageTintColor(color1: .lightGray)
        cell.locationDetailsLabel.textColor = .lightGray
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("Locaton>>",indexPath.row)
        
        XCurrentLocation.shared.currentAddress = addressDatasource[indexPath.row].street ?? ""
        
    //    onLocationSelection?(addressDatasource[indexPath.row].street ?? "")
        
        navigationController?.popViewController(animated: true)
       
    }
}



extension SaveLocationController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.navigateToManageAddressView()
    }
}



extension SaveLocationController: HomePresenterToHomeViewProtocol {
    
    func savedAddressSuccess(addressEntity: SavedAddressEntity) {
        addressDatasource = addressEntity.responseData ?? []
        addressTypeArray.removeAll()
        for addressTypeDic in addressDatasource {
            if let addressType = addressTypeDic.address_type, addressType.uppercased() == AddressType.Other.rawValue {
                if let addressTitle = addressTypeDic.title {
                    addressTypeArray.append(addressTitle.uppercased())
                }
                else {
                    addressTypeArray.append(addressType.uppercased())
                }
            }
        }
        self.locationTableView.reloadInMainThread()
    }
}
