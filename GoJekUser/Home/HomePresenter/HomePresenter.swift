//
//  HomePresenter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class HomePresenter: HomeViewToHomePresenterProtocol {

    
    
    var homeView: HomePresenterToHomeViewProtocol?
    
    var homeInteractor: HomePresenterToHomeInteractorProtocol?
    
    var homeRouter: HomePresenterToHomeRouterProtocol?
    
    func fetchUserProfileDetails() {
        
        homeInteractor?.fetchUserProfileDetails()
    }
    
    func getSavedAddress() {
        homeInteractor?.getSavedAddress()
    }
    
    func getHomeDetails(param: Parameters) {
        homeInteractor?.getHomeDetails(param: param)
    }
    
    func userCity(param: Parameters) {
        homeInteractor?.userCity(param: param)
    }
    func getCheckRequest(){
        homeInteractor?.getCheckRequest()
        
    }
    func getPromoCodeList() {
        homeInteractor?.getPromoCodeList()
    }
    
    func getXuberRequest() {
        homeInteractor?.getXuberRequest()
    }
    
    func getUserChatHistory(param: Parameters) {
        homeInteractor?.getUserChatHistory(param: param)
    }
    func checkCourierRequest() {
        homeInteractor?.checkCourierRequest()
    }
    func getPromoCodeDetailsList() {
        homeInteractor?.getPromoCodeDetailsList()

    }
    func getListOfStores(Id: Int, param: Parameters) {
        homeInteractor?.getListOfStores(Id: Id, param: param)
    }
    
    func getPromoCodeListNew() {
        homeInteractor?.getPromoCodeListNew()
    }
    
    func getCityData(Id: Int, param: Parameters) {
        homeInteractor?.getCityData(Id: Id, param: param)
    }
       
    
    func getbannerResponse() {
        homeInteractor?.getbannerResponse()

    }
    func searchRestaurantList(id: Int,type: String,searchStr: String,param: Parameters) {
        homeInteractor?.searchRestaurantList(id: id,type: type,searchStr: searchStr,param: param)
    }
    
    func getCartList() {
        homeInteractor?.getCartList()
    }
    func getAllRequest() {
        homeInteractor?.getAllRequest()
    }
    
    
}

extension HomePresenter: HomeInteractorToHomePresenterProtocol {

    
    
    func searchRestaturantResponse(getSearchRestuarantResponse: SearchEntity) {
        homeView?.searchRestaturantResponse(getSearchRestuarantResponse: getSearchRestuarantResponse)
    }
    func getbannerResponse(details: BannerEntity) {
        homeView?.getbannerResponse(details: details)
    }
    
    
    
    func getCityResponse(details: HomeEntity) {
        homeView?.getCityResponse(details: details)
    }
    
    func getPromoCodeNewResponse(getPromoCodeResponse: PromocodeDetailsEntity) {
        homeView?.getPromoCodeNewResponse(getPromoCodeResponse: getPromoCodeResponse)

    }
    
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) {
        homeView?.getListOfStoresResponse(getStoreResponse: getStoreResponse)
    }
    
    
    func getPromoCodeListResponse(getPromoCodeResponse: PromocodeDetailsEntity) {
        homeView?.getPromoCodeListResponse(getPromoCodeResponse: getPromoCodeResponse)
    }
    
    func checkCourierRequest(requestEntity: Request) {
        homeView?.checkCourierRequest(requestEntity: requestEntity)
    }
        
    func getPromoCodeResponse(getPromoCodeResponse: PromocodeEntity) {
        homeView?.getPromoCodeResponse(getPromoCodeResponse: getPromoCodeResponse)
    }
    
    func getCheckRequestResponse(checkRequestEntity: FoodieCheckRequestEntity) {
        homeView?.getCheckRequestResponse(checkRequestEntity: checkRequestEntity)
    }
    func showHomeDetails(details: HomeEntity) {
        homeView?.showHomeDetails(details: details)
    }
    
   
    func showUserProfileDtails(details: UserProfileResponse) {
        homeView?.showUserProfileDtails(details: details)
    }
    
    func savedAddressSuccess(addressEntity: SavedAddressEntity) {
        homeView?.savedAddressSuccess(addressEntity: addressEntity)
    }
    
    func showUserCity(selectedCityDetails: UserCity) {
        homeView?.showUserCity(selectedCityDetails: selectedCityDetails)
    }
    
    func xuberCheckRequest(xuberReesponse: XuberRequestEntity) {
        homeView?.xuberCheckRequest(xuberReesponse: xuberReesponse)
    }
    
    func getUserChatHistoryResponse(chatEntity: ChatEntity) {
        homeView?.getUserChatHistoryResponse(chatEntity: chatEntity)
    }
    
    func getCartListResponse(cartListEntity: FoodieCartListEntity) {
        homeView?.getCartListResponse(cartListEntity: cartListEntity)
    }
    
    func getAllRequest(AllRequestResponse: AllRequest) {
        homeView?.getAllRequest(AllRequestResponse: AllRequestResponse)
    }
    
    
}
