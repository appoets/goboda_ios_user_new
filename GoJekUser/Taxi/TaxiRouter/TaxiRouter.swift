//
//  TaxiRouter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class TaxiRouter: TaxiPresenterToTaxiRouterProtocol {
static func createTaxiModule(rideTypeId: Int?, isfromHome: Bool?) -> UIViewController {
        let taxiHomeViewController  = taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.TaxiHomeViewController) as! TaxiHomeViewController
        taxiHomeViewController.isFromHome = isfromHome ?? false
        let taxiPresenter: TaxiViewToTaxiPresenterProtocol & TaxiInterectorToTaxiPresenterProtocol = TaxiPresenter()
        let taxiInteractor: TaxiPresentorToTaxiInterectorProtocol = TaxiInteractor()
        let taxiRouter: TaxiPresenterToTaxiRouterProtocol = TaxiRouter()
        
        taxiHomeViewController.taxiPresenter = taxiPresenter
        taxiPresenter.taxiView = taxiHomeViewController
        taxiPresenter.taxiRouter = taxiRouter
        taxiPresenter.taxiInterector = taxiInteractor
        taxiInteractor.taxiPresenter = taxiPresenter
        taxiHomeViewController.rideTypeId = rideTypeId
        return taxiHomeViewController
    }
    static func createTaxiModuleChat(rideTypeId: Int?) -> UIViewController {
        let taxiHomeViewController  = taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.TaxiHomeViewController) as! TaxiHomeViewController
        let taxiPresenter: TaxiViewToTaxiPresenterProtocol & TaxiInterectorToTaxiPresenterProtocol = TaxiPresenter()
        let taxiInteractor: TaxiPresentorToTaxiInterectorProtocol = TaxiInteractor()
        let taxiRouter: TaxiPresenterToTaxiRouterProtocol = TaxiRouter()
        
        taxiHomeViewController.taxiPresenter = taxiPresenter
        taxiPresenter.taxiView = taxiHomeViewController
        taxiPresenter.taxiRouter = taxiRouter
        taxiPresenter.taxiInterector = taxiInteractor
        taxiInteractor.taxiPresenter = taxiPresenter
        taxiHomeViewController.rideTypeId = rideTypeId
        taxiHomeViewController.fromChatNotification = true
        return taxiHomeViewController
    }
    static func createLiveTaxiModule(rideTypeId: Int?,isfromLive:Bool,requestID:Int) -> UIViewController {
        let taxiHomeViewController  = taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.TaxiHomeViewController) as! TaxiHomeViewController
        let taxiPresenter: TaxiViewToTaxiPresenterProtocol & TaxiInterectorToTaxiPresenterProtocol = TaxiPresenter()
        let taxiInteractor: TaxiPresentorToTaxiInterectorProtocol = TaxiInteractor()
        let taxiRouter: TaxiPresenterToTaxiRouterProtocol = TaxiRouter()
        taxiHomeViewController.isFromOrderPage = isfromLive
        taxiHomeViewController.liveRequestId = requestID
        taxiHomeViewController.isFromLiveTrack = isfromLive
        taxiHomeViewController.isIndexSet = false
        taxiHomeViewController.taxiPresenter = taxiPresenter
        taxiPresenter.taxiView = taxiHomeViewController
        taxiPresenter.taxiRouter = taxiRouter
        taxiPresenter.taxiInterector = taxiInteractor
        taxiInteractor.taxiPresenter = taxiPresenter
        taxiHomeViewController.rideTypeId = rideTypeId
        taxiHomeViewController.fromChatNotification = false
        taxiHomeViewController.isRide = true
        return taxiHomeViewController
    }
    
    
    
    static var taxiStoryboard: UIStoryboard {
        return UIStoryboard(name:"Taxi",bundle: Bundle.main)
    }
}
