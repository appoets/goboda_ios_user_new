//
//  XuberProviderReviewCell.swift
//  GoJekUser
//
//  Created by on 14/04/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import SDWebImage
class XuberProviderReviewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.initialLoads()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.profileImage.setRoundCorner()
    }
    
    func setValues(review: Review) {
        
       
        self.profileImage.sd_setImage(with: URL(string: review.user?.picture ?? ""), placeholderImage: UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                        // Perform operation.
                                        if (error != nil) {
                                            // Failed to load image
                                             self.profileImage.image = UIImage(named: Constant.userPlaceholderImage)
                                        } else {
                                            // Successful in loading image
                                             self.profileImage.image = image
                                        }
                                    })
        if review.provider_comment == "" {
            self.reviewLabel.text =  "No comment"
        } else {
            self.reviewLabel.text =  review.provider_comment
        }
        self.dateLabel.text = review.created_at?.formatDateFromString(withFormat: DateFormat.ddmmyyyy)
    }
}

extension XuberProviderReviewCell {
    
    private func initialLoads() {
        reviewLabel.font = UIFont.setCustomFont(name: .light, size: .x14)
        dateLabel.font = UIFont.setCustomFont(name: .light, size: .x12)
    }
}
