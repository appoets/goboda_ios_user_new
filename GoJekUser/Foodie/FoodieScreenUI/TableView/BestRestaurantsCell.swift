//
//  BestRestaurantsCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 12/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class BestRestaurantsCell: UITableViewCell {
    
    @IBOutlet weak var storeCollection : UICollectionView!
    @IBOutlet weak var offerCollection : UICollectionView!
    @IBOutlet weak var offerView : UIView!
    @IBOutlet weak var stackView : UIStackView!
    
    
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var titleLblHeight : NSLayoutConstraint!
    @IBOutlet weak var titleLblTopHeight : NSLayoutConstraint!
    
    
    @IBOutlet weak var storeCollectionHeight : NSLayoutConstraint!
    @IBOutlet weak var offerCollectionHeight : NSLayoutConstraint!
    
    var topBanner : [UIImage] = [UIImage(named: "FoodieFlowIcon") ?? UIImage(),UIImage(named: "CourierFlowIcon") ?? UIImage()]
    
    var topRest : Bool = false {
        didSet{
            self.stackView.removeArrangedSubview(self.offerView)
            self.stackView.insertArrangedSubview(self.offerView, at: 1)
            self.titleLbl.text = "Top Picks for you"
            self.titleLbl.isHidden = false
            self.titleLblHeight.constant = 25
            self.titleLblTopHeight.constant = 20
        }
    }
    
    var topShops:[ShopsListData] = []{
        didSet{
            self.storeCollection.reloadData()
        }
    }
    
    var shopArrList:[ShopsListData] = []{
        didSet{
            self.storeCollection.reloadData()
        }
    }
    
    var promocodes : [PromocodeData] = []{
        didSet{
            self.offerCollection.reloadData()
        }
    }

    var selectedStore : ((ShopsListData) -> Void)?
    var selectedPromo : ((PromocodeData) -> Void)?
    var selectedBanner : ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupCollectionCell()
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupCollectionCell(){
        self.storeCollection.delegate = self
        self.storeCollection.dataSource = self
        self.offerCollection.delegate = self
        self.offerCollection.dataSource = self
        self.storeCollection.registerCell(withId: "FoodieStoreCell")
        self.offerCollection.registerCell(withId: "FoodieOfferCell")
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
    }
    
}

extension BestRestaurantsCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == storeCollection{
            if topRest{
                return self.topShops.count
            }else{
                return shopArrList.count
            }
           
        }else{
            if topRest{
                return self.promocodes.count
            }else{
                return self.topBanner.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == storeCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodieStoreCell", for: indexPath) as! FoodieStoreCell
            if !topRest{
                cell.store = self.shopArrList[indexPath.row]
            }else{
                cell.store = self.topShops[indexPath.row]
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodieOfferCell", for: indexPath) as! FoodieOfferCell
            if topRest{
                cell.promocodes = self.promocodes[indexPath.row]
            }else{
                cell.offerImg.image = self.topBanner[indexPath.row]
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == storeCollection{
            if !topRest{
                self.selectedStore?(self.shopArrList[indexPath.row])
            }else{
                self.selectedStore?(self.topShops[indexPath.row])
            }
            
        }else{
            if topRest{
                self.selectedPromo?(self.promocodes[indexPath.row])
            }else{
                self.selectedBanner?(indexPath.row)
            }
        }
    }
    
}

extension BestRestaurantsCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == storeCollection{
            if topRest{
                if topShops.count == 0{
                    self.storeCollectionHeight.constant = 0
                    return CGSize(width: (UIScreen.main.bounds.width / 5), height: ((UIScreen.main.bounds.width / 4)) + 40)
                }else{
                    self.storeCollectionHeight.constant = (((UIScreen.main.bounds.width / 4)) + 30)
                    return CGSize(width: (UIScreen.main.bounds.width / 5), height: ((UIScreen.main.bounds.width / 4)) + 40)
                }
                
            }else{
                if shopArrList.count == 0{
                    self.storeCollectionHeight.constant = 0
                    return CGSize(width: (UIScreen.main.bounds.width / 4) - 15, height:  40)
                }else{
                    self.storeCollectionHeight.constant = (((UIScreen.main.bounds.width / 4) - 15) + 40)
                    return CGSize(width: (UIScreen.main.bounds.width / 4) - 15, height: ((UIScreen.main.bounds.width / 4) - 15) + 40)
                }
            }
            
        }else{
            if topRest{
                self.offerCollectionHeight.constant = ((UIScreen.main.bounds.height / 10)*2) + 20
                if promocodes.count == 0{
                    return CGSize(width: (UIScreen.main.bounds.width) - 60 , height: 0 + 20)
                }else{
                    return CGSize(width: (UIScreen.main.bounds.width) - 60 , height: ((UIScreen.main.bounds.height / 10)*2) + 10)
                }
            }else{
                self.offerCollectionHeight.constant = ((UIScreen.main.bounds.height / 10)*2)
                return CGSize(width: ((UIScreen.main.bounds.width - 40) / 2), height: (UIScreen.main.bounds.height / 10)*1.8)
            }
        }
    }
}

extension BestRestaurantsCell{
    
}
