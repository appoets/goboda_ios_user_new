//
//  MakesHappyCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class MakesHappyCell: UITableViewCell {

    @IBOutlet weak var RestCollection : UICollectionView!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var seeMoreView : UIView!
    @IBOutlet weak var seeMoreLbl : UILabel!
    @IBOutlet weak var seeMoreImg : UIImageView!
    
    var eatsMakeList:[ShopsListData] = []{
        didSet{
            RestCollection.reloadData()
            if (self.eatsMakeList.count < 4 ){
                seeMoreView.isHidden = true
            }else{
                seeMoreView.isHidden = false
            }
        }
    }
    
    var selectedStore : ((ShopsListData) -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupCollection()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
        self.titleLbl.text = "Eat what makes you happy"
        self.seeMoreView.addShadow(radius: 10, color: .gray,shadowRadious : 1)
    }
    
    func setupCollection(){
        self.RestCollection.delegate = self
        self.RestCollection.dataSource = self
        self.RestCollection.registerCell(withId: "FoodiRestaurantCell")
    }
}

extension MakesHappyCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.eatsMakeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodiRestaurantCell", for: indexPath) as! FoodiRestaurantCell
            cell.store = self.eatsMakeList[indexPath.row]
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedStore?(self.eatsMakeList[indexPath.row])
    }
}

extension MakesHappyCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: (UIScreen.main.bounds.width)/4 + 10, height: (UIScreen.main.bounds.width)/4 + 10)
    }
}
