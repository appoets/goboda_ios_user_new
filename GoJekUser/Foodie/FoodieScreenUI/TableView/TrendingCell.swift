//
//  TrendingCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class TrendingCell: UITableViewCell {

    @IBOutlet weak var trendingCollection : UICollectionView!
    @IBOutlet weak var titleLbl : UILabel!
    
    var trendCusineList:[CusinesShop] = []{
        didSet{
            self.trendingCollection.reloadData()
        }
    }
    var selectedStore : ((CusinesShop) -> Void)?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
        self.titleLbl.text = "Trending Cuisines around you"
    }
    
    func setupCollection(){
        self.trendingCollection.delegate = self
        self.trendingCollection.dataSource = self
        self.trendingCollection.registerCell(withId: "TrendingCuisineCell")
    }
}

extension TrendingCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.trendCusineList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingCuisineCell", for: indexPath) as! TrendingCuisineCell
            cell.CusinesShop = self.trendCusineList[indexPath.row]
            return cell
    }
    
}

extension TrendingCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: ((UIScreen.main.bounds.width - 60) / 3), height: collectionView.frame.height)//(UIScreen.main.bounds.height / 10)*2.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedStore?(self.trendCusineList[indexPath.row])
    }
}
