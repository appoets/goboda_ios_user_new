//
//  PopularCuisinesCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PopularCuisinesCell: UITableViewCell {

    @IBOutlet weak var CuisineCollection : UICollectionView!
    @IBOutlet weak var titleLbl : UILabel!
    
    var trendCusineList:[CusinesShop] = []{
        didSet{
            self.CuisineCollection.reloadData()
        }
    }
    
    
    var selectedStore : ((CusinesShop) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
        self.titleLbl.text = "Popular Cuisines around you"
    }
    
    func setupCollection(){
        self.CuisineCollection.delegate = self
        self.CuisineCollection.dataSource = self
        self.CuisineCollection.registerCell(withId: "FoodieStoreCell")
    }
}

extension PopularCuisinesCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.trendCusineList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodieStoreCell", for: indexPath) as! FoodieStoreCell
            cell.isCusine = true
            cell.cusinesShop = self.trendCusineList[indexPath.row]
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedStore?(self.trendCusineList[indexPath.row])
    }
    
}

extension PopularCuisinesCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: (UIScreen.main.bounds.width / 4) - 15, height: ((UIScreen.main.bounds.width / 4) - 15) + 40)
    }
}
