//
//  FoodieCoouponCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class FoodieCoouponCell: UITableViewCell {

    @IBOutlet weak var couponCollection : UICollectionView!
    @IBOutlet weak var titleLbl : UILabel!
    
    var couponList:[Coupon] = []{
        didSet{
            self.couponCollection.reloadData()
        }
    }
    
    var onClickCoupon:((Coupon)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
        self.setupCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .bold, size: .x22)
        self.titleLbl.textColor = .black
        self.titleLbl.text = "Coupons For You"
    }
    
    func setupCollection(){
        self.couponCollection.delegate = self
        self.couponCollection.dataSource = self
        self.couponCollection.registerCell(withId: "CouponShopCell")
    }
}

extension FoodieCoouponCell : UICollectionViewDelegate , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.couponList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CouponShopCell", for: indexPath) as! CouponShopCell
        cell.coupon = self.couponList[indexPath.row]
        cell.labelText.text = self.couponList[indexPath.row].promo_code
        cell.OuterView.setCornerRadiuswithValue(value: 8)
        cell.couponImage.setCornerRadiuswithValue(value: 8)
        cell.GradiemtView.setCornerRadiuswithValue(value: 8)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.onClickCoupon?(self.couponList[indexPath.row])
    }
    
}

extension FoodieCoouponCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: ((UIScreen.main.bounds.width - 60) / 3), height: collectionView.frame.height) //(UIScreen.main.bounds.height / 10)*2)
    }
}
