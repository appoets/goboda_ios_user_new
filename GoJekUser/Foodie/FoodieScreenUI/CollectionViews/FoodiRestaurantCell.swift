//
//  FoodiRestaurantCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class FoodiRestaurantCell: UICollectionViewCell {
    
    @IBOutlet weak var restaurantView : UIView!
    @IBOutlet weak var restaurantImg : UIImageView!
    @IBOutlet weak var restaurantNameLbl : UILabel!
    
    var store : ShopsListData? {
        didSet{
            self.restaurantImg.sd_setImage(with: URL(string: self.store?.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.restaurantImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.restaurantImg.image = image
                           }
                       })
            
            self.restaurantNameLbl.text = self.store?.store_name ?? ""
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.restaurantView.addShadow(radius: 10, color: .gray)
        self.restaurantView.maskToBounds = true
        self.restaurantNameLbl.font = .setCustomFont(name: .medium, size: .x16)
        self.restaurantNameLbl.textColor = .black
    }
    
}
