//
//  TrendingCuisineCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class TrendingCuisineCell: UICollectionViewCell {
    
    @IBOutlet weak var cuisienImg : UIImageView!
    @IBOutlet weak var bg1 : UIView!
    @IBOutlet weak var bg2 : UIView!
    
    var CusinesShop : CusinesShop?{
        didSet{
            self.cuisienImg.sd_setImage(with: URL(string: self.CusinesShop?.images ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.cuisienImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.cuisienImg.image = image
                           }
                       })
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.bg1.addShadow(radius: 10, color: .gray,shadowRadious : 2)
        self.bg2.addShadow(radius: 10, color: .gray,shadowRadious : 2)
        self.cuisienImg.addShadow(radius: 10, color: .gray)
    }

}
