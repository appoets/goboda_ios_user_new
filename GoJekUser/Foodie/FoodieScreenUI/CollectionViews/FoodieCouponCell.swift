//
//  FoodieCouponCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 13/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class FoodieCouponCell: UICollectionViewCell {
    
    @IBOutlet weak var couponImage : UIImageView!
    @IBOutlet weak var labelText: UILabel!
    
    var coupon : Coupon?{
        didSet{
            self.couponImage.sd_setImage(with: URL(string: self.coupon?.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.couponImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.couponImage.image = image
                           }
                       })
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.couponImage.addShadow(radius: 10, color: .gray)
    }

}
