//
//  FoodieStoreCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 12/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class FoodieStoreCell: UICollectionViewCell {
    
    @IBOutlet weak var storeImg : UIImageView!
    @IBOutlet weak var storeLbl : UILabel!
    
    var store : ShopsListData? {
        didSet{
            self.storeImg.sd_setImage(with: URL(string: self.store?.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.storeImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.storeImg.image = image
                           }
                       })
            
            self.storeLbl.text = self.store?.store_name ?? ""
        }
    }
    
    var isCusine : Bool = false
    var cusinesShop : CusinesShop?{
        didSet{
            self.storeImg.sd_setImage(with: URL(string: self.cusinesShop?.images ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.storeImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.storeImg.image = image
                           }
                       })
            
            self.storeLbl.text = self.cusinesShop?.name ?? ""
        }
    }
    
    override func layoutSubviews() {
        if isCusine{
        self.storeImg.cornerRadius = storeImg.frame.width / 2
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView(){
        self.storeImg.addShadow(radius: 10, color: .gray)
        self.storeLbl.font = .setCustomFont(name: .bold, size: .x14)
        self.storeLbl.textColor = .black
    }

}
