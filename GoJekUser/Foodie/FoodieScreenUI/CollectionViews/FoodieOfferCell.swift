//
//  FoodieOfferCell.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 12/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class FoodieOfferCell: UICollectionViewCell {
    
    @IBOutlet weak var offerImg : UIImageView!

    var promocodes : PromocodeData?{
        didSet{
            self.offerImg.sd_setImage(with: URL(string: self.promocodes?.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.offerImg.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.offerImg.image = image
                           }
                       })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupView()
    }
    
    func setupView()
    {
        self.offerImg.addShadow(radius: 10, color: .gray)
    }
}
