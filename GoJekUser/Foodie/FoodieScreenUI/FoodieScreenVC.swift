//
//  FoodieScreenVC.swift
//  Goboda
//
//  Created by Sethuram's MacBook Pro on 12/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class FoodieScreenVC: UIViewController {
    
    @IBOutlet weak var foodieTable : UITableView!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var titleLbl : UILabel!
    
    var shopArrList:[ShopsListData] = []
    var promocodes : [PromocodeData] = []
    var topShops:[ShopsListData] = []
    var cusineList:[CusinesShop] = []
    var couponList:[Coupon] = []
    var restID = 1
    var trendCusineList:[CusinesShop] = []
    var eatsMakeList:[ShopsListData] = []

    var seeMoreTapped : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initalLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initalApi()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.isHidden = true
    }
    

}

extension FoodieScreenVC{
    
    func initalLoad(){
        self.setupAction()
        self.setupTable()
        self.setupView()
    }
    
    func setupAction(){
        self.backButton.addTap {
            self.navigationController?.popViewController(animated: true)
        }
    }
    func setupView(){
        self.titleLbl.font = .setCustomFont(name: .medium, size: .x22)
        self.titleLbl.textColor = .black
        self.titleLbl.text = "GOBODA FOOD"
    }
    func setupTable(){
        self.foodieTable.delegate = self
        self.foodieTable.dataSource = self
        self.foodieTable.registerCell(withId: "BestRestaurantsCell")
        self.foodieTable.registerCell(withId: "FoodieCoouponCell")
        self.foodieTable.registerCell(withId: "PopularCuisinesCell")
        self.foodieTable.registerCell(withId: "MakesHappyCell")
        self.foodieTable.registerCell(withId: "TrendingCell")
    }
}

extension FoodieScreenVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestRestaurantsCell", for: indexPath) as! BestRestaurantsCell
            cell.shopArrList = shopArrList
            cell.selectedBanner = { index in
                if index == 0{
                    let foodieDetailViewController  = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieHomeViewController) as! FoodieHomeViewController
                    foodieDetailViewController.restID = self.restID
                    self.navigationController?.pushViewController(foodieDetailViewController, animated: true)
                }else{
                    //let vc = CourierRouter.createCourierModule()
                   // UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                    let foodieDetailViewController  = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieHomeViewController) as! FoodieHomeViewController
                    foodieDetailViewController.restID = self.restID
                    self.navigationController?.pushViewController(foodieDetailViewController, animated: true)
                }
                
            }
            cell.selectedStore = {[weak self] store in
                self?.storeNavidation(restID : store.id ?? 0)
            }
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestRestaurantsCell", for: indexPath) as! BestRestaurantsCell
            cell.topRest = true
            cell.promocodes = self.promocodes
            cell.topShops = self.topShops
            cell.selectedPromo = {[weak self] promo in
                
            }
            cell.selectedStore = {[weak self] store in
                self?.storeNavidation(restID : store.id ?? 0)
            }
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodieCoouponCell", for: indexPath) as! FoodieCoouponCell
            cell.couponList = self.couponList
            cell.onClickCoupon = { [weak self] promo in
                self?.storeNavidation(restID: promo.store_id ?? 0)
            }
            
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopularCuisinesCell", for: indexPath) as! PopularCuisinesCell
            cell.trendCusineList = self.trendCusineList
            cell.selectedStore = {[weak self] store in
                self?.storeNavidation(restID : store.id ?? 0)
            }
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MakesHappyCell", for: indexPath) as! MakesHappyCell
            cell.eatsMakeList = self.eatsMakeList
            cell.seeMoreView.addTap {
                if cell.seeMoreLbl.text?.lowercased() == "see more"{
                    self.seeMoreTapped = true
                    cell.seeMoreLbl.text = "See Less"
                }else{
                    self.seeMoreTapped = false
                    cell.seeMoreLbl.text = "See More"
                }
                
                self.foodieTable.reloadData()
            }
            
            cell.selectedStore = {[weak self] store in
                self?.storeNavidation(restID : store.id ?? 0)
            }
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingCell", for: indexPath) as! TrendingCell
            cell.trendCusineList = self.trendCusineList
            cell.selectedStore = {[weak self] store in
                self?.storeNavidation(restID : store.id ?? 0)
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BestRestaurantsCell", for: indexPath) as! BestRestaurantsCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            var storeHeight  : Int = 0
            if shopArrList.count != 0 {
              storeHeight = Int(((UIScreen.main.bounds.width / 4) - 15) + 40)
            }
            return  CGFloat(storeHeight) + ((UIScreen.main.bounds.height / 10)*1.8) + 30
            
        }else if indexPath.row == 1{
            var promoHeight  : Int = 0
            var topstore  : Int = 0
            
            if promocodes.count == 0 && topShops.count == 0{
                return 0
            }else{
                if promocodes.count != 0 {
                    promoHeight = Int((((UIScreen.main.bounds.height / 10)*2) + 20))
                }
                if topShops.count != 0 {
                    topstore = Int(((UIScreen.main.bounds.width / 4) - 15) + 40)
                }
                return CGFloat(topstore) + CGFloat(promoHeight) + 80
            }
        }else if indexPath.row == 2{
            return self.couponList.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2.1) - 20
        }else if indexPath.row == 3{
            return self.trendCusineList.count == 0 ? 0 : ((UIScreen.main.bounds.width / 4) - 15) + 40 + 80
        }else if indexPath.row == 4{
            if seeMoreTapped{
                let count = self.eatsMakeList.count
                return self.eatsMakeList.count == 0 ? 0 : (CGFloat((UIScreen.main.bounds.width)/4 + CGFloat(10)) * CGFloat(Double((count / 3)) * 1.4))
            }else{
                return self.eatsMakeList.count == 0 ? 0 : (self.eatsMakeList.count < 4 ) ? ((UIScreen.main.bounds.width)/4 + 10) * 2 : ((UIScreen.main.bounds.width)/4 + 10) * 3.2
            }
            
        }else if indexPath.row == 5{
            return self.trendCusineList.count == 0 ? 0 : ((UIScreen.main.bounds.height / 10)*2) //- 100
        }
        else{
             return 0
        }
    }
    
    func storeNavidation(restID : Int){
        let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
        foodieItemsVC.restaurentId = restID
        navigationController?.pushViewController(foodieItemsVC, animated: true)
    }
}

extension FoodieScreenVC : FoodiePresenterToFoodieViewProtocol{
    
    func initalApi(){
        self.getCusineShopList()
        self.getPromoList()
        self.shopList()
    }
    
    func getCusineShopList(){
        self.foodiePresenter?.getCusineShopList(param: [:],id : 1)//self.restID)
    }
    
    func getCusineShopListResponse(foodieDetailEntity: CouponListEntity) {
        LoadingIndicator.hide()
        trendCusineList = foodieDetailEntity.responseData?.cuisinelist ?? []
//        couponList = foodieDetailEntity.responseData?.coupons ?? []
        DispatchQueue.main.async {
            self.foodieTable.reloadData()
        }
    }
    
    func getPromoList(){
        self.foodiePresenter?.getPromoCodeOrderList(Id: self.restID)
    }
    
    func getPromoCodeShopResponse(getPromoCodeResponse: PromocodeEntity) {
        promocodes = getPromoCodeResponse.responseData ?? []
        self.foodieTable.reloadData()
    }
    
    func shopList(){
        FLocationManager.shared.stop()
        FLocationManager.shared.start { (info) in
            
            let param: Parameters = ["latitude":info.latitude ?? 0,
                                     "longitude":info.longitude ?? 0]
            self.foodiePresenter?.getShopList(param: param, id: self.restID )
        }
    }
    
    func getShopListResponse(foodieDetailEntity: ShopDetailList) {
        
        shopArrList.removeAll()
        topShops.removeAll()
        eatsMakeList.removeAll()
        couponList.removeAll()
        shopArrList = foodieDetailEntity.responseData?.store_list ?? []
        topShops = foodieDetailEntity.responseData?.toppicks ?? []
        eatsMakeList = foodieDetailEntity.responseData?.toppicks ?? [] //foodieDetailEntity.responseData?.eatsmakehappy ?? []
        DispatchQueue.main.async {
            let coupons = foodieDetailEntity.responseData?.store_list?.map({$0.storepromo})
        for value in coupons ?? [] {
            for coupons in value ?? []{
                self.couponList.append(coupons)
            }
        }
        }

        self.foodieTable.reloadData()
    }
}
