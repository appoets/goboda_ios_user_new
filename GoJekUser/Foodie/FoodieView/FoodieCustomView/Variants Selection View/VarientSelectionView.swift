//
//  ItemSizeSelectionView.swift
//  GoJekUser
//
//  Created by Chan Basha on 20/07/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit

class VarientSelectionView: UIView {

    @IBOutlet weak var labelSize: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var varientTV: UITableView!
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnClose: UIButton!
    
    var SelectedvarientList : [[VarientSelection]] = [[VarientSelection]]()
    
    var varientsList : [VarientEntity]?
    var selectedID : (([Int?])->Void)?
    var id = 0
    var variantData : [VariantModel] = []
    var subVariant : [SubVariant] = []
    var idArray = [Int]()
    var idsArray : NSMutableArray = []
    
    var close : (()->Void)?
    var index = 0

    override func awakeFromNib() {
        initialLoads()
    }
    
    private func initialLoads()
    {
        setFont()
        setColor()
        setConstant()
        setDesign()
        varientTV.separatorStyle = .none
        varientTV.register(UINib(nibName: "VarientCell", bundle: .main), forCellReuseIdentifier: "VarientCell")
        varientTV.delegate = self
        varientTV.dataSource = self
        
      //  let tvheigh = bgViewHeight.multiplier + CGFloat((Double(varientsList?.count ?? 0) * 0.02))
       // bgViewHeight = bgViewHeight.setMultiplier(multiplier: tvheigh)
        
        btnConfirm.addTarget(self, action: #selector(confirmAction(sender:)), for: .touchUpInside)
        btnClose.addTarget(self, action: #selector(closeAction(sender:)), for: .touchUpInside)
       
    }
    
    
    
    override func draw(_ rect: CGRect) {
        
        for index in 0..<variantData.count {
            self.idsArray.add("")
        }
        
        
    }
    private func setFont()
    {
        labelSize.font = .setCustomFont(name: .bold, size: .x18)
        itemName.font = .setCustomFont(name: .medium, size: .x16)
        itemPrice.font = .setCustomFont(name: .medium, size: .x14)
        btnConfirm.titleLabel?.font = .setCustomFont(name: .bold, size: .x18)
        btnClose.titleLabel?.font = .setCustomFont(name: .medium, size: .x14)
    }
    private func setColor()
    {
        
        itemPrice.textColor = .darkGray
        labelSize.textColor = .white
        btnConfirm.setTitleColor(.white, for: .normal)
        btnConfirm.backgroundColor = .foodieColor
        
    }
    private func setConstant(){
        
        labelSize.text = FoodieConstant.selectSize.localized
        btnConfirm.setTitle(FoodieConstant.confirm.localized, for: .normal)
        btnClose.setTitle("Close", for: .normal)
        
    }
    
    private func setDesign(){
        
        self.bgView.layer.cornerRadius = 8
        self.itemImage.layer.cornerRadius = 6
        self.btnConfirm.layer.cornerRadius = 8
    }
    
    @IBAction func confirmAction(sender:UIButton)
    {
      
        self.selectedID?(self.idArray)
    }
    
    @IBAction func closeAction(sender:UIButton)
    {
        self.close?()
    }

}

extension VarientSelectionView : UITableViewDelegate ,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.variantData.count > 0 {
            SelectedvarientList.removeAll()
            for i in 0..<(self.variantData.count ){
                var varients : [VarientSelection] = [VarientSelection]()
                for j in 0..<(self.variantData[i].subVariant?.count ?? 0){
                    var selectedVarient : VarientSelection = VarientSelection()
                    selectedVarient.id = self.variantData[i].subVariant?[j].id ?? 0
                    selectedVarient.isVarient = false
                    varients.append(selectedVarient)
                }
                SelectedvarientList.append(varients)
                print("varientListvarientList",SelectedvarientList)
            }
            
            for (index, _) in self.variantData.enumerated() {
                if section == index {
                    return self.variantData[index].subVariant?.count ?? 0
                }
            }
        }
        
        return 0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return variantData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "VarientCell", for: indexPath) as! VarientCell
            cell.labelVarientName.text = self.variantData[indexPath.section].subVariant?[indexPath.row].name
        cell.labelPrice.text = "+" + (self.variantData[indexPath.section].subVariant?[indexPath.row].price?.setCurrency() ?? "")
            cell.selectionStyle = .none
            if self.idArray.contains(self.variantData[indexPath.section].subVariant?[indexPath.row].id ?? 0){
                cell.btnSelect.setImage(#imageLiteral(resourceName: "redtap"), for: .normal)
            }else{
                cell.btnSelect.setImage(#imageLiteral(resourceName: "deselect"), for: .normal)
            }
      
        cell.btnSelect.addTap {
            self.id = self.variantData[indexPath.section].subVariant?[indexPath.row].id ?? 0
                if self.idArray.contains(self.id){
                    self.idArray.remove(at: self.idArray.lastIndex(of: self.id) ?? 0)
                    cell.btnSelect.setImage(#imageLiteral(resourceName: "deselect"), for: .normal)
                    self.SelectedvarientList[indexPath.section][indexPath.row].isVarient = false
                }else{
                    let filterVarient = self.SelectedvarientList[indexPath.section].filter { $0.isVarient == true}
                    if filterVarient.count < (Int(self.variantData[indexPath.section].choose ?? "0") ?? 0){
                        self.idArray.append(self.id)
                        cell.btnSelect.setImage(#imageLiteral(resourceName: "redtap"), for: .normal)
                        self.SelectedvarientList[indexPath.section][indexPath.row].isVarient = true
                    }else{
                        ToastManager.show(title: "you can select only \(self.variantData[indexPath.section].choose ?? "0") Varients in \(self.variantData[indexPath.section].name ?? "")" , state: .error)
                    }
                }
        }
        return cell
        
  
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      return 45
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
      for (index, item) in self.variantData.enumerated() {
            if section == index {
                return self.variantData[index].name
            }
            
        }
        return ""
    }
    

}

struct VarientSelection {
    var isVarient : Bool?
    var id : Int?
}
