//
//  VarientCell.swift
//  GoJekUser
//
//  Created by Chan Basha on 20/07/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit

class VarientCell: UITableViewCell {
    
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var labelVarientName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    var selectedVarient : ((Int?)->Void)?
    var isVarientSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setFont()
       // btnSelect.addTarget(self, action: #selector(selectionAction(sender:)), for: .touchUpInside)
        
        
    }

    private func setFont()
    {
      
        labelVarientName.font = .setCustomFont(name: .light, size: .x16)
        labelPrice.font = .setCustomFont(name: .light, size: .x16)
        
    }
    
    
    @IBAction func selectionAction(sender:UIButton){
        
        isVarientSelected = !isVarientSelected
        
       // self.btnSelect.setImage(isVarientSelected ? #imageLiteral(resourceName: "select") : #imageLiteral(resourceName: "deselect"), for: .normal)
//        if isVarientSelected
//        {
        selectedVarient?(sender.tag)
       // }
        
    }
    
    func setSelectedVariant(isSelected :Bool) {
        
        if isVarientSelected {
           // addOnsImageView.image = UIImage(named: Constant.circleFullImage)
            self.btnSelect.setImage(UIImage(named: Constant.circleFullImage), for: .normal)

        }else {
             self.btnSelect.setImage(UIImage(named: Constant.circleImage), for: .normal)
        }
    }
    
}
