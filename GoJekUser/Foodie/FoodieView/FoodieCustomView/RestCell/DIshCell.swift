//
//  DIshCell.swift
//  Goboda
//
//  Created by Mithra Mohan on 01/04/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class DIshCell: UICollectionViewCell {

    
    
    @IBOutlet weak var imgRest : UIImageView!
    @IBOutlet weak var dishLabel : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }

}



extension DIshCell {
    
    private func initialLoads() {
        setDesign()
    }
    
    private func setDesign() {
        imgRest.layer.cornerRadius = 10
    }
    
    func setShopListData(data: ShopsListData){
 
        imgRest.sd_setImage(with: URL(string: data.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                // Perform operation.
                if (error != nil) {
                    // Failed to load image
                    self.imgRest.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                } else {
                    // Successful in loading image
                    self.imgRest.image = image
                }
            })

        dishLabel.text = data.store_name
  
        
    }
    
    func setEatMakes(data: ShopsListData){
 
        imgRest.sd_setImage(with: URL(string: data.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                // Perform operation.
                if (error != nil) {
                    // Failed to load image
                    self.imgRest.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                } else {
                    // Successful in loading image
                    self.imgRest.image = image
                }
            })

        dishLabel.text = data.item_name ?? ""
  
        
    }
}
