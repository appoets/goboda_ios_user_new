//
//  ShopCell.swift
//  Goboda
//
//  Created by Mithra Mohan on 30/03/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage
class ShopCell: UICollectionViewCell {

    
    @IBOutlet weak var imgRest : UIImageView!
    @IBOutlet weak var dishLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}


extension ShopCell {
    
    func setShopListData(data: ShopsListData){
        
        DispatchQueue.main.async {
            self.imgRest.sd_setImage(with: URL(string: data.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        self.imgRest.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                    } else {
                        // Successful in loading image
                        self.imgRest.image = image
                    }
                })
        }
        
 
      

        dishLabel.text = data.store_name
  
        
    }
    
    func setCusineListData(data: CusinesShop){
        print("data>>",data.images)
 
        DispatchQueue.main.async {
        
            self.imgRest.sd_setImage(with: URL(string: data.images ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                // Perform operation.
                if (error != nil) {
                    // Failed to load image
                    self.imgRest.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                } else {
                    // Successful in loading image
                    self.imgRest.image = image
                }
            })
            
        }

        dishLabel.text = data.name ?? ""
        dishLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
        self.imgRest.layer.cornerRadius = self.imgRest.frame.height/2
        
  
        
    }
    
    
}
