//
//  FoodieAddOns.swift
//  GoJekUser
//
//  Created by CSS on 07/05/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class FoodieAddOns: UIView {
    
    @IBOutlet weak var addOnsTableView: UITableView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var closeImageView: UIImageView!
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var addOnHeight: NSLayoutConstraint!
    
    var onClickClose:(()->Void)?
    var AddonsArr:[Itemsaddon] = []
    var CartAddonsArr:[Cartaddon] = []
    var variantsList : [VarientEntity] = []
    var variantData : [VariantModel] = []
    var cartVariantData : [Cartvarients] = []
    var sectionCount : Int = 0
    var selectedVariantId : Int = 0
    var productAmount:Double = 0
    var staticCount:Double = 0
    var addonsItem:NSMutableArray = []
    weak var delegate: FoodieAddOnsProtocol?
    var addOnsCount : Int = 0
    var tagCount = 0
    var index = 0
    var isplus = false
    var isCartPage = false
    var staticVariantCount : [Int] = []
    var itemID  = 0 {
        didSet{
            
            if itemID != 0 {
                isVariant = true
            }
        }
    }
    var isVariant = false {
        didSet {
            if isVariant {
                if itemID != 0 {
                    self.foodiePresenter?.getVarientList(itemID: self.itemID)
                }
            }
            
        }
    }
    var sellectedItemID = -1
    var sellectedItemArray : [Int] = [Int]()
    override func awakeFromNib() {
        super.awakeFromNib()
      
        initialLoad()
    }
    
    private func initialLoad(){
        
        DispatchQueue.main.async {
            self.itemImageView.setCornerRadiuswithValue(value: 8)
        }
        
        if isVariant
        {
            self.foodiePresenter?.getVarientList(itemID: self.itemID)
        }
        self.addOnsTableView.register(UINib(nibName: FoodieConstant.FoodieAddOnsCell,bundle: nil), forCellReuseIdentifier: FoodieConstant.FoodieAddOnsCell)
        addOnsTableView.register(UINib(nibName: "VarientCell", bundle: .main), forCellReuseIdentifier: "VarientCell")
        setFont()
        addOnsTableView.delegate = self
        addOnsTableView.dataSource = self
        addOnsTableView.alwaysBounceVertical = false
        addOnsTableView.tableFooterView = UIView()
//        self.sectionCount = self.variantsList.count + self.AddonsArr.count
//        addOnsTableView.reloadData()
       
        closeImageView.image = UIImage(named: Constant.closeImage)?.imageTintColor(color1: .lightGray)
        let closeViewGesture = UITapGestureRecognizer(target: self, action: #selector(closeButtonAction(_:)))
        self.closeView.addGestureRecognizer(closeViewGesture)
        
        doneButton.addTarget(self, action: #selector(doneButtonAction), for: .touchUpInside)
        
        doneButton.backgroundColor = .foodieColor
        doneButton.setTitle(FoodieConstant.SAdd.localized.uppercased(), for: .normal)
        doneButton.setTitleColor(UIColor.white, for: .normal)
        doneButton.setBothCorner()
        itemPriceLabel.textColor = .lightGray
        
        
    }
    
    func setPrice(addOnAmount:Double,isPlus:Bool){
       
        if isPlus{
            staticCount = staticCount + addOnAmount
        }else{
            staticCount = staticCount - addOnAmount
        }
        self.doneButton.setTitle(FoodieConstant.SAdd.localize() + " " + staticCount.setCurrency(), for: .normal)
    }
    
    func setValues(addOns:[Itemsaddon],variantsData:[VariantModel],cartVariants:[Cartvarients],
                   cartAddons:[Cartaddon],iscartData:Bool,ProductPrice:Double?){
        
        self.AddonsArr = addOns
        self.variantData = variantsData
        for _ in self.AddonsArr{
            self.addonsItem.add("")
        }
        if iscartData{
        self.CartAddonsArr = cartAddons
        self.cartVariantData = cartVariants
        }
//        self.productAmount = ProductPrice ?? 0
        self.staticCount = ProductPrice ?? 0
        self.addOnsTableView.reloadInMainThread()
        self.doneButton.setTitle(FoodieConstant.SAdd.localize() + " " + staticCount.setCurrency(), for: .normal)
        for i in 0 ... variantsData.count{
            self.staticVariantCount.append(0)
        }
        self.addOnsTableView.reloadData()
    }
    
    
    @objc func closeButtonAction(_ sender: UITapGestureRecognizer) {
        self.onClickClose!()
        
    }
    private func setFont(){
        itemNameLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        itemPriceLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        doneButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x14)
        
    }
    
    @objc func doneButtonAction(){
        var varients : String = ""
        if self.sellectedItemArray.count > 0{
            for i in 0..<self.sellectedItemArray.count{
                if i == 0{
                    varients = "\(self.sellectedItemArray[i])"
                }else{
                    varients = "\(varients),\(self.sellectedItemArray[i])"
                }
            }
        }
        print("self.sellectedItemArray",varients)
        
        delegate?.ondoneAction(addonsItem: addonsItem,indexPath:index,tag:tagCount,isplus: isplus, isVariant: isVariant, variantItemID: varients)
        
    }
    
}

extension FoodieAddOns: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if AddonsArr.count > 0{
            return self.variantData.count + 1
        }else{
            return self.variantData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        for (index,value) in self.variantData.enumerated(){
            if section == index{
                return value.subVariant?.count ?? 0
            }
        }
        if self.AddonsArr.count > 0
        {
        if section == self.variantData.count{
               if isCartPage {
                    return AddonsArr.count
            }else{
                   return AddonsArr.count
                }
           }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // if self.AddonsArr.count > 0{
            if indexPath.section == self.variantData.count{
                let cell = self.addOnsTableView.dequeueReusableCell(withIdentifier: FoodieConstant.FoodieAddOnsCell, for: indexPath) as! FoodieAddOnsCell
                
                if isCartPage {
                    
                    let addOnsArr = AddonsArr[indexPath.row]
                    cell.setSelectedAddons(isSelected: self.CartAddonsArr.contains(where: { (element) -> Bool in
                        
                        if element.store_item_addons_id == addOnsArr.id {
                            addonsItem.replaceObject(at: indexPath.row, with: addOnsArr.id?.toString() ?? "")
                            return true
                        } else {
                            addonsItem.replaceObject(at: indexPath.row, with: "")
                            return false
                        }
                    }))
                    cell.addOnsNameLabel.text = addOnsArr.addonName
                    cell.priceLabel.text = Double(addOnsArr.price ?? 0).setCurrency()
                    cell.addOnsNameBTn.addTap {
                        //                    let cell = addOnsTableView.cellForRow(at: indexPath) as! FoodieAddOnsCell
                        let addOnsArr = self.AddonsArr[indexPath.row]
                        if cell.addOnsImageView.image?.isEqual(to: UIImage(named: Constant.circleImage) ?? UIImage()) ?? false  {
                            cell.addOnsImageView.image = UIImage(named: Constant.circleFullImage)
                            
//                            self.addonsItem.add(addOnsArr.id?.toString() ?? "")
                            self.addonsItem.replaceObject(at: indexPath.row, with: addOnsArr.id?.toString() ?? "")
//                            self.isVariant = false
                        }else{
                            cell.addOnsImageView.image = UIImage(named: Constant.circleImage)
//                            self.addonsItem.removeObject(at: indexPath.row)
                            self.addonsItem.replaceObject(at: indexPath.row, with: "")
//                            self.isVariant = false
                        }
                    }
                }else{
                    let addOnsArr = AddonsArr[indexPath.row]
                    cell.addOnsNameLabel.text = addOnsArr.addonName
                    cell.priceLabel.text = Double(addOnsArr.price ?? 0).setCurrency()
//                    cell.priceLabel.isHidden = true
                
                    cell.addOnsNameBTn.addTap {
                    
                        //  let cell = addOnsTableView.cellForRow(at: indexPath) as! FoodieAddOnsCell
                        let addOnsArr = self.AddonsArr[indexPath.row]
                        if cell.addOnsImageView.image?.isEqual(to: UIImage(named: Constant.circleImage) ?? UIImage()) ?? false  {
                            if self.addOnsCount >= 3 {
                                ToastManager.show(title: "You cannot add More than 3 Add Ons", state: .warning)
                                
                         }else{
                            cell.addOnsImageView.image = UIImage(named: Constant.circleFullImage)
//                            self.addonsItem.add(addOnsArr.id?.toString() ?? "")
                            self.addOnsCount = self.addOnsCount + 1
                            self.setPrice(addOnAmount: Double(addOnsArr.price ?? 0), isPlus: true)
                            self.addonsItem.replaceObject(at: indexPath.row, with: addOnsArr.id?.toString() ?? "")
                        }
//                            self.isVariant = false
                        }else{
                            cell.addOnsImageView.image = UIImage(named: Constant.circleImage)
                            self.setPrice(addOnAmount: Double(addOnsArr.price ?? 0), isPlus: false)
                            self.addOnsCount = self.addOnsCount - 1
//                            self.addonsItem.removeObject(at: indexPath.row)
                            self.addonsItem.replaceObject(at: indexPath.row, with: "")
//                            self.isVariant = false
                        }
                    }
                }
            }else{
                if isCartPage{
                    let cell = self.addOnsTableView.dequeueReusableCell(withIdentifier: FoodieConstant.FoodieAddOnsCell, for: indexPath) as! FoodieAddOnsCell
                    let object = self.variantData[indexPath.section]
                    cell.priceLabel.isHidden = true
//                    if object.subVariant?[indexPath.row].price ?? 0 == 0 {
//                        cell.priceLabel.isHidden = true
//                    }else{
//                        cell.priceLabel.isHidden = false
//                        cell.priceLabel.text = Double(object.subVariant?[indexPath.row].price ?? 0).setCurrency()
//                    }
                    
                    cell.addOnsNameLabel.text = object.subVariant?[indexPath.row].name
                    cell.setSelectedAddons(isSelected: self.cartVariantData.contains(where: { (element) -> Bool in
                       
                        
                        if element.varients?.name == variantData[indexPath.section].subVariant?[indexPath.row].name{
                            self.selectedVariantId = variantData[indexPath.section].subVariant?[indexPath.row].id ?? 0
                            return true
                        } else {
                            self.selectedVariantId = 0
                            return false
                        }
                    }))
                    cell.addOnsNameBTn.addTap {
                        if self.variantData.count > 0 {
                            
                            self.isVariant = true
                            let object = self.variantData[indexPath.section]
                            let item = object.subVariant?[indexPath.row]
                            
                            if cell.addOnsImageView.image?.isEqual(to: UIImage(named: Constant.circleImage) ?? UIImage()) ?? false  {
                                cell.addOnsImageView.image = UIImage(named: Constant.circleFullImage)
    //                            self.variantData?[indexPath.section].replaceObject(at: indexPath.row, with: item?.id?.toString() ?? "")
                                
                                if let itemID  = item?.id {
                                    self.sellectedItemID = itemID
                                    self.sellectedItemArray.append(itemID)
                                }
                            }else{
                                self.isVariant = false
                                cell.addOnsImageView.image = UIImage(named: Constant.circleImage)
    //                            self.addonsItem.replaceObject(at: indexPath.row, with: "")
                                self.sellectedItemID = -1
                                if self.sellectedItemArray.contains( item?.id ?? 0){
                                    self.sellectedItemArray.remove(at: self.sellectedItemArray.indexesOf(object: item?.id ?? 0))
                                    
                                }
                            }}
                    }
                    return cell
                }else{
                let cell = self.addOnsTableView.dequeueReusableCell(withIdentifier: FoodieConstant.FoodieAddOnsCell, for: indexPath) as! FoodieAddOnsCell
                let object = self.variantData[indexPath.section]
//                cell.priceLabel.text = Double(object.subVariant?[indexPath.row].price ?? 0).setCurrency()
                if object.subVariant?[indexPath.row].price ?? 0 == 0 {
                   cell.priceLabel.isHidden = true
                }else{
                   cell.priceLabel.isHidden = false
                   cell.priceLabel.text = Double(object.subVariant?[indexPath.row].price ?? 0).setCurrency()
                }
            if self.sellectedItemArray.contains(object.subVariant?[indexPath.row].id ?? 0){
            cell.addOnsImageView.image = UIImage(named: Constant.circleFullImage)
                }else{
                cell.addOnsImageView.image = UIImage(named: Constant.circleImage)
                }
                cell.addOnsNameLabel.text = object.subVariant?[indexPath.row].name
                cell.addOnsNameBTn.addTap {
                    if self.variantData.count > 0 {
                    
                        self.isVariant = true
                        let object = self.variantData[indexPath.section]
                        let item = object.subVariant?[indexPath.row]
                    
                        
                        if cell.addOnsImageView.image?.isEqual(to: UIImage(named: Constant.circleImage) ?? UIImage()) ?? false  {
                            cell.addOnsImageView.image = UIImage(named: Constant.circleFullImage)
                            self.setPrice(addOnAmount: Double(object.subVariant?[indexPath.row].price ?? 0), isPlus: true)
//                            self.variantData?[indexPath.section].replaceObject(at: indexPath.row, with: item?.id?.toString() ?? "")
                            
                            if let itemID  = item?.id {
                                self.sellectedItemID = itemID
                                self.sellectedItemArray.append(itemID)
                                self.checkVariant(count: self.staticVariantCount[indexPath.section], TotalCount: Int(object.choose ?? "0") ?? 0, isPlus: true, index: indexPath.row, section: indexPath.section)
                            }
                        }else{
                            self.isVariant = false
                            cell.addOnsImageView.image = UIImage(named: Constant.circleImage)
//                            self.addonsItem.replaceObject(at: indexPath.row, with: "")
                            self.setPrice(addOnAmount: Double(object.subVariant?[indexPath.row].price ?? 0), isPlus: false)
                            self.checkVariant(count: self.staticVariantCount[indexPath.section], TotalCount: Int(object.choose ?? "0") ?? 0, isPlus: false, index: indexPath.row, section: indexPath.section)
                            self.sellectedItemID = -1
                            if self.sellectedItemArray.contains( item?.id ?? 0){
                                self.sellectedItemArray.remove(at: self.sellectedItemArray.indexesOf(object: item?.id ?? 0))
                               
                            }
                        }}
                   
                }
//                cell.priceLabel.isHidden = true
                return cell
                }
            }
            //}
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = UIColor.white
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.25
        if section == self.variantData.count {
            label.text = FoodieConstant.Addons.localized
        }else{
            label.text = (self.variantData[section].name ?? "") + "(You can add Upto \(self.variantData[section].choose ?? "") Variants)"
        }
        
        
        label.font =  UIFont.setCustomFont(name: .medium, size: .x16) // my custom font
        label.textColor = UIColor.black // my custom colour
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    
}

extension FoodieAddOns: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if AddonsArr.count > 2 {
            addOnHeight.constant = CGFloat(AddonsArr.count*8)
        }else{
            addOnHeight.constant = CGFloat(AddonsArr.count)
        }
        return  44
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // remove bottom extra 20px space.
        return CGFloat.leastNormalMagnitude
    }

    func checkVariant(count:Int,TotalCount:Int,isPlus:Bool,index:Int,section:Int){
//        staticVariantCount[section] = count
        let indexPath = IndexPath(row: index, section: section)
        let cell = self.addOnsTableView.cellForRow(at: indexPath) as! FoodieAddOnsCell
//       staticVariantCount[section] = count
        if isPlus{
            staticVariantCount[section] =  staticVariantCount[section] + 1
        }else{
            staticVariantCount[section] =  staticVariantCount[section] - 1
        }
        if staticVariantCount[section] > TotalCount{
            let object = self.variantData[section]
            let item = object.subVariant?[index]
            if self.sellectedItemArray.contains( item?.id ?? 0){
                self.sellectedItemArray.remove(at: self.sellectedItemArray.indexesOf(object: item?.id ?? 0))
                if cell.addOnsImageView.image?.isEqual(to: UIImage(named: Constant.circleImage) ?? UIImage()) ?? false  {
                    cell.addOnsImageView.image = UIImage(named: Constant.circleFullImage) ?? UIImage()
                }else{
                    cell.addOnsImageView.image = UIImage(named: Constant.circleImage) ?? UIImage()
                }
            }
            staticVariantCount[section] =  staticVariantCount[section] - 1
            self.setPrice(addOnAmount: Double(object.subVariant?[indexPath.row].price ?? 0), isPlus: false)
            ToastManager.show(title: "You cannot add More than \(object.choose ?? "") Variants", state: .warning)
        }else{
            
       }
    }

}

extension FoodieAddOns : FoodiePresenterToFoodieViewProtocol
{
    func getVarientResponse(response: VarientResponse) {
        
        if let variant = response.responseData {
            
            self.variantsList = variant
//            self.addOnsTableView.reloadData()
        }
        
    }
}



//Forward process
protocol FoodieAddOnsProtocol: class {
    
    func ondoneAction(addonsItem: NSMutableArray,indexPath:Int,tag:Int,isplus: Bool,isVariant:Bool,variantItemID:String)
}
