//
//  FoodieInstructions.swift
//  Goboda
//
//  Created by Sethuram Vijayakumar on 17/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class FoodieInstructions: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var instructionTextView: CustomTextField!
    @IBOutlet weak var instructionDescriptionLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var onClickAddAction:((String?)->Void)?
    var onClickClose:(()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.intialLoads()
    }
}


extension FoodieInstructions {
    
    private func intialLoads(){
        self.localize()
        self.addButton.setTitleColor(.appPrimaryColor, for: .normal)
        self.instructionDescriptionLabel.textColor = .red
        
    }
    private func localize(){
        self.titleLabel.text  = FoodieConstant.specialCookingInstruction.localized
        self.instructionTextView.placeholder = FoodieConstant.cookingInstruction.localized
        self.instructionDescriptionLabel.text = FoodieConstant.cookingDescriptions.localized
        self.addButton.setTitle(FoodieConstant.add.localize(), for: .normal)
        self.addButton.setBorder(width: 1, color: .appPrimaryColor)
        self.addButton.setCorneredElevation()
        self.addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        self.closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
    }
    
    @objc private func addButtonAction(){
        
        guard let instructionText = self.instructionTextView.text , !instructionText.isEmpty else{
            ToastManager.show(title: "The Instruction is not Entered", state: .warning)
            return
        }
        self.onClickAddAction?(instructionText)
        
        
    }
    
    @objc private func closeButtonAction(){
        self.onClickClose?()
    }
    
}
