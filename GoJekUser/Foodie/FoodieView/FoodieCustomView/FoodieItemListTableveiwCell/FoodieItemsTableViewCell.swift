//
//  FoodieItemsTableViewCell.swift
//  GoJekUser
//
//  Created by Thiru on 07/03/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class FoodieItemsTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlet
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var varientNameLbl: UILabel!
    @IBOutlet weak var instrNameLbl: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var itemsaddView: PlusMinusView!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var addonsLabel: UILabel!
    @IBOutlet weak var customizeButton: UIButton!
    @IBOutlet weak var imageBGViewWidth: NSLayoutConstraint!
    @IBOutlet weak var imageBGView: UIView!
    
    var foodieAddonsgaDelegate: ShowAddonsDelegates?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoad()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        itemsaddView.setCornerRadius()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//MARK: - LocalMethod
extension FoodieItemsTableViewCell {
    
    private func initialLoad() {
        addonsLabel.isHidden = true
        customizeButton.isHidden = true
        
        itemsaddView.buttonColor = .foodieColor
        contentView.backgroundColor = .veryLightGray
        itemNameLabel.font = UIFont.setCustomFont(name: .medium, size: .x18)
        priceLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        customizableLabel.text = FoodieConstant.Customizable.localized
        customizableLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
        customizableLabel.textColor = .lightGray
        itemNameLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        priceLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        customizeButton.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x10)
        addonsLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        customizeButton.setTitle(FoodieConstant.customize.localized, for: .normal)
        customizeButton.setImage(UIImage(named: FoodieConstant.ic_downarrow), for: .normal)
        customizeButton.addTarget(self, action: #selector(tapCustomize(_:)), for: .touchUpInside)
        if CommonFunction.checkisRTL() {
            customizeButton.changeToRight(spacing: -6)
        }else {
            customizeButton.semanticContentAttribute = .forceRightToLeft
            customizeButton.sizeToFit()
            customizeButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: -6, bottom: 0, right: 0)
        }
        
        addonsLabel.textColor = .lightGray
        DispatchQueue.main.async {
            self.itemImageView.setCornerRadiuswithValue(value: 5.0)
            self.backView.addShadow(radius: 5.0, color: UIColor.foodieColor.withAlphaComponent(0.1))
        }
        
    }
    @objc func tapCustomize(_ sender: UIButton){
        
        foodieAddonsgaDelegate?.addonsCustomize(tag: sender.tag)
    }
    
}
protocol ShowAddonsDelegates {
    
    func addonsCustomize(tag: Int)
}



