//
//  FoodieItemsCell.swift
//  GoJekUser
//
//  Created by CSS on 21/06/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class FoodieItemsCell: UITableViewCell {
    
    //MARK: - IBOutlet
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var itemsaddView: PlusMinusView!
    @IBOutlet weak var customizableLabel: UILabel!
    @IBOutlet weak var discountPriceLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var vegImgVw: UIImageView!
    @IBOutlet weak var imageOuterView: UIView!

    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    var foodieAddonsgaDelegate: ShowAddonsDelegates?

    var isVeg: Bool = false {
          didSet {
              vegImgVw.image = UIImage(named: isVeg ? FoodieConstant.ic_veg : FoodieConstant.ic_nonveg)
          }
      }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialLoad()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        itemImageView?.applyshadowWithCorner(containerView : imageOuterView, cornerRadious : 5.0)
    }
    
}
//MARK: - LocalMethod
extension FoodieItemsCell {
    
    private func initialLoad() {
        
        self.itemsaddView.buttonColor = .foodieColor
        self.itemsaddView.layer.cornerRadius = self.itemsaddView.frame.height/2
        self.itemsaddView.layer.borderWidth = 0.7
        self.itemsaddView.layer.borderColor = UIColor.darkGray.cgColor
        contentView.backgroundColor = .veryLightGray
        self.priceLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        customizableLabel.text = FoodieConstant.Customizable.localized
        customizableLabel.font = UIFont.setCustomFont(name: .medium, size: .x12)
        customizableLabel.textColor = .lightGray
        customizableLabel.isHidden = true
        qtyLabel.textColor = .lightGray
        itemNameLabel.font = UIFont.setCustomFont(name: .medium, size: .x16)
        priceLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        qtyLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        DispatchQueue.main.async {
            self.itemImageView.setCornerRadiuswithValue(value: 5.0)
            self.backView.addShadow(radius: 5.0, color: UIColor.foodieColor.withAlphaComponent(0.1))
        }
        
        descriptionLabel.font = UIFont.setCustomFont(name: .medium, size: .x10)
        
    }
    @objc func tapCustomize(_ sender: UIButton){
        
        foodieAddonsgaDelegate?.addonsCustomize(tag: sender.tag)
    }
    
}

