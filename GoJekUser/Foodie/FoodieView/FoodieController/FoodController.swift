//
//  FoodController.swift
//  Goboda
//
//  Created by Mithra Mohan on 22/03/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import Alamofire

class FoodController: UIViewController {

    @IBOutlet weak var foodTableView: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var restCollectionView: UICollectionView!
    @IBOutlet weak var btnFoodieFlow: UIButton!
    @IBOutlet weak var btnCourierFlow: UIButton!

    
    
    var shopArrList:[ShopsListData] = []
    var promocodes : [PromocodeData] = []
    var topShops:[ShopsListData] = []
    var cusineList:[CusinesShop] = []
    var couponList:[Coupon] = []
    var restID = 0
    var trendCusineList:[CusinesShop] = []
    var eatsMakeList:[ShopsListData] = []
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("restID>",restID)

        initialLoads()
        navigationController?.isNavigationBarHidden = false
        restCollectionView.dataSource = self
        restCollectionView.delegate = self

    }

}


extension FoodController {

    private func initialLoads() {
        DispatchQueue.main.async {
            FLocationManager.shared.stop()
            FLocationManager.shared.start { (info) in
                 print(info.longitude ?? 0.0)
                 print(info.latitude ?? 0.0)
                let param: Parameters = ["latitude":info.latitude ?? 0.0,
                                        "longitude":info.longitude ?? 0.0]
                self.foodiePresenter?.getShopList(param: param, id: self.restID )
            }
        }

        self.foodTableView.register(UINib(nibName: HomeConstant.RestaurantCell, bundle: nil), forCellReuseIdentifier: HomeConstant.RestaurantCell)
        restCollectionView.register(UINib(nibName: HomeConstant.ShopCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.ShopCell)
        foodTableView.reloadData()
        self.btnCourierFlow.addTarget(self, action: #selector(courierFlowAction), for: .touchUpInside)
    }
    
    @IBAction func foodieActionFlow() {
        
    }
    
    @IBAction func courierFlowAction() {
        let vc = CourierRouter.createCourierModule()
        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - UITableViewDataSource
extension FoodController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 1 :
            return 2
        case 0 :
            return 0
        default:
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return viewHeader
        }
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 300
        } else {
            return  0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeConstant.RestaurantCell, for: indexPath) as! RestaurantCell
        cell.collectionViewRest.register(UINib(nibName: HomeConstant.BannerCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.BannerCell)
        cell.collectionViewRest.register(UINib(nibName: HomeConstant.ShopCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.ShopCell)
        cell.collectionViewRest.register(UINib(nibName: HomeConstant.DIshCell, bundle: nil), forCellWithReuseIdentifier: HomeConstant.DIshCell)
        
        cell.collectionViewRest.delegate = self
        cell.collectionViewRest.dataSource  = self
        
        cell.titleLabel.text = indexPath.section == 1 ? "Top picks for you" : indexPath.section == 2 ? "Coupons for you" : indexPath.section == 3 ? "Popular cusines around you" : indexPath.section == 4 ? "Eat what you makes happy" : indexPath.section == 5 ? "Trending cusines around you" : ""
        
        cell.collectionViewRest.accessibilityIdentifier = indexPath.section == 1 ? (indexPath.row == 0 ? "toppicks" : "promo") : indexPath.section == 2 ? "coupons" : indexPath.section == 3 ? "popular" : indexPath.section == 4 ? "fav" : indexPath.section == 5 ? "trending" : "none"
        
        if indexPath.section == 4 {
            if let layout = cell.collectionViewRest.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }
        }
        
        if indexPath.section == 1 && indexPath.row == 1 {
            cell.titleLabel.isHidden = true
        } else {
            cell.titleLabel.isHidden = false
        }
        
        cell.collectionViewRest.reloadData()
        return cell
    }
 
    
}

// MARK: - UITableViewDelegate
extension FoodController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            if topShops.count != 0 {
                return  indexPath.row == 0 ? CGFloat(150) : 200
            }else{
                return 0
            }
            
        }

        if indexPath.section == 3 {
            return 150
        }
        
        if indexPath.section == 4 {
            return 200 //400
        }
        if indexPath.section == 5 {
            return 220
        }
        return  200 //UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// MARK: - UICollectionViewDelegate

extension FoodController : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == restCollectionView  {
            return shopArrList.count
        }
        if collectionView.accessibilityIdentifier == "promo" {
            return promocodes.count
        }
        if  collectionView.accessibilityIdentifier == "toppicks" {
            
            return topShops.count
        }
        if  collectionView.accessibilityIdentifier == "popular" {
            return trendCusineList.count
        }
        if collectionView.accessibilityIdentifier == "coupons" {
            return couponList.count
        }
        if collectionView.accessibilityIdentifier == "trending" {
            return trendCusineList.count
        }
        if collectionView.accessibilityIdentifier == "fav" {
            return eatsMakeList.count
        }
        return shopArrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.ShopCell, for: indexPath) as! ShopCell
        if collectionView == restCollectionView{
            cell.setShopListData(data: shopArrList[indexPath.row])
            return cell
        }
        else if collectionView.accessibilityIdentifier == "popular" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.BannerCell, for: indexPath) as! BannerCell
            cell.setCusineListData(data: trendCusineList[indexPath.row])
            return cell
        }
      else if collectionView.accessibilityIdentifier == "toppicks" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.ShopCell, for: indexPath) as! ShopCell
            if topShops.count != 0 {
                if collectionView.accessibilityIdentifier == "toppicks" {
                    cell.setShopListData(data: topShops[indexPath.row])
                }
            }
            return cell
        }
        else if collectionView.accessibilityIdentifier == "fav" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.DIshCell, for: indexPath) as! DIshCell
            cell.setEatMakes(data: eatsMakeList[indexPath.row])
            return cell
        }
        
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConstant.BannerCell, for: indexPath) as! BannerCell
            if collectionView.accessibilityIdentifier == "promo" {
                cell.titleLabel.isHidden = false
                cell.setData(values: promocodes[indexPath.row])
            }
            if collectionView.accessibilityIdentifier == "coupons" {
                cell.titleLabel.isHidden = false
                cell.setData(values: couponList[indexPath.row])
            }
            if collectionView.accessibilityIdentifier == "trending" {
                cell.titleLabel.isHidden = true
                cell.setCusineListData(data: trendCusineList[indexPath.row])
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        if collectionView == restCollectionView {
            return CGSize(width: 100, height: 100)
        }
        else {
            
            if collectionView.accessibilityIdentifier == "toppicks" {
                
                if topShops.count != 0 {
                    print("top>")
                    return CGSize(width: 100, height: 100)
                }else{
                    print("rr>>")
                    
                    return CGSize(width: 0, height: 0)
                }
               
            }
            if collectionView.accessibilityIdentifier == "promo" {
                return CGSize(width: collectionView.frame.width, height: 160)
            }
            if collectionView.accessibilityIdentifier == "coupons" {
                return CGSize(width: collectionView.frame.width/3, height: 160)
            }
            if collectionView.accessibilityIdentifier == "popular" {
                return CGSize(width: 100, height: 100)
            }
            if collectionView.accessibilityIdentifier == "fav" {
                return CGSize(width: (collectionView.frame.width/3) - 10, height: 140)
            }
            if collectionView.accessibilityIdentifier == "trending" {
                return CGSize(width: collectionView.frame.width/3.1, height: 180)
            }
            return CGSize(width: collectionView.frame.width - 100, height: 160)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView == restCollectionView {
            let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
            foodieItemsVC.restaurentId = shopArrList[indexPath.row].id ?? 0
            navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
        if collectionView.accessibilityIdentifier == "coupons" {
            let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
            foodieItemsVC.restaurentId = couponList[indexPath.row].store_id ?? 0
            navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
        if collectionView.accessibilityIdentifier == "popular" {
            let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
            foodieItemsVC.restaurentId = trendCusineList[indexPath.row].id ?? 0
            navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
        
        if collectionView.accessibilityIdentifier == "toppicks" {
            let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
            foodieItemsVC.restaurentId = topShops[indexPath.row].id ?? 0
            navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
        
        if collectionView.accessibilityIdentifier == "fav" {
                let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
            foodieItemsVC.restaurentId = eatsMakeList[indexPath.row].store_id ?? 0
                navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
        if collectionView.accessibilityIdentifier == "trending" {
                let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
                foodieItemsVC.restaurentId = trendCusineList[indexPath.row].id ?? 0
                navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
        if collectionView.accessibilityIdentifier == "promo" {
            let foodieItemsVC = FoodieRouter.foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
            foodieItemsVC.restaurentId = promocodes[indexPath.row].id ?? 0
            navigationController?.pushViewController(foodieItemsVC, animated: true)
        }
    }
}



//MARK: - FoodiePresenterToFoodieViewProtocol

extension FoodController: FoodiePresenterToFoodieViewProtocol {
    
    func getListOfStoresResponse(getStoreResponse: StoreListEntity) {
        shopArrList = getStoreResponse.responseData ?? []
        cusineList = getStoreResponse.responseData?.first?.store_cusinie ?? []
        DispatchQueue.main.async {
            self.foodTableView.reloadData()
            self.restCollectionView.reloadData()
        }
    }
    
    
    func getTopShopsResponse(foodieDetailEntity: StoreListEntity) {
        self.topShops = foodieDetailEntity.responseData ?? []
        FLocationManager.shared.stop()
        self.foodiePresenter?.getPromoCodeOrderList(Id: self.restID)
    }
    
    
    func getShopListResponse(foodieDetailEntity: ShopDetailList) {
        shopArrList = foodieDetailEntity.responseData?.store_list ?? []
        topShops = foodieDetailEntity.responseData?.toppicks ?? []
        eatsMakeList = foodieDetailEntity.responseData?.eatsmakehappy ?? []
        self.foodiePresenter?.getTopShops(param: [:])
    }
 
    func getPromoCodeShopResponse(getPromoCodeResponse: PromocodeEntity) {
        promocodes = getPromoCodeResponse.responseData ?? []
        self.foodiePresenter?.getCusineShopList(param: [:],id : self.restID)
    }
    
    func getCusineShopListResponse(foodieDetailEntity: CouponListEntity) {
        LoadingIndicator.hide()
        trendCusineList = foodieDetailEntity.responseData?.cuisinelist ?? []
        couponList = foodieDetailEntity.responseData?.coupons ?? []
        DispatchQueue.main.async {
            self.foodTableView.reloadInputViews()
            self.foodTableView.reloadInMainThread()
            self.restCollectionView.reloadData()
        }
    }
}
