//
//  CouponShopCell.swift
//  Goboda
//
//  Created by Suren on 24/07/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import SDWebImage

class CouponShopCell: UICollectionViewCell {

    @IBOutlet weak var couponImage : UIImageView!
    @IBOutlet weak var GradiemtView : UIImageView!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var OuterView: UIView!
    @IBOutlet weak var OfferlabelText: UILabel!
    
    var coupon : Coupon?{
        didSet{
            self.couponImage.sd_setImage(with: URL(string: self.coupon?.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                           if (error != nil) {
                            self.couponImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                           } else {
                            self.couponImage.image = image
                           }
                       })
            OfferlabelText.text = coupon?.promo_description
            labelText.text = coupon?.promo_code
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        //self.couponImage.addShadow(radius: 10, color: .gray)
        self.labelText.font = .setCustomFont(name: .bold, size: .x14)
        self.OfferlabelText.font = .setCustomFont(name: .medium, size: .x12)
    }
    
   

}
