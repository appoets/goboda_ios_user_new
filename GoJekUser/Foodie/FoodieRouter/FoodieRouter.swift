//
//  FoodieRouter.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class FoodieRouter: FoodiePresenterToFoodieRouterProtocol {
    
    static func createFoodieModule(Id : Int?) -> UIViewController {
        let foodieDetailViewController  = foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieHomeViewController) as! FoodieHomeViewController
        let foodiePresenter: FoodieViewToFoodiePresenterProtocol & FoodieInteractorToFoodiePresenterProtocol = FoodiePresenter()
        let foodieInteractor: FoodiePresenterToFoodieInteractorProtocol = FoodieInteractor()
        let foodieRouter: FoodiePresenterToFoodieRouterProtocol = FoodieRouter()
        foodieDetailViewController.foodiePresenter = foodiePresenter
        foodiePresenter.foodieView = foodieDetailViewController
        foodiePresenter.foodieRouter = foodieRouter
        foodiePresenter.foodieInteractor = foodieInteractor
        foodieInteractor.foodiePresenter = foodiePresenter
        
        print("Id>>>>",Id)
        foodieDetailViewController.restID = Id ?? 0
        return foodieDetailViewController
    }
    
     static func createFoodieOrderStatusModule(isHome: Bool?, orderId: Int?,isChat: Bool?) -> UIViewController {
           
           let foodieOrderStatusViewController  = foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieOrderStatusViewController) as! FoodieOrderStatusViewController
        let foodiePresenter: FoodieViewToFoodiePresenterProtocol & FoodieInteractorToFoodiePresenterProtocol = FoodiePresenter()
        let foodieInteractor: FoodiePresenterToFoodieInteractorProtocol = FoodieInteractor()
        let foodieRouter: FoodiePresenterToFoodieRouterProtocol = FoodieRouter()
        foodieOrderStatusViewController.foodiePresenter = foodiePresenter
        foodiePresenter.foodieView = foodieOrderStatusViewController
        foodiePresenter.foodieRouter = foodieRouter
        foodiePresenter.foodieInteractor = foodieInteractor
        foodieInteractor.foodiePresenter = foodiePresenter
        foodieOrderStatusViewController.isHome = isHome ?? false
        foodieOrderStatusViewController.orderRequestId = orderId ?? 0
        foodieOrderStatusViewController.OrderfromChatNotification = isChat ?? false
        return foodieOrderStatusViewController
       }
    
    
     static func createFoodieHomeModule(id : Int?) -> UIViewController {
           
           let foodieOrderStatusViewController  = foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieScreenVC) as! FoodieScreenVC //FoodController
           let foodiePresenter: FoodieViewToFoodiePresenterProtocol & FoodieInteractorToFoodiePresenterProtocol = FoodiePresenter()
           let foodieInteractor: FoodiePresenterToFoodieInteractorProtocol = FoodieInteractor()
           let foodieRouter: FoodiePresenterToFoodieRouterProtocol = FoodieRouter()
           foodieOrderStatusViewController.foodiePresenter = foodiePresenter
           foodiePresenter.foodieView = foodieOrderStatusViewController
           foodiePresenter.foodieRouter = foodieRouter
           foodiePresenter.foodieInteractor = foodieInteractor
           foodieInteractor.foodiePresenter = foodiePresenter
           foodieOrderStatusViewController.restID = id ?? 0
           return foodieOrderStatusViewController
       }
    
    
    static func createFoodieItemModule(id : Int?) -> UIViewController {
          
          let foodieOrderStatusViewController  = foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieItemsViewController) as! FoodieItemsViewController
          let foodiePresenter: FoodieViewToFoodiePresenterProtocol & FoodieInteractorToFoodiePresenterProtocol = FoodiePresenter()
          let foodieInteractor: FoodiePresenterToFoodieInteractorProtocol = FoodieInteractor()
          let foodieRouter: FoodiePresenterToFoodieRouterProtocol = FoodieRouter()
          foodieOrderStatusViewController.restaurentId = id
          foodieOrderStatusViewController.foodiePresenter = foodiePresenter
          foodiePresenter.foodieView = foodieOrderStatusViewController
          foodiePresenter.foodieRouter = foodieRouter
          foodiePresenter.foodieInteractor = foodieInteractor
          foodieInteractor.foodiePresenter = foodiePresenter
          return foodieOrderStatusViewController
      }
    
    static func createCart(storeTypeId:Int,restaurantId:Int) -> UIViewController {
          
          let foodieOrderStatusViewController  = foodieStoryboard.instantiateViewController(withIdentifier: FoodieConstant.FoodieCartViewController) as! FoodieCartViewController
          let foodiePresenter: FoodieViewToFoodiePresenterProtocol & FoodieInteractorToFoodiePresenterProtocol = FoodiePresenter()
          let foodieInteractor: FoodiePresenterToFoodieInteractorProtocol = FoodieInteractor()
          let foodieRouter: FoodiePresenterToFoodieRouterProtocol = FoodieRouter()
          foodieOrderStatusViewController.foodiePresenter = foodiePresenter
        foodieOrderStatusViewController.restaurantId = restaurantId
        foodieOrderStatusViewController.storeTypeId = storeTypeId
          foodiePresenter.foodieView = foodieOrderStatusViewController
          foodiePresenter.foodieRouter = foodieRouter
          foodiePresenter.foodieInteractor = foodieInteractor
          foodieInteractor.foodiePresenter = foodiePresenter
          return foodieOrderStatusViewController
      }
    
    static var foodieStoryboard: UIStoryboard {
        return UIStoryboard(name:"Foodie",bundle: Bundle.main)
    }
    
    
    
    
  
    
    
}



