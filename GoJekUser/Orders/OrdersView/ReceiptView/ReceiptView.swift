//
//  ReceiptView.swift
//  GoJekUser
//
//  Created by Ansar on 12/04/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class ReceiptView: UIView {
    
    @IBOutlet weak var staticReceiptLabel: UILabel!
    
    @IBOutlet weak var itemDiscountView: UIView!
    @IBOutlet weak var itemDiscountValueLabel: UILabel!
    @IBOutlet weak var staticItemDiscountLabel: UILabel!
    @IBOutlet weak var tollChargeView: UIView!
    @IBOutlet weak var tollChargeValueLabel: UILabel!
    @IBOutlet weak var staticTollChargeLabel: UILabel!
    @IBOutlet weak var staticTotalLabel: UILabel!
    @IBOutlet weak var staticBaseFareLabel: UILabel!
    @IBOutlet weak var staticTaxFareLabel: UILabel!
    @IBOutlet weak var staticHourlyLabel: UILabel!
    @IBOutlet weak var staticDistanceLabel: UILabel!
    @IBOutlet weak var staticWalletLabel: UILabel!
    @IBOutlet weak var staticDiscountLabel: UILabel!
    @IBOutlet weak var staticTipsLabel: UILabel!
    @IBOutlet weak var staticWaitingLabel: UILabel!
    @IBOutlet weak var staticRoundOffLabel: UILabel!
    @IBOutlet weak var staticExtraChargeLabel: UILabel!
    @IBOutlet weak var staticPayableLabel: UILabel!
    
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var deliveryChargeLabel: UILabel!
    @IBOutlet weak var staticDeliveryChargeLabel: UILabel!
    @IBOutlet weak var packageValueLabel: UILabel!
    @IBOutlet weak var staticPackageLabel: UILabel!
    @IBOutlet weak var baseFareValueLabel: UILabel!
    @IBOutlet weak var taxFareValueLabel: UILabel!
    @IBOutlet weak var hourlyFareValueLabel: UILabel!
    @IBOutlet weak var distanceValueLabel: UILabel!
    @IBOutlet weak var walletFareValueLabel: UILabel!
    @IBOutlet weak var discountFareValueLabel: UILabel!
    @IBOutlet weak var tipsFareValueLabel: UILabel!
    @IBOutlet weak var totalFareValueLabel: UILabel!
    @IBOutlet weak var waitingValueLabel: UILabel!
    @IBOutlet weak var roundOffValueLabel: UILabel!
    @IBOutlet weak var extraChargeLabel: UILabel!
    @IBOutlet weak var payableLabel: UILabel!
    
    @IBOutlet weak var receiptOuterView: UIView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var baseFareView: UIView!
    @IBOutlet weak var taxFareView: UIView!
    @IBOutlet weak var hourlyFareView: UIView!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var tipsView: UIView!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var roundOffView: UIView!
    @IBOutlet weak var extraChargeView: UIView!
    @IBOutlet weak var payableView: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
    
    var onTapClose:(()->Void)?
    
    override func awakeFromNib() {
        superview?.awakeFromNib()
        initialLoads()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        receiptOuterView.addDashLine(strokeColor: .appPrimaryColor, lineWidth: 1)
    }
}

//MARK: - Methods

extension ReceiptView  {
    
    private func initialLoads() {
        totalView.backgroundColor = .appPrimaryColor
        totalView.setCornerRadiuswithValue(value: 5)
        self.closeButton.addTarget(self, action: #selector(tapClose), for: .touchUpInside)
        setFont()
        localize()
        payableView.backgroundColor = .veryLightGray
    }
    
    @objc func tapClose() {
        self.onTapClose!()
    }
    
    private func localize()  {
        staticReceiptLabel.text  = OrderConstant.receipt.localized
//        staticBaseFareLabel.text  = OrderConstant.baseFare.localized
        staticTaxFareLabel.text  = OrderConstant.taxFare.localized
        staticHourlyLabel.text  = OrderConstant.hourFare.localized
        staticWalletLabel.text  = OrderConstant.wallet.localized
        staticDiscountLabel.text  = OrderConstant.discountApplied.localized
        staticTipsLabel.text  = OrderConstant.tips.localized
        staticDistanceLabel.text = OrderConstant.distanceFare.localized
        staticWaitingLabel.text = OrderConstant.waiting.localized
        staticRoundOffLabel.text = OrderConstant.roundOff.localized
        staticExtraChargeLabel.text = OrderConstant.extraCharge.localized
  
        staticPackageLabel.text = OrderConstant.packageCharge.localized
        staticDeliveryChargeLabel.text = OrderConstant.deliveryCharge.localized
        staticTollChargeLabel.text = OrderConstant.tollCharge.localized
        staticItemDiscountLabel.text = OrderConstant.itemDiscount.localized
        staticPayableLabel.text = OrderConstant.total.localized
        
        
        if CommonFunction.checkisRTL() {
            baseFareValueLabel.textAlignment = .left
            taxFareValueLabel.textAlignment = .left
            hourlyFareValueLabel.textAlignment = .left
            distanceValueLabel.textAlignment = .left
            discountFareValueLabel.textAlignment = .left
            walletFareValueLabel.textAlignment = .left
            tipsFareValueLabel.textAlignment = .left
            totalFareValueLabel.textAlignment = .left
            waitingValueLabel.textAlignment = .left
            roundOffValueLabel.textAlignment = .left
            extraChargeLabel.textAlignment = .left
            payableLabel.textAlignment = .left
        }else {
            baseFareValueLabel.textAlignment = .right
            taxFareValueLabel.textAlignment = .right
            hourlyFareValueLabel.textAlignment = .right
            distanceValueLabel.textAlignment = .right
            walletFareValueLabel.textAlignment = .right
            discountFareValueLabel.textAlignment = .right
            tipsFareValueLabel.textAlignment = .right
            totalFareValueLabel.textAlignment = .right
            waitingValueLabel.textAlignment = .right
            roundOffValueLabel.textAlignment = .right
            extraChargeLabel.textAlignment = .right
            payableLabel.textAlignment = .right
        }
    }
    
    private func setFont()  {
        itemDiscountValueLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticItemDiscountLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticTollChargeLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        tollChargeValueLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticReceiptLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        staticBaseFareLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticTaxFareLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticHourlyLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticWalletLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticDiscountLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticTotalLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticTipsLabel.font  = UIFont.setCustomFont(name: .medium, size: .x14)
        staticDistanceLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        staticWaitingLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        staticRoundOffLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        staticExtraChargeLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        staticPackageLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        staticDeliveryChargeLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)
        staticPayableLabel.font = UIFont.setCustomFont(name: .medium, size: .x14)

        baseFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        taxFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        hourlyFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        walletFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        discountFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        walletFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        tipsFareValueLabel.font  = UIFont.setCustomFont(name: .bold, size: .x14)
        distanceValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        waitingValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        roundOffValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        totalFareValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        extraChargeLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        packageValueLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        deliveryChargeLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
        payableLabel.font = UIFont.setCustomFont(name: .bold, size: .x14)
    }
    
    func setCourierValues(values: CourierHistoryEntity) {
        deliveryView.isHidden = true
        packageView.isHidden = true
        itemDiscountView.isHidden = true
        tipsView.isHidden = true
        let paymentMode = PaymentType(rawValue: values.payment_mode ?? "CASH") ?? .CASH
        self.baseFareValueLabel.text = values.fixed_amount?.setCurrency()
        staticBaseFareLabel.text  = OrderConstant.baseFare.localized
        self.taxFareValueLabel.text = (values.tax_amount ?? 0)?.setCurrency()
        staticWaitingLabel.text = OrderConstant.weight.localized
       // self.hourlyFareValueLabel.text = (values.hour ?? 0)?.setCurrency()
        hourlyFareView.isHidden = true
        self.distanceValueLabel.text = (values.distance_amount ?? 0)?.setCurrency()
        
//        self.walletFareValueLabel.text = values.wallet?.setCurrency()
//        self.walletView.isHidden = (values.wallet ?? 0) == 0
        self.walletView.isHidden = true
        self.discountFareValueLabel.text = "-" + (values.discount_amount?.setCurrency() ?? "")
        self.discountView.isHidden = (values.discount_amount ?? 0) == 0
        
//        self.tipsFareValueLabel.text = values.tips?.setCurrency()
//        self.tipsView.isHidden = (values.tips ?? 0) == 0
        
        self.waitingValueLabel.text = values.weight_amount?.setCurrency()
        self.waitingView.isHidden = (values.weight_amount ?? 0) == 0
//        self.roundOffValueLabel.text = values.round_of?.setCurrency()
//        self.roundOffView.isHidden = (values.round_of ?? 0) == 0
        self.roundOffView.isHidden = true
        
//        self.extraChargeLabel.text = values.extra_charges?.setCurrency()
//        self.extraChargeView.isHidden = (values.extra_charges ?? 0) == 0
        self.extraChargeView.isHidden = true
        
//        self.tollChargeValueLabel.text = values.toll_charge?.setCurrency()
//        self.tollChargeView.isHidden = (values.toll_charge ?? 0) == 0
        self.tollChargeView.isHidden = true
        
        let payableAmt = Double((values.total_amount ?? 0.0))
        payableLabel.text = payableAmt.setCurrency()
        
        if paymentMode == .CASH {
            
            let totalAmt = Double((values.total_amount ?? 0.0)).rounded()
            self.totalFareValueLabel.text = totalAmt.setCurrency()
        }else {
//            let totalAmt = Double((values.card ?? 0.0))
//            self.totalFareValueLabel.text = totalAmt.setCurrency()
        }
        staticTotalLabel.text  = OrderConstant.payableAmount.localized

    }
    
    func setValues(values: Payment) {
        deliveryView.isHidden = true
        packageView.isHidden = true
        itemDiscountView.isHidden = true
        let paymentMode = PaymentType(rawValue: values.payment_mode ?? "CASH") ?? .CASH
        self.baseFareValueLabel.text = values.fixed?.setCurrency()
        staticBaseFareLabel.text  = OrderConstant.baseFare.localized
        self.taxFareValueLabel.text = (values.tax ?? 0)?.setCurrency()
        
        self.hourlyFareValueLabel.text = (values.hour ?? 0)?.setCurrency()
        
        self.distanceValueLabel.text = (values.distance ?? 0)?.setCurrency()
        
        self.walletFareValueLabel.text = values.wallet?.setCurrency()
        self.walletView.isHidden = (values.wallet ?? 0) == 0
        
        self.discountFareValueLabel.text = "-" + (values.discount?.setCurrency() ?? "")
        self.discountView.isHidden = (values.discount ?? 0) == 0
        
        self.tipsFareValueLabel.text = values.tips?.setCurrency()
        self.tipsView.isHidden = (values.tips ?? 0) == 0
        
        self.waitingValueLabel.text = values.waiting_amount?.setCurrency()
        self.waitingView.isHidden = (values.waiting_amount ?? 0) == 0
        
        self.roundOffValueLabel.text = values.round_of?.setCurrency()
        self.roundOffView.isHidden = (values.round_of ?? 0) == 0
        
        self.extraChargeLabel.text = values.extra_charges?.setCurrency()
        self.extraChargeView.isHidden = (values.extra_charges ?? 0) == 0
        
        self.tollChargeValueLabel.text = values.toll_charge?.setCurrency()
        self.tollChargeView.isHidden = (values.toll_charge ?? 0) == 0
        
        let payableAmt = Double((values.tips ?? 0.0)+(values.total ?? 0.0))
        payableLabel.text = payableAmt.setCurrency()
        
        if paymentMode == .CASH {
            
            let totalAmt = Double((values.tips ?? 0.0)+(values.cash ?? 0.0))
            self.totalFareValueLabel.text = totalAmt.setCurrency()
        }else {
            let totalAmt = Double((values.tips ?? 0.0)+(values.card ?? 0.0))
            self.totalFareValueLabel.text = totalAmt.setCurrency()
        }
        staticTotalLabel.text  = OrderConstant.payableAmount.localized

    }
    
    func setFoodieValues(values: Order_invoice) {
      
        
        self.baseFareValueLabel.text = values.item_price?.setCurrency()
        self.baseFareView.isHidden  = (values.item_price ?? 0) == 0
        staticBaseFareLabel.text  = OrderConstant.cardSubTotalFare.localized
        self.taxFareValueLabel.text = values.tax_amount?.setCurrency()
        self.taxFareView.isHidden = (values.tax_amount ?? 0) == 0
        payableView.isHidden = true
        
        self.hourlyFareView.isHidden = true
        self.distanceView.isHidden = true
        self.tipsView.isHidden = true
        self.waitingView.isHidden = true
        self.extraChargeView.isHidden = true
        self.tollChargeView.isHidden = true
        
        self.roundOffValueLabel.text = values.round_of?.setCurrency()
        self.roundOffView.isHidden = (values.round_of ?? 0) == 0
        staticTotalLabel.text  = OrderConstant.total.localized

        
        self.itemDiscountValueLabel.text = "-" + (values.discount?.setCurrency() ?? "")
        self.itemDiscountView.isHidden = (values.discount ?? 0) == 0
        
        
        self.walletFareValueLabel.text = values.wallet_amount?.setCurrency()
        self.walletView.isHidden = (values.wallet_amount ?? 0) == 0
        
        self.discountFareValueLabel.text = "-" + (values.promocode_amount?.setCurrency() ?? "")
        self.discountView.isHidden =  (values.promocode_amount ?? 0) == 0
        
        self.deliveryChargeLabel.text = (values.delivery_amount?.setCurrency() ?? "")
        self.deliveryView.isHidden =  (values.delivery_amount ?? 0) == 0
        
        self.packageValueLabel.text = (values.store_package_amount?.setCurrency() ?? "")
        self.packageView.isHidden =  (values.store_package_amount ?? 0) == 0
        
        self.payableLabel.text = values.total_amount?.setCurrency()
        
        self.totalFareValueLabel.text = values.cash?.setCurrency() //Web team told there is no condition for cash and card so added only cash flow
    }
}
