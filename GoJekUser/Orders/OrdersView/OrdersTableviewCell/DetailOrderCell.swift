//
//  DetailOrderCell.swift
//  GoJekUser
//
//  Created by Sudar on 06/07/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit

class DetailOrderCell: UITableViewCell {
    
    // Payment view
    @IBOutlet weak var staticPaymentLabel: UILabel!
    @IBOutlet weak var cardOrCashLabel: UILabel!
    @IBOutlet weak var paymentImageView: UIImageView!
    
    // Status View
    @IBOutlet weak var staticStatusLabel: UILabel!
    @IBOutlet weak var statusValueLabel: UILabel!
    
    // Commentes View
    @IBOutlet weak var commentOuterView: UIView!
    @IBOutlet weak var staticCommentLabel: UILabel!
    @IBOutlet weak var commentValueLabel: UILabel!
    @IBOutlet weak var paymentOuterView: UIView!

    // Items missing View
    @IBOutlet weak var lostItemOuterView: UIView!
    @IBOutlet weak var staticLostLabel: UILabel!
    @IBOutlet weak var staticLostDescLabel: UILabel!
    @IBOutlet weak var lostItemSubView: UIView!
    @IBOutlet weak var statusOuterView: UIView!

    var onClickLostItem:(()->Void)?


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initalLoads()
    }
    
    private func initalLoads(){
        if  let lostImage = lostItemSubView.subviews.first as? UIImageView {
            lostImage.image = UIImage(named: OrderConstant.alertImage)
            lostImage.imageTintColor(color1: .appPrimaryColor)
        }
        let lostGesture = UITapGestureRecognizer(target: self, action: #selector(tapLostItem))
        self.lostItemSubView.addGestureRecognizer(lostGesture)
        self.paymentImageView.image = UIImage(named: Constant.payment)
        self.paymentImageView.imageTintColor(color1: .lightGray)
        lostItemSubView.isUserInteractionEnabled = true
        setFont()
        setColors()
        localize()
        lostItemOuterView.isHidden = true

    }
    
    private func setFont() {
        staticStatusLabel.font = UIFont.setCustomFont(name: .bold, size: .x12)
        statusValueLabel.font = UIFont.setCustomFont(name: .light, size: .x12)
        staticPaymentLabel.font = UIFont.setCustomFont(name: .bold, size: .x12)
        cardOrCashLabel.font = UIFont.setCustomFont(name: .light, size: .x12)
        staticCommentLabel.font = UIFont.setCustomFont(name: .bold, size: .x12)
        commentValueLabel.font = UIFont.setCustomFont(name: .light, size: .x12)
        staticLostLabel.font = UIFont.setCustomFont(name: .bold, size: .x12)
        staticLostDescLabel.font = UIFont.setCustomFont(name: .bold, size: .x12)
    }
    
    @objc func tapLostItem() {
        onClickLostItem!()
    }
    
    private func localize() {
           staticPaymentLabel.text = OrderConstant.paymentVia.localized
           staticLostDescLabel.text = OrderConstant.presstheicon.localized
           staticStatusLabel.text = OrderConstant.status.localized
    }
    
    private func setColors() {
        lostItemSubView.backgroundColor = UIColor.appPrimaryColor.withAlphaComponent(0.3)
        staticLostDescLabel.textColor = .appPrimaryColor
    }
    
    func setValues(data: OrderDetailReponseData) {
        DispatchQueue.main.async {
            if let transportData = data.transport {
                self.staticCommentLabel.text = OrderConstant.commentsfor.localized.giveSpace + ServiceType.trips.rawValue
                self.lostItemOuterView.isHidden = false

                //self.cardOrCashLabel.text = transportData.payment_mode ?? ""
                if transportData.use_wallet == 1 && transportData.payment?.wallet == transportData.payment?.total {
                    self.cardOrCashLabel.text = "Wallet"
                }else{
                    self.cardOrCashLabel.text = transportData.payment_mode ?? ""  + "\( transportData.use_wallet ?? 0 == 1 ? " , Wallet" : "")"
                }
                
                
                
                if let comment = transportData.rating?.provider_comment, comment.count != 0  {
                    self.commentValueLabel.text = comment
                }else{
                    self.commentValueLabel.text = OrderConstant.noComment.localized
                }
                
                if let status = tripStatus(rawValue: transportData.status ?? "") {
                    self.statusValueLabel.text = status.statusString
                }
                self.staticLostLabel.text = transportData.lost_item == nil ? OrderConstant.didyoulosesomething.localized : OrderConstant.knowLostItemStatus.localized
                
                if transportData.payment == nil {
                    self.paymentOuterView.isHidden = true
                }
            }
            if let courierData = data.delivery {
                self.staticCommentLabel.text = OrderConstant.commentsfor.localized.giveSpace + ServiceType.delivery.rawValue
               //self.cardOrCashLabel.text = courierData.payment_mode
                
                
                if courierData.use_wallet == 1 && courierData.payment?.wallet == courierData.payment?.total {
                    self.cardOrCashLabel.text = "Wallet"
                }else{
                    self.cardOrCashLabel.text = courierData.payment_mode ?? ""  + "\( courierData.use_wallet ?? 0 == 1 ? " , Wallet" : "")"
                }

                if let comment = courierData.rating?.provider_comment, comment.count != 0  {
                    self.commentValueLabel.text = comment
                }else{
                    self.commentValueLabel.text = OrderConstant.noComment.localized
                }
                
                if let status = tripStatus(rawValue: courierData.deliveries?.first?.status ?? "") {
                    self.statusValueLabel.text = status.statusString
                }
            }
            if let serviceData = data.service {
                self.staticCommentLabel.text = OrderConstant.commentsfor.localized.giveSpace + ServiceType.service.rawValue
                
                self.lostItemOuterView.isHidden = true
                //self.cardOrCashLabel.text = serviceData.payment_mode
                
                  if serviceData.use_wallet == 1 && serviceData.payment?.wallet == serviceData.payment?.total {
                        self.cardOrCashLabel.text = "Wallet"
                  }else{
                       self.cardOrCashLabel.text = serviceData.payment_mode ?? ""  + "\( serviceData.use_wallet ?? 0 == 1 ? " , Wallet" : "")"
                   }
                
                if let comment = serviceData.rating?.provider_comment , comment.count != 0,comment != "null"  {
                    self.commentValueLabel.text = comment
                }else{
                    self.commentValueLabel.text = OrderConstant.noComment.localized
                }
                if let status = tripStatus(rawValue: serviceData.status ?? "") {
                    self.statusValueLabel.text = status.statusString
                }
                
                if serviceData.payment == nil {
                    self.paymentOuterView.isHidden = true
                }
            }
            if let foodieValue = data.order {
                self.staticCommentLabel.text = OrderConstant.commentsfor.localized.giveSpace + ServiceType.orders.rawValue
                self.lostItemOuterView.isHidden = true
                if let status = tripStatus(rawValue: foodieValue.status ?? "") {
                    self.statusValueLabel.text = status.statusString
                }
                //self.cardOrCashLabel.text = foodieValue.order_invoice?.payment_mode
                
                if foodieValue.order_invoice?.total_amount != 0.0 && foodieValue.order_invoice?.total_amount == foodieValue.order_invoice?.wallet_amount{
                    self.cardOrCashLabel.text = "Wallet"
                }else{
                    self.cardOrCashLabel.text = foodieValue.order_invoice?.payment_mode?.uppercased() ?? "" + "\((foodieValue.order_invoice?.wallet_amount != nil && foodieValue.order_invoice?.wallet_amount ?? 0 > 0.0) ? "Wallet" : "") "
                }
                
                if let comment = foodieValue.rating?.provider_comment, comment.count != 0  {
                    self.commentValueLabel.text = comment
                }else{
                    self.commentValueLabel.text = OrderConstant.noComment.localized
                }
                if foodieValue.order_invoice == nil {
                    self.paymentOuterView.isHidden = true
                }
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
